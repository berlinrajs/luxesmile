//
//  ResultAcceptanceViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/16/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ResultAcceptanceViewController: PDViewController {

    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var signatureView3: SignatureView!

    @IBOutlet weak var initialView1: SignatureView!
    @IBOutlet weak var initialView2: SignatureView!
    @IBOutlet weak var initialView3: SignatureView!

    @IBOutlet weak var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonNextAction() {
        if !signatureView1.isSigned() || !signatureView3.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView1.signatureImage()
            patient.signature2 = signatureView2.signatureImage()
            patient.signature3 = signatureView3.signatureImage()
            
            patient.initial1 = initialView1.signatureImage()
            patient.initial2 = initialView2.signatureImage()
            patient.initial3 = initialView3.signatureImage()

            let form = self.storyboard?.instantiateViewController(withIdentifier: "kResultAcceptanceFormVC") as! ResultAcceptanceFormViewController
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        }
    }

}
