//
//  ResultAcceptanceFormViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/16/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ResultAcceptanceFormViewController: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var signatureView1: UIImageView!
    @IBOutlet weak var signatureView2: UIImageView!
    @IBOutlet weak var signatureView3: UIImageView!
    
    @IBOutlet weak var initialView1: UIImageView!
    @IBOutlet weak var initialView2: UIImageView!
    @IBOutlet weak var initialView3: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView1.image = patient.signature1
        signatureView2.image = patient.signature2
        signatureView3.image = patient.signature3
        
        initialView1.image = patient.initial1
        initialView2.image = patient.initial2
        initialView3.image = patient.initial3

        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
