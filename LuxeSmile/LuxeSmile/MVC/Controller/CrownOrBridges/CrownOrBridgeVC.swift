//
//  CrownOrBridgeVC.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 6/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class CrownOrBridgeVC: PDViewController {
    
   
    @IBOutlet weak var textField1: PDTextField!
    
    @IBOutlet weak var signatureView1: SignatureView!
    
    @IBOutlet weak var dateLabel1: DateLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signatureView1.layer.cornerRadius = 3.0
        dateLabel1.todayDate = patient.dateToday
        buttonBack?.isHidden = isFromPreviousForm
        loadValues()
    }
  
    func loadValues() {
        textField1.text = patient.witness
    }
    
    func setValues() {
        patient.witness = textField1.text
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        if !signatureView1.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
            
        }else if !dateLabel1.dateTapped {
            
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else if textField1.isEmpty {
            
            let alert = Extention.alert("PLEASE ENTER WITNESS NAME")
            self.present(alert, animated: true, completion: nil)
        }else {
            
            setValues()
            patient.patientSignature = signatureView1.signatureImage()
            
            let crownBridgeForm = self.storyboard?.instantiateViewController(withIdentifier: "kCrownBridgeFormVC") as! CrownOrBridgeVCForm
            crownBridgeForm.patient = patient
            self.navigationController?.pushViewController(crownBridgeForm, animated: true)
        }

        
        
        
    }
    
  
}
