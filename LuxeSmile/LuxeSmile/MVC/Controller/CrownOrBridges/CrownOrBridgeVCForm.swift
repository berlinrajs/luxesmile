//
//  CrownOrBridgeVCForm.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 6/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class CrownOrBridgeVCForm: PDViewController {
    
    
    @IBOutlet weak var signatureView1: UIImageView!
    
    @IBOutlet weak var witnessName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelToothNumber: UILabel!
    
    @IBOutlet weak var PatientSignname: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kCrownOrBridgeVC
        }

        
        signatureView1.image = patient.patientSignature
        
        witnessName.text = patient.witness.value
        
        labelToothNumber.text = form[0].toothNumber
        
        labelDate.text = patient.dateToday
        
        PatientSignname.text = patient.fullName
        
        
    }


    
}
