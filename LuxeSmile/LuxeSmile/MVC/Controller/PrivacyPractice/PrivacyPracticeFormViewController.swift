//
//  PrivacyPracticeFormViewController.swift
//  LuxeSmile
//
//  Created by Bala Murugan on 6/23/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrivacyPracticeFormViewController: PDViewController {

    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelPatientDOB : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelRepName : UILabel!
    @IBOutlet weak var labelRepAuthority : UILabel!
    @IBOutlet weak var signatureImage : UIImageView!
    @IBOutlet weak var logoWithAddress: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

         labelPatientName.text = patient.fullName
        labelPatientDOB.text = patient.dateOfBirth
        labelDate.text = patient.dateToday
        labelRepName.text = patient.repName.value
        labelRepAuthority.text = patient.repAuthority.value
        signatureImage.image = patient.repSignature
        
        if isFromPatientIntakeFrom {
            buttonSubmit?.isHidden = true
            buttonBack?.isHidden = true
            logoWithAddress.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
