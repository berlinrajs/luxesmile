//
//  PrivacyPractice1ViewController.swift
//  LuxeSmile
//
//  Created by Bala Murugan on 6/23/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrivacyPractice1ViewController: PDViewController {
    @IBOutlet weak var textfieldRepresentativeName : UITextField!
    @IBOutlet weak var textfieldRepresentativeAuthority : UITextField!
    @IBOutlet weak var signature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var buttonNext: PDButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBack?.isHidden = isFromPreviousForm && !isFromPatientIntakeFrom
        if isFromPatientIntakeFrom {
            buttonNext?.setTitle("NEXT", for: UIControlState())
        }
        labelDate.todayDate = patient.dateToday
        loadValues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
        patient.repName = textfieldRepresentativeName.text
        patient.repAuthority = textfieldRepresentativeAuthority.text
    }
    
    func loadValues() {
        textfieldRepresentativeName.text = patient.repName
        textfieldRepresentativeAuthority.text = patient.repAuthority
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if !signature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
            
        }else{
            setValues()
            patient.repSignature = signature.signatureImage()
            if isFromPatientIntakeFrom {
                let financialPolicy = PatientInTakeStoryBoard.instantiateViewController(withIdentifier: "kFinancialPolicyVC") as! FinancialPolicyVC
                financialPolicy.patient = self.patient
                financialPolicy.isFromPatientIntakeFrom = isFromPatientIntakeFrom
                self.navigationController?.pushViewController(financialPolicy, animated: true)
            } else {
                let privacy = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyFormVC") as! PrivacyPracticeFormViewController
                privacy.patient = patient
                self.navigationController?.pushViewController(privacy, animated: true)
            }
        }
    }
}

extension PrivacyPractice1ViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
