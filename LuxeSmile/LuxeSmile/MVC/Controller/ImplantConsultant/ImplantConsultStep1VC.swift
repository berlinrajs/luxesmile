//
//  ImplantConsultStep1VC.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 15/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImplantConsultStep1VC: PDViewController {

    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var witnessSignView: SignatureView!
    @IBOutlet weak var dentistSignView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var replaceDoctorName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelName.text = patient.fullName
        
        replaceDoctorName.text = replaceDoctorName.text?.replacingOccurrences(of: "Dr. Burghardt", with: patient.doctorName)
        
        doctorName.text = patient.doctorName
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !witnessSignView.isSigned() {
            let alert = Extention.alert("PLEASE GET SIGNATURE FROM WITNESS")
            self.present(alert, animated: true, completion: nil)
        } else if !dentistSignView.isSigned() {
            let alert = Extention.alert("PLEASE GET SIGNATURE FROM THE DENTIST")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            let form = self.storyboard?.instantiateViewController(withIdentifier: "kImplantConsultantFormVC") as! ImplantConsultantFormVC
            form.patientSignature = signatureView.signatureImage()
            form.witnessSignature = witnessSignView.signatureImage()
            form.dentistSignature = dentistSignView.signatureImage()
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        }
    }
}
