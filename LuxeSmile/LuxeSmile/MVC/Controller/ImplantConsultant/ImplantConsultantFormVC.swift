//
//  ImplantConsultantFormVC.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 15/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImplantConsultantFormVC: PDViewController {

    @IBOutlet weak var labelToothNumber: FormLabel!
    var patientSignature: UIImage!
    var dentistSignature: UIImage!
    var witnessSignature: UIImage!
    
    @IBOutlet weak var doctorName1: UILabel!
    
    @IBOutlet weak var doctorName2: UILabel!
    
    @IBOutlet weak var patientSignatureView: UIImageView!
    @IBOutlet weak var witnessSignatureView: UIImageView!
    @IBOutlet weak var dentistSignatureView: UIImageView!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        patientSignatureView.image = patientSignature
        witnessSignatureView.image = witnessSignature
        dentistSignatureView.image = dentistSignature
        
        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        
        doctorName1.text = patient.doctorName
        doctorName2.text = patient.doctorName
        
        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kImplantConsent
        }
        
           labelToothNumber.text = form[0].toothNumber

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
