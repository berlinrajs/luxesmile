//
//  QuickTreatmentStep1ViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class QuickTreatmentStep1ViewController: PDViewController {
    
    @IBOutlet weak var textFieldCrowdingUpper: PDTextField!
    @IBOutlet weak var textFieldCrowdingUpperSpacing: PDTextField!
    @IBOutlet weak var textFieldCrowdingLower: PDTextField!
    @IBOutlet weak var textFieldCrowdingLowerSpacing: PDTextField!
    @IBOutlet weak var textFieldUpperWire: PDTextField!
    @IBOutlet weak var textFieldLowerWire: PDTextField!
    
    @IBOutlet weak var radioButtonUpperReproximation: RadioButton!
    @IBOutlet weak var radioButtonLowerReproximation: RadioButton!
    
    @IBOutlet weak var radioButtonAlignmentUpper: RadioButton!
    @IBOutlet weak var radioButtonAlignmentLower: RadioButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if qt.quickTreatmentQuestions.count == 0 {
            for index in 0...5 {
                let question = PDQuestion()
                question.index = index
                patient.quickTreatment.quickTreatmentQuestions.append(question)
            }
        }
        loadValues()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var qt : QuickTreatment {
        return patient.quickTreatment
    }
    
    
    func loadValues() {
        radioButtonAlignmentUpper.isSelected = qt.quickTreatmentQuestions[0].selectedOption == true
        radioButtonAlignmentLower.isSelected = qt.quickTreatmentQuestions[1].selectedOption == true
        
        textFieldCrowdingUpper.text = qt.crowdingUpper
        textFieldCrowdingUpperSpacing.text = qt.crowdingUpperSpacing
        textFieldCrowdingLower.text = qt.crowdingLower
        textFieldCrowdingLowerSpacing.text = qt.crowdingLowerSpacing
        textFieldUpperWire.text = qt.upperWire
        textFieldLowerWire.text = qt.lowerWire
        if let tag = qt.upperReproximation {
            self.radioButtonUpperReproximation.setSelectedWithTag(tag)
        }
        if let tag = qt.lowerReproximation {
            self.radioButtonLowerReproximation.setSelectedWithTag(tag)
        }
    }
    
    func setValues() {
        qt.quickTreatmentQuestions[0].selectedOption = radioButtonAlignmentUpper.isSelected
        qt.quickTreatmentQuestions[1].selectedOption = radioButtonAlignmentLower.isSelected
        
        qt.crowdingUpper = textFieldCrowdingUpper.text
        qt.crowdingUpperSpacing = textFieldCrowdingUpperSpacing.text
        qt.crowdingLower = textFieldCrowdingLower.text
        qt.crowdingLowerSpacing = textFieldCrowdingLowerSpacing.text
        qt.upperWire = textFieldUpperWire.text
        qt.lowerWire = textFieldLowerWire.text
        if let selected = self.radioButtonUpperReproximation.selected {
            qt.upperReproximation = selected.tag
        }
        if let selected = self.radioButtonLowerReproximation.selected {
            qt.lowerReproximation = selected.tag
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(self)
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(false)
        setValues()
        let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kQuickTreatmentStep2VC") as! QuickTreatmentStep2ViewController
        step2VC.patient = self.patient
        self.navigationController?.pushViewController(step2VC, animated: true)
    }

}
