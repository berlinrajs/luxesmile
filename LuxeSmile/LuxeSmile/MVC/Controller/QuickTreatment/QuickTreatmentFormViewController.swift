//
//  QuickTreatmentFormViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class QuickTreatmentFormViewController: PDViewController {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    

    @IBOutlet weak var labelCrowdingUpper: UILabel!
    @IBOutlet weak var labelCrowdingUpperSpacing: UILabel!
    @IBOutlet weak var labelCrowdingLower: UILabel!
    @IBOutlet weak var labelCrowdingLowerSpacing: UILabel!
    @IBOutlet weak var labelUpperWire: UILabel!
    @IBOutlet weak var labelLowerWire: UILabel!
    
    @IBOutlet weak var radioButtonUpperReproximation: RadioButton!
    @IBOutlet weak var radioButtonLowerReproximation: RadioButton!
    
    @IBOutlet weak var radioButtonAlignmentUpper: RadioButton!
    @IBOutlet weak var radioButtonAlignmentLower: RadioButton!
    
    @IBOutlet weak var radioButtonAmountOfCrowding: RadioButton!
    @IBOutlet weak var labelAmountOfCrowdingToothNumber: UILabel!

    @IBOutlet weak var radioButtonRisk: RadioButton!
    @IBOutlet weak var labelRiskToothNumber: UILabel!

    @IBOutlet weak var radioButtonOverjet: RadioButton!
    @IBOutlet weak var radioButtonCommunicated: RadioButton!
    
    
    @IBOutlet var labelSpecialAttention: [UILabel]!
    @IBOutlet var labelComplaint: [UILabel]!
    @IBOutlet var labelItemToCorrect: [UILabel]!
    
    
    var qt : QuickTreatment {
        return patient.quickTreatment
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kQuickTreatment
        }
        
        labelName.text = patient.fullName
        labelTime.text = form[0].toothNumber
        
        labelCrowdingUpper.text = qt.crowdingUpper.value
        labelCrowdingUpperSpacing.text = qt.crowdingUpperSpacing.value
        labelCrowdingLower.text = qt.crowdingLower.value
        labelCrowdingLowerSpacing.text = qt.crowdingLowerSpacing.value
        labelUpperWire.text = qt.upperWire.value
        labelLowerWire.text = qt.lowerWire.value
        
        radioButtonAlignmentUpper.isSelected = qt.quickTreatmentQuestions[0].selectedOption!
        radioButtonAlignmentLower.isSelected = qt.quickTreatmentQuestions[1].selectedOption!
        radioButtonAmountOfCrowding.isSelected = qt.quickTreatmentQuestions[2].selectedOption!
        radioButtonRisk.isSelected = qt.quickTreatmentQuestions[3].selectedOption!
        radioButtonOverjet.isSelected = qt.quickTreatmentQuestions[4].selectedOption!
        radioButtonCommunicated.isSelected = qt.quickTreatmentQuestions[5].selectedOption!
        
        qt.specialAttention.value.setTextForArrayOfLabels(labelSpecialAttention)
        qt.complaint.value.setTextForArrayOfLabels(labelComplaint)
        qt.itemToCorrect.value.setTextForArrayOfLabels(labelItemToCorrect)

        if radioButtonAmountOfCrowding.isSelected {
            labelAmountOfCrowdingToothNumber.text = qt.quickTreatmentQuestions[2].answer
        }

        if radioButtonRisk.isSelected {
            labelRiskToothNumber.text = qt.quickTreatmentQuestions[3].answer
        }
        
        if let selected = qt.upperReproximation {
            radioButtonUpperReproximation.setSelectedWithTag(selected)
        }
        
        if let selected = qt.lowerReproximation {
            radioButtonLowerReproximation.setSelectedWithTag(selected)
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
