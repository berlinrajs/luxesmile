//
//  QuickTreatmentStep2ViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class QuickTreatmentStep2ViewController: PDViewController {
    
    @IBOutlet weak var radioButtonAmountOfCrowding: RadioButton!
    @IBOutlet weak var radioButtonRisk: RadioButton!
    @IBOutlet weak var radioButtonOverjet: RadioButton!
    @IBOutlet weak var radioButtonCommunicated: RadioButton!
    
    @IBOutlet weak var textViewSpecialAttention: MCTextView!
    @IBOutlet weak var textViewComplaint: MCTextView!
    @IBOutlet weak var textViewItemToCorrect: MCTextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func amountOfCrowdingAction(_ sender: RadioButton) {
        let question = patient.quickTreatment.quickTreatmentQuestions[2]
        showPopup(question, sender: sender)
    }
    
    @IBAction func riskAction(_ sender: RadioButton) {
        let question = patient.quickTreatment.quickTreatmentQuestions[3]
        showPopup(question, sender: sender)
    }
    
    func showPopup(_ question: PDQuestion, sender : RadioButton) {
        PopupTextField.popUpView().showWithTitle("TOOTH NUMBERS", placeHolder: "05, 18, 29", keyboardType: UIKeyboardType.numbersAndPunctuation, textFormat: TextFormat.toothNumber, inViewController: self, completion: { (popUpView, textField, isEdited) in
            if textField.isEmpty {
                sender.isSelected = false
            } else {
                question.answer = textField.text!
            }
        })
    }
    
    func loadValues() {
        radioButtonAmountOfCrowding.isSelected = qt.quickTreatmentQuestions[2].selectedOption == true
        radioButtonRisk.isSelected = qt.quickTreatmentQuestions[3].selectedOption == true
        radioButtonOverjet.isSelected = qt.quickTreatmentQuestions[4].selectedOption == true
        radioButtonCommunicated.isSelected = qt.quickTreatmentQuestions[5].selectedOption == true
        if let text = qt.specialAttention, !text.isEmpty {
            textViewSpecialAttention.text = text
            textViewSpecialAttention.textColor = UIColor.black
        }
        if let text = qt.complaint, !text.isEmpty {
            textViewComplaint.text = text
            textViewComplaint.textColor = UIColor.black
        }
        if let text = qt.itemToCorrect, !text.isEmpty {
            textViewItemToCorrect.text = text
            textViewItemToCorrect.textColor = UIColor.black
        }
    }
    
    func setValues() {
        qt.quickTreatmentQuestions[2].selectedOption = radioButtonAmountOfCrowding.isSelected
        qt.quickTreatmentQuestions[3].selectedOption = radioButtonRisk.isSelected
        qt.quickTreatmentQuestions[4].selectedOption = radioButtonOverjet.isSelected
        qt.quickTreatmentQuestions[5].selectedOption = radioButtonCommunicated.isSelected
        
        qt.specialAttention = textViewSpecialAttention.isEmpty ? "" : textViewSpecialAttention.text
        qt.complaint = textViewComplaint.isEmpty ? "" : textViewComplaint.text
        qt.itemToCorrect = textViewItemToCorrect.isEmpty ? "" : textViewItemToCorrect.text
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    var qt : QuickTreatment {
        return patient.quickTreatment
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(false)
        setValues()

        let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kQuickTreatmentFormVC") as! QuickTreatmentFormViewController
        formVC.patient = self.patient
        self.navigationController?.pushViewController(formVC, animated: true)
    }

}
