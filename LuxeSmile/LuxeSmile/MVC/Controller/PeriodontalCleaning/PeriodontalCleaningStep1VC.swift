//
//  PeriodontalCleaningStep1VC.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 15/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PeriodontalCleaningStep1VC: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelName.text = labelName.text?.replacingOccurrences(of: "PATIENT_NAME", with: patient.fullName)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            let form = self.storyboard?.instantiateViewController(withIdentifier: "kPeriodontalCleaningFormVC") as! PeriodontalCleaningFormVC
            form.signature = signatureView.signatureImage()
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
