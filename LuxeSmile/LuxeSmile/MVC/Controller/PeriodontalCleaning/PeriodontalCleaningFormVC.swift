//
//  PeriodontalCleaningFormVC.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 15/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PeriodontalCleaningFormVC: PDViewController {

    var signature: UIImage!
    
    @IBOutlet weak var PatietnSignName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        PatietnSignName.text = patient.fullName
        
        signatureView.image = signature
        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
