//
//  ImplantSurgeryFormViewController.swift
//  LuxeSmile
//
//  Created by Bala Murugan on 7/15/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImplantSurgeryFormViewController: PDViewController {

    var signatureImage : UIImage!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var imageViewSignature : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = patient.fullName
        labelDate.text = patient.dateToday
        imageViewSignature.image = signatureImage
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
