//
//  InformedConsentTreatmentStep1VC.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 15/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InformedConsentTreatmentStep1VC: PDViewController {

    @IBOutlet weak var initials1: SignatureView!
    @IBOutlet weak var initials2: SignatureView!
    @IBOutlet weak var initials3: SignatureView!
    @IBOutlet weak var initials4: SignatureView!
    @IBOutlet weak var initials5: SignatureView!
    @IBOutlet weak var initials6: SignatureView!
    @IBOutlet weak var initials7: SignatureView!
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var dentistSignatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    
    @IBOutlet weak var replaceDoctorLabel: UILabel!
    
    
       @IBOutlet weak var replaceDoctorLabel2: UILabel!
    
    var toothNumber: String = ""
    var procedure: String = ""
    var condition: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
        
        replaceDoctorLabel.text = replaceDoctorLabel.text?.replacingOccurrences(of: "Dr. Burghardt", with: patient.doctorName)
        
             replaceDoctorLabel2.text = replaceDoctorLabel2.text?.replacingOccurrences(of: "Dr. Burghardt", with: patient.doctorName)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        if selectedNone {
            let alert = Extention.alert("PLEASE SIGN ANY ONE OF THE TREATMENT TO CONTINUE")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !dentistSignatureView.isSigned() {
            let alert = Extention.alert("PLEASE GET SIGNATURE FROM THE DENTIST")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            self.checkProcedutreCondition({ (procedure, condition) in
                self.procedure = procedure
                self.condition = condition
                self.checkToothNumber({ (toothNumber) in
                    self.toothNumber = toothNumber
                    let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kInformedConsentTreatmentFormVC") as! InformedConsentTreatmentFormVC
                    formVC.initial1 = self.initials1.signatureImage()
                    formVC.initial2 = self.initials2.signatureImage()
                    formVC.initial3 = self.initials3.signatureImage()
                    formVC.initial4 = self.initials4.signatureImage()
                    formVC.initial5 = self.initials5.signatureImage()
                    formVC.initial6 = self.initials6.signatureImage()
                    formVC.initial7 = self.initials7.signatureImage()
                    
                    formVC.procedure = self.procedure
                    formVC.toothNumber = self.toothNumber
                    formVC.condition = self.condition
                    formVC.patientSign = self.signatureView.signatureImage()
                    formVC.dentistSign = self.dentistSignatureView.signatureImage()
                    formVC.patient = self.patient
                    self.navigationController?.pushViewController(formVC, animated: true)
                })
            })
        }
    }
    func checkToothNumber(_ completion: @escaping (_ toothNumber: String)->Void) {
        if initials4.isSigned() {
            ToothNumberView.popUpView().showWith(self.toothNumber, inViewController: self, completion: { (popUpView: ToothNumberView, toothNumbers : String?) in
                completion(toothNumbers == nil ? "" : toothNumbers!)
            })
        } else {
            completion("")
        }
    }
    func checkProcedutreCondition(_ completion: @escaping (_ procedure: String, _ condition: String)->Void) {
        if initials3.isSigned() {
            ProcedureConditionView.popUpView().showWith(self.condition, procedure: self.procedure, inViewController: self, completion: { (popUpView: ProcedureConditionView, procedure: String?, condition: String?) in
                completion(procedure == nil ? "" : procedure!, condition == nil ? "" : condition!)
            })
        } else {
            completion("", "")
        }
    }

    var selectedNone: Bool {
        get {
            return (!initials1.isSigned() && !initials2.isSigned() && !initials3.isSigned() && !initials4.isSigned() && !initials5.isSigned() && initials6.isSigned() && initials7.isSigned())
        }
    }
}
