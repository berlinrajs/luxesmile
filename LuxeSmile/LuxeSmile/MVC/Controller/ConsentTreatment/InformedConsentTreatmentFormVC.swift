//
//  InformedConsentTreatmentFormVC.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 15/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InformedConsentTreatmentFormVC: PDViewController {

    var initial1: UIImage!
    var initial2: UIImage!
    var initial3: UIImage!
    var initial4: UIImage!
    var initial5: UIImage!
    var initial6: UIImage!
    var initial7: UIImage!
    
    @IBOutlet weak var doctorName1: UILabel!
     @IBOutlet weak var doctorName2: UILabel!
     @IBOutlet weak var doctorName3: UILabel!
     @IBOutlet weak var doctorName4: UILabel!
    
    
    var procedure: String!
    var toothNumber: String!
    var condition: String!
    
    var patientSign: UIImage!
    var dentistSign: UIImage!
    
    @IBOutlet weak var initial1View: UIImageView!
    @IBOutlet weak var initial2View: UIImageView!
    @IBOutlet weak var initial3View: UIImageView!
    @IBOutlet weak var initial4View: UIImageView!
    @IBOutlet weak var initial5View: UIImageView!
    @IBOutlet weak var initial6View: UIImageView!
    @IBOutlet weak var initial7View: UIImageView!
    
    @IBOutlet weak var labelProcedure: UILabel!
    @IBOutlet weak var labelToothNumber: UILabel!
    @IBOutlet weak var labelCondition: UILabel!
    
    @IBOutlet weak var patientSignView: UIImageView!
    @IBOutlet weak var dentistSignView: UIImageView!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDentistName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        doctorName1.text = patient.doctorName
        doctorName2.text = patient.doctorName
        doctorName3.text = patient.doctorName
        doctorName4.text = patient.doctorName
        
        
        initial1View.image = initial1
        initial2View.image = initial2
        initial3View.image = initial3
        initial4View.image = initial4
        initial5View.image = initial5
        initial6View.image = initial6
        initial7View.image = initial7
        
        labelProcedure.text = procedure.value
        labelToothNumber.text = toothNumber.value
        labelCondition.text = condition.value
        patientSignView.image = patientSign
        dentistSignView.image = dentistSign
        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName
        labelDentistName.text = patient.doctorName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
