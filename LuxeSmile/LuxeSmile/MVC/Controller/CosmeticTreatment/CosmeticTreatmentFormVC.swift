//
//  CosmeticTreatmentFormVC.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 15/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CosmeticTreatmentFormVC: PDViewController {

    var signature: UIImage!
    var witnessSign: UIImage!
    
    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var witnessSignView: UIImageView!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTooth1: UILabel!
    @IBOutlet weak var labelTooth2: UILabel!
    @IBOutlet weak var labelTooth3: UILabel!
    
    @IBOutlet weak var doctorName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        doctorName.text = patient.doctorName

        signatureView.image = signature
        witnessSignView.image = witnessSign
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelName.text = patient.fullName
        
        patient.selectedForms.first!.toothNumber!.setTextForArrayOfLabels([labelTooth1, labelTooth2, labelTooth3])
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
