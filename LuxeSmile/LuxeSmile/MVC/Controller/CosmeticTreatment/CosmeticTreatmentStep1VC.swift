//
//  CosmeticTreatmentStep1VC.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 15/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CosmeticTreatmentStep1VC: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var witnessSignView: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buttonBack?.isHidden = isFromPreviousForm
//        labelDetails.text = labelDetails.text?.stringByReplacingOccurrencesOfString("DR. DOCTOR_NAME", withString: "DR. BURGHARDT")
        
        labelDetails.text = labelDetails.text?.replacingOccurrences(of: "DR. BURGHARDT", with: patient.doctorName)
        
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
    }
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !witnessSignView.isSigned() {
            let alert = Extention.alert("PLEASE GET SIGNATURE FROM WITNESS")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            let form = self.storyboard?.instantiateViewController(withIdentifier: "kCosmeticTreatmentFormVC") as! CosmeticTreatmentFormVC
            form.signature = signatureView.signatureImage()
            form.witnessSign = witnessSignView.signatureImage()
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
