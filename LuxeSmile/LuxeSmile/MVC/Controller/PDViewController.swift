//
//  PDViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class PDViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: PDButton?
    @IBOutlet var buttonBack: PDButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var isFromPatientIntakeFrom: Bool = false
    
    let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let consentStoryBoard: UIStoryboard = UIStoryboard(name: "Consent", bundle: Bundle.main)
    let PatientInTakeStoryBoard: UIStoryboard = UIStoryboard(name: "PatientInTake", bundle: Bundle.main)
    let newConsentStoryBoard: UIStoryboard = UIStoryboard(name: "NewConsent", bundle: Bundle.main)

    var patient: PDPatient!
    var isFromPreviousForm: Bool {
        get {
            #if AUTO
                if navigationController!.viewControllers.count > 3 && (navigationController!.viewControllers[2].isKind(of: VerificationViewController.self) == true) {
                    navigationController!.viewControllers.remove(at: 2)
                    return false
                }
            #endif
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    
    func showAlert(_ message: String) {
        let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlert(_ message: String, completion: @escaping (_ completed: Bool) -> Void) {
        let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            completion(true)
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        buttonSubmit?.backgroundColor = UIColor.green
        
        self.buttonBack?.isHidden = self.buttonSubmit == nil && isFromPreviousForm
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonBackAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonPressed (_ sender : UIButton){
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true
                
                pdfManager.uploadToGoogleDrive(self.pdfView != nil ? self.pdfView! : self.view, patient: self.patient, completionBlock: { (finished) -> Void in
                    if finished {
                        self.patient.selectedForms.removeFirst()
                        self.gotoNextForm()
                    } else {
                        self.buttonSubmit?.isHidden = false
                        self.buttonBack?.isHidden = false
                    }
                })
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
            }
        }
    }
    
    func gotoNextForm() {
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
//        if formNames.contains(kNewPatientForm){
//            let parentInfoVC = mainStoryBoard.instantiateViewControllerWithIdentifier("kParentInfoVC") as! ParentInfoViewController
//            parentInfoVC.patient = patient
//            self.navigationController?.pushViewController(parentInfoVC, animated: true)
//        }else
        if formNames.contains(kNewPatientIntakeForm) || formNames.contains(kPatientIntakeForm){
            let medicalHistoryStep1VC = PatientInTakeStoryBoard.instantiateViewController(withIdentifier: "kPatientInTakeAddressInfoVC") as! AddressInfoVC
            medicalHistoryStep1VC.patient = patient
            self.navigationController?.pushViewController(medicalHistoryStep1VC, animated: true)
        }else if formNames.contains(kMedicalHistoryForm){
            let medicalHistoryStep1VC = mainStoryBoard.instantiateViewController(withIdentifier: "kMedicalHistoryStep1VC") as! MedicalHistoryStep1ViewController
            medicalHistoryStep1VC.patient = patient
            self.navigationController?.pushViewController(medicalHistoryStep1VC, animated: true)
        }else if formNames.contains(kInsuranceCard) {
            let cardCapture = consentStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let cardCapture = consentStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            self.navigationController?.pushViewController(cardCapture, animated: true)
        }else if formNames.contains(kPrivacyPractice){
            let privacy = mainStoryBoard.instantiateViewController(withIdentifier: "Privacy1VC") as! PrivacyPractice1ViewController
            privacy.patient = patient
            self.navigationController?.pushViewController(privacy, animated: true)
        }else if formNames.contains(kCreditCard){
            let creditCardStep1VC = mainStoryBoard.instantiateViewController(withIdentifier: "kCreditCardAuthorizationStep1VC") as! CreditCardAuthorizationStep1ViewController
            creditCardStep1VC.patient = self.patient
            self.navigationController?.pushViewController(creditCardStep1VC, animated: true)
        }else if formNames.contains(kDentalRecords){
            let parentInfoVC = mainStoryBoard.instantiateViewController(withIdentifier: "kParentInfo1VC") as! ParentInfo1ViewController
            parentInfoVC.patient = patient
            self.navigationController?.pushViewController(parentInfoVC, animated: true)
        }else if formNames.contains(kdentalRelease){
            let dentalRelease = mainStoryBoard.instantiateViewController(withIdentifier: "DentalRelease1VC") as! DentalRelease1ViewController
            dentalRelease.patient = patient
            self.navigationController?.pushViewController(dentalRelease, animated: true)
        }else if formNames.contains(kImplantConsult){
            let implantConsultationStep1VC = PatientInTakeStoryBoard.instantiateViewController(withIdentifier: "kImplantConsultationStep1VC") as! ImplantConsultationStep1ViewController
            implantConsultationStep1VC.patient = self.patient
            self.navigationController?.pushViewController(implantConsultationStep1VC, animated: true)
        }else if formNames.contains(kMedicalClearance){
            let medicalClearanceStep1VC = mainStoryBoard.instantiateViewController(withIdentifier: "kMedicalClearanceStep1VC") as! MedicalClearanceStep1ViewController
            medicalClearanceStep1VC.patient = self.patient
            self.navigationController?.pushViewController(medicalClearanceStep1VC, animated: true)
        }else if formNames.contains(kReleaseHealthCare){
            let releaseStep1VC = mainStoryBoard.instantiateViewController(withIdentifier: "kReleaseHealthCareInfoStep1VC") as! ReleaseHealthCareInfoStep1VC
            releaseStep1VC.patient = self.patient
            self.navigationController?.pushViewController(releaseStep1VC, animated: true)
        }else if formNames.contains(kPersonalRepresentative){
            let personalRep = mainStoryBoard.instantiateViewController(withIdentifier: "kPersonalRepresentativeStep1VC") as! PersonalRepresentativeStep1VC
            personalRep.patient = self.patient
            self.navigationController?.pushViewController(personalRep, animated: true)
        }else if formNames.contains(kRefusalDentalTreatment) {
            let refusalOfDentalTreatmentVC = mainStoryBoard.instantiateViewController(withIdentifier: "kRefusalOfDentalVC") as! RefusalOfDentalTreatmentViewController
            refusalOfDentalTreatmentVC.patient = self.patient
            self.navigationController?.pushViewController(refusalOfDentalTreatmentVC, animated: true)
        }else if formNames.contains(kCrownOrBridgeVC) {
            let CrownOrBridge = mainStoryBoard.instantiateViewController(withIdentifier: "kCrownOrBridgeVC") as! CrownOrBridgeVC
            CrownOrBridge.patient = self.patient
            self.navigationController?.pushViewController(CrownOrBridge, animated: true)
        }else if formNames.contains(kFinancialPolicy) {
            let financialPolicy = PatientInTakeStoryBoard.instantiateViewController(withIdentifier: "kFinancialPolicyVC") as! FinancialPolicyVC
            financialPolicy.patient = self.patient
            self.navigationController?.pushViewController(financialPolicy, animated: true)
        }else if formNames.contains(KInHouseBleaching) {
            let refusalOfDentalTreatmentVC = PatientInTakeStoryBoard.instantiateViewController(withIdentifier: "InHouseBleachingVc") as! InHouseBleachingVc
            refusalOfDentalTreatmentVC.patient = self.patient
            self.navigationController?.pushViewController(refusalOfDentalTreatmentVC, animated: true)
        } else if formNames.contains(kCosmeticTreatment) {
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "kCosmeticTreatmentStep1VC") as! CosmeticTreatmentStep1VC
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        }else if formNames.contains(kImplantConsent) {
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "kImplantConsultStep1VC") as! ImplantConsultStep1VC
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        }else if formNames.contains(kImplantSurgery) {
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "ImplantSurgeryVC") as! ImplantSurgeryViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        }else if formNames.contains(kPostInstructions) {
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "PostInstructionVC") as! PostInstructionViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        }else if formNames.contains(kProcelain) {
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "ProcelainVC") as! ProcelainViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        }else if formNames.contains(kPeriodontalCleaning) {
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "kPeriodontalCleaningStep1VC") as! PeriodontalCleaningStep1VC
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kOperativeInstructions) {
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "kOperativeInstructionsStep1VC") as! OperativeInstructionsStep1VC
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kConsentTreatment) {
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "kInformedConsentTreatmentStep1VC") as! InformedConsentTreatmentStep1VC
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        }else if formNames.contains(kDigitalPhotos1) || formNames.contains(kDigitalPhotos2) {
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "DigitalPhotosVC") as! DigitalPhotosViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        }else if formNames.contains(kInformedRefusal) {
            let step1VC = PatientInTakeStoryBoard.instantiateViewController(withIdentifier: "kInformedRefusalVC1") as! InformedRefusalTreatmentVC1
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        }else if formNames.contains(kInformedRadioGraph) {
            let step1VC = PatientInTakeStoryBoard.instantiateViewController(withIdentifier: "kInformedRadioVC1") as! InformedRefusalRadioVC1
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kNitrousOxide) {
            let step1VC = newConsentStoryBoard.instantiateViewController(withIdentifier: "kNitrousOxideVC") as! NitrousOxideViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kRetainerInfo) {
            let step1VC = newConsentStoryBoard.instantiateViewController(withIdentifier: "kRetainerInfoVC") as! RetainerInfoViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kToothAlignment) {
            let step1VC = newConsentStoryBoard.instantiateViewController(withIdentifier: "kToothAlignmentVC") as! ToothAlignmentViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kResultAcceptance) {
            let step1VC = newConsentStoryBoard.instantiateViewController(withIdentifier: "kResultAcceptanceVC") as! ResultAcceptanceViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kQuickTreatment) {
            let step1VC = newConsentStoryBoard.instantiateViewController(withIdentifier: "kQuickTreatmentStep1VC") as! QuickTreatmentStep1ViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kImproveAspects) {
            let step1VC = newConsentStoryBoard.instantiateViewController(withIdentifier: "kimproveAspects1VC") as! ImproveAspectsStep1VC
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kExamForm) {
            let step1VC = newConsentStoryBoard.instantiateViewController(withIdentifier: "kExamFormStep1VC") as! ExamFormStep1ViewController
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        }  else if formNames.contains(kWarrantyForm) {
            let formVC = newConsentStoryBoard.instantiateViewController(withIdentifier: "kWarrantyVC") as! WarrantyVC
            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }else if formNames.contains(kFeedBack) {
            let step1VC = mainStoryBoard.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
