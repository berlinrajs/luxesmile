//
//  ProcelainFormViewController.swift
//  LuxeSmile
//
//  Created by Bala Murugan on 7/15/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ProcelainFormViewController: PDViewController {
    
    
    @IBOutlet weak var labelContent1: UILabel!

    @IBOutlet weak var labelContent2: UILabel!
    var signatureImage : UIImage!
    var signatureDoctor : UIImage!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var imageViewSignature : UIImageView!
    @IBOutlet weak var imageViewDoctor : UIImageView!


    @IBOutlet weak var dateLabel: FormLabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = patient.fullName
        imageViewSignature.image = signatureImage
        imageViewDoctor.image = signatureDoctor
        dateLabel.text = patient.dateToday
        
        labelContent1.text = labelContent1.text?.replacingOccurrences(of: "Dr Burghardt", with: patient.doctorName)
        
        labelContent2.text = labelContent2.text?.replacingOccurrences(of: "Dr Burghardt", with: patient.doctorName)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
