//
//  ProcelainViewController.swift
//  LuxeSmile
//
//  Created by Bala Murugan on 7/15/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ProcelainViewController: PDViewController {
    
    
    
    @IBOutlet weak var ContentLabel: UILabel!

    @IBOutlet weak var signatureViewPatient : SignatureView!
    @IBOutlet weak var signatureViewDoctor : SignatureView!

    @IBOutlet weak var labelDate1: DateLabel!
    
    @IBOutlet weak var labelDate2: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        ContentLabel.text = ContentLabel.text?.replacingOccurrences(of: "Dr Burghardt", with: patient.doctorName)
    
        // Do any additional setup after loading the view.
        
        labelDate1.todayDate = patient.dateToday
        
         labelDate2.todayDate = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if !signatureViewPatient.isSigned() || !signatureViewDoctor.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "ProcelainFormVC") as! ProcelainFormViewController
            step1VC.signatureImage = signatureViewPatient.signatureImage()
            step1VC.signatureDoctor = signatureViewDoctor.signatureImage()
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
            
        }
    }

}
