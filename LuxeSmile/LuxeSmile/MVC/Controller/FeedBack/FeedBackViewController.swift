//
//  FeedBackViewController.swift
//  DistinctiveDentalCare
//
//  Created by Berlin Raj on 25/08/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FeedBackViewController: PDViewController {

    var fullName: String!
    var phoneNumber: String!
    
    @IBOutlet weak var textViewComment: PDTextView!
    @IBOutlet weak var buttonAllowMessage: UIButton!
    @IBOutlet weak var buttonAnonymous: UIButton!
    @IBOutlet weak var viewRating: UIView!
    
    var ratingView : HCSStarRatingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ratingView = HCSStarRatingView(frame: viewRating.bounds)
        ratingView.allowsHalfStars = true
        ratingView.emptyStarImage = UIImage(named: "ReviewNoStar")
        ratingView.halfStarImage = UIImage(named: "ReviewHalfStar")
        ratingView.filledStarImage = UIImage(named: "ReviewFullStar")
        ratingView.spacing = 5.0
        ratingView.maximumValue = 5
        ratingView.backgroundColor = UIColor.clear
        viewRating.addSubview(ratingView)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonActionBack(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSubmitAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            BRProgressHUD.show()
            ServiceManager.postReview(fullName, comment: textViewComment.text == "Please share your thoughts. Let us know if there is anything we can improve on." ? "" : textViewComment.text!, rating: ratingView.value, phoneNumber: phoneNumber, allowMessage: self.buttonAllowMessage.isSelected, email: "", anonymous: buttonAnonymous.isSelected, completion: { (success, error) in
                BRProgressHUD.hide()
                if success {
                    self.showAlert(self.ratingView.value >= 4.0 && self.buttonAllowMessage.isSelected == true ? "Thanks for your feedback, Please check your phone" : "Thanks for your feedback", completion: { (completed) in
                        if completed {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    })
                } else {
                    self.showAlert(error!.localizedDescription)
                }
            })
        }
    }
    @IBAction func buttonAllowAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}
extension FeedBackViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if textViewComment.text == "" {
            textViewComment.text = "Please share your thoughts. Let us know if there is anything we can improve on."
            textViewComment.textColor = UIColor.lightGray
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textViewComment.text == "" || textViewComment.text == "Please share your thoughts. Let us know if there is anything we can improve on." {
            textViewComment.text = ""
            textViewComment.textColor = UIColor.black
        }
    }
}
