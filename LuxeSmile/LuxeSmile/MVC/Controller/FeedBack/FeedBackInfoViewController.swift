//
//  FeedBackInfoViewController.swift
//  DistinctiveDentalCare
//
//  Created by Berlin Raj on 29/08/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FeedBackInfoViewController: PDViewController {

    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldPhoneNumber: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonActionBack(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            self.showAlert("PLEASE ENTER YOUR FIRST NAME")
        } else if textFieldLastName.isEmpty {
            self.showAlert("PLEASE ENTER YOUR LAST NAME")
        } else if textFieldPhoneNumber.isEmpty || !textFieldPhoneNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID PHONE NUBMER")
        } else {
            let feedBackVC = self.storyboard?.instantiateViewController(withIdentifier: "kFeedBackViewController") as! FeedBackViewController
            feedBackVC.fullName = textFieldFirstName.text! + " " + textFieldLastName.text!
            feedBackVC.phoneNumber = textFieldPhoneNumber.text!.phoneNumber
            self.navigationController?.pushViewController(feedBackVC, animated: true)
        }
    }
}
extension FeedBackInfoViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhoneNumber {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
