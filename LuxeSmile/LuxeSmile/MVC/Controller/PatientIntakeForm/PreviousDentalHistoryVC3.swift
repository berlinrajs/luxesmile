//
//  PreviousDentalHistoryVC1.swift
//  LuxeSmile
//
//  Created by SRS on 18/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//kPreviousDentalHistoryVC3

import UIKit

class PreviousDentalHistoryVC3: PDViewController {
    
    
    @IBOutlet weak var textFieldToldByMedicalDoctor: PDTextField!
    
    
    @IBOutlet weak var textFieldAnyCardiac: PDTextField!
    
    @IBOutlet weak var textFieldAnyAllergic: PDTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setValues() {
        patient.Dental3HaveyouBeendoctor = textFieldToldByMedicalDoctor.text
        patient.Dental3AnyCardiac = textFieldAnyCardiac.text
        patient.Dental3AnyAllergic = textFieldAnyAllergic.text
    }
    
    func loadValues() {
        textFieldToldByMedicalDoctor.text = patient.Dental3HaveyouBeendoctor
        textFieldAnyCardiac.text = patient.Dental3AnyCardiac
        textFieldAnyAllergic.text = patient.Dental3AnyAllergic
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func NextBtnPressed(_ sender: AnyObject) {
        
        
        setValues()
        
        
        let appoinmentVC = self.storyboard?.instantiateViewController(withIdentifier: "kAppointmentVC") as! AppoinmentVC
        appoinmentVC.patient = patient
        self.navigationController?.pushViewController(appoinmentVC, animated: true)
        
        
        
        
    }
    
}

extension PreviousDentalHistoryVC3 : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

