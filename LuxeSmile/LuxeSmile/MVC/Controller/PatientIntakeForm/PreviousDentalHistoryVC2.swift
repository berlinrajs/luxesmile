//
//  MedicalHistoryStep1ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PreviousDentalHistoryVC2: PDViewController {

    var selectedButton : RadioButton!
    var fetchCompleted : Bool! = true
    
    @IBOutlet weak var tableViewQuestions: UITableView!
  
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    
    @IBOutlet weak var buttonVerified: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if self.fetchCompleted == true {
//            if let _ = self.findEmptyValue() {
//                let alert = Extention.alert("MISSING INFORMATION.\nPLEASE SELECT YES OR NO")
//                self.presentViewController(alert, animated: true, completion: nil)
//            } else {
            if !buttonVerified.isSelected {
                let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                self.present(alert, animated: true, completion: nil)
            } else {
                
//                patient.dentalHistory2 = self.patient.dentalHistory2
                
                
//                let dentalHistoryVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPreviousDentalHistoryVC3") as! PreviousDentalHistoryVC3
//                dentalHistoryVC.patient = patient
//                self.navigationController?.pushViewController(dentalHistoryVC, animated: true)
                
                let appoinmentVC = self.storyboard?.instantiateViewController(withIdentifier: "kAppointmentVC") as! AppoinmentVC
                appoinmentVC.patient = patient
                self.navigationController?.pushViewController(appoinmentVC, animated: true)

               
            }
        }
    }

    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        
        let obj = self.patient.dentalHistory2[selectedButton.tag]
        if !textViewAnswer.isEmpty && textViewAnswer.text != "IF YES TYPE HERE" {
            obj.answer = textViewAnswer.text
            obj.selectedOption = true
            

            
        } else {
            selectedButton.isSelected = false
            obj.selectedOption = false
        }
        textViewAnswer.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
    
    
    func findEmptyValue() -> PDQuestion? {
        for question in self.patient.dentalHistory2 {
            if question.selectedOption == nil {
                return question
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension PreviousDentalHistoryVC2 : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "IF YES TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        if textView.text == "TYPE HERE*" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "IF YES TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
        
        if textView.text.isEmpty {
            textView.text = "TYPE HERE*"
            textView.textColor = UIColor.lightGray
        }

    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension PreviousDentalHistoryVC2 : PreviousDentalHistoryCellDelegate {
   
    func radioButtonAction(_ sender: RadioButton) {
        
        selectedButton = sender
        
            let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
            self.viewPopup.frame = frameSize
            self.viewPopup.center = self.view.center
            self.viewShadow.addSubview(self.viewPopup)
            self.viewPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            textViewAnswer.text = "IF YES TYPE HERE"
            textViewAnswer.textColor = UIColor.lightGray
            self.viewShadow.isHidden = false
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.viewPopup.transform = CGAffineTransform.identity
            UIView.commitAnimations()
     
    }
}


extension PreviousDentalHistoryVC2 : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.patient.dentalHistory2.count
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPreviouDentalHistory", for: indexPath) as! PreviousDentalHistoryCell
        let obj = self.patient.dentalHistory2[indexPath.row]
        cell.configureCell(obj)
        cell.buttonYes.tag = indexPath.row
        cell.buttonNo.tag = indexPath.row
        cell.delegate = self
        return cell
        
        
    }
}

extension PreviousDentalHistoryVC2 : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
      
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


