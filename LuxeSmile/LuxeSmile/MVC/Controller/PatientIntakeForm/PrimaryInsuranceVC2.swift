//
//  NewPatient3ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrimaryInsuranceVC2: PDViewController {
    
    @IBOutlet weak var textFieldSubscriberId : PDTextField!
  
    @IBOutlet weak var textFieldCoPhone: PDTextField!
    
    @IBOutlet weak var textFieldEmployerName: PDTextField!
    
    @IBOutlet weak var textfieldAccountNumber : PDTextField!
 

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
     
        loadValues()        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadValues() {
        textFieldCoPhone.text = patient.InsuranceCoPhone
        textFieldSubscriberId.text = patient.InsuranceSubsciberID
        textFieldEmployerName.text = patient.InsuranceGroupName
        textfieldAccountNumber.text = patient.InsuranceAccountNum
    }
    
    func setValues() {
        patient.InsuranceCoPhone = textFieldCoPhone.text
        patient.InsuranceSubsciberID = textFieldSubscriberId.text
        patient.InsuranceGroupName = textFieldEmployerName.text
        patient.InsuranceAccountNum = textfieldAccountNumber.text
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    

    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        
        
        if !textFieldCoPhone.isEmpty && !textFieldCoPhone.text!.isPhoneNumber {
            
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
            
        }  else {
            setValues()
            
            
            let dentalHistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "kPreviousDentalHistoryVC1") as! PreviousDentalHistoryVC1
            dentalHistoryVC.patient = patient
            self.navigationController?.pushViewController(dentalHistoryVC, animated: true)
            
        }
    }
    
 
    
}



extension PrimaryInsuranceVC2 : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
         if textField == textFieldCoPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

