//
//  PatientInTakeForm.swift
//  LuxeSmile
//
//  Created by SRS on 18/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInTakeForm: PDViewController {
    
    @IBOutlet weak var patientName: FormLabel!
    
    @IBOutlet weak var DOB: FormLabel!
    @IBOutlet weak var address: FormLabel!
    @IBOutlet weak var City: FormLabel!
    
    @IBOutlet weak var State: FormLabel!
    @IBOutlet weak var zip: FormLabel!
    
    @IBOutlet weak var homePhone: FormLabel!
    @IBOutlet weak var WorkPhone: FormLabel!
    @IBOutlet weak var CellPhone: FormLabel!
    
    @IBOutlet weak var email: FormLabel!
    
//    @IBOutlet weak var socialSecurity: FormLabel!
    
    @IBOutlet weak var RadioGender: RadioButton!
    
    @IBOutlet weak var ReasonForAppoinment: FormLabel!
    
    
    @IBOutlet weak var HowdidYouHearAboutUs: FormLabel!
    
    @IBOutlet weak var AnyFamilyMembers: FormLabel!
    
    @IBOutlet weak var RadioDoYouHaveInsurance: RadioButton!
    
    @IBOutlet weak var InsuranceCompany: FormLabel!
    
    @IBOutlet weak var SubscriberName: FormLabel!
    
    @IBOutlet weak var SubscriberDOB: FormLabel!
    
    @IBOutlet weak var RelationToSubscriber: FormLabel!
    
    @IBOutlet weak var SubscriberId: FormLabel!
    
    @IBOutlet weak var insuranceCoPhone: FormLabel!
    
    @IBOutlet weak var EmployerName: FormLabel!
    
    @IBOutlet weak var AccountNumber: FormLabel!
    
    
    //dental history
    
    @IBOutlet weak var sawADentist: FormLabel!
    
    @IBOutlet weak var CleaningOrProblem: FormLabel!
    
    @IBOutlet weak var RegularDentalcleaning: FormLabel!
    
    @IBOutlet weak var ScaleAboutSmile: FormLabel!
    
    @IBOutlet weak var ChangeAbtSmile: FormLabel!
    
    @IBOutlet weak var anyPartialDenture: FormLabel!
    
    @IBOutlet weak var doYouGrind: FormLabel!
    
    @IBOutlet weak var anyDiscomfort: FormLabel!
    
    @IBOutlet weak var doYouGumsBleed: FormLabel!
    
    // premed ques
    @IBOutlet weak var HaveYouToldMedicalDoctor: FormLabel!
    @IBOutlet weak var DoYouHaveAnyCardiac: FormLabel!
    @IBOutlet weak var DoYouHaveanyAllergic: FormLabel!
    
    //Appoinment
    @IBOutlet weak var Appoinmentdate: FormLabel!
    @IBOutlet weak var AppoinmentTime: FormLabel!
    @IBOutlet weak var patientPrintName: UILabel!
    @IBOutlet weak var PatientSign: UIImageView!
    @IBOutlet weak var dateOfSign: UILabel!
    
    @IBOutlet weak var viewMedicalHistory: UIView!
    @IBOutlet weak var viewAcknowledgment: UIView!
    @IBOutlet weak var viewFinancialPolicy: UIView!

    @IBOutlet weak var labelReferredBy: FormLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self .loadValues()
        
        // add it to the view hierarchie
        let medicalHistoryFormVC = mainStoryBoard.instantiateViewController(withIdentifier: "kMedicalHistoryFormVC") as! MedicalHistoryFormViewController
        medicalHistoryFormVC.patient = self.patient
        medicalHistoryFormVC.isFromPatientIntakeFrom = true
        self.addChildViewController(medicalHistoryFormVC)
        viewMedicalHistory.addSubview(medicalHistoryFormVC.view)
        medicalHistoryFormVC.didMove(toParentViewController: self)
        
        let privacy = mainStoryBoard.instantiateViewController(withIdentifier: "PrivacyFormVC") as! PrivacyPracticeFormViewController
        privacy.patient = self.patient
        privacy.isFromPatientIntakeFrom = true
        self.addChildViewController(privacy)
        viewAcknowledgment.addSubview(privacy.view)
        privacy.didMove(toParentViewController: self)
        
        let financialForm = self.storyboard?.instantiateViewController(withIdentifier: "kFinancialPolicyFormVC") as! FinancialPolicyForm
        financialForm.patient = self.patient
        financialForm.isFromPatientIntakeFrom = true
        self.addChildViewController(financialForm)
        viewFinancialPolicy.addSubview(financialForm.view)
        financialForm.didMove(toParentViewController: self)
    }
    
    func loadValues (){
        patientName.text = patient.fullName
        DOB.text = patient.dateOfBirth
        address.text = patient.PatientIntakeaddressLine?.value
        City.text = patient.PatientIntakecity?.value
        State.text = patient.PatientIntakestate?.value
        zip.text = patient.PatientIntakezipCode?.value
        homePhone.text = patient.PatientIntakephoneNumber?.value
        WorkPhone.text = patient.PatientIntakeWorkPhone?.value
        CellPhone.text = patient.PatientIntakeCellPhone?.value
        email.text = patient.PatientIntakeemail?.value
//        socialSecurity.text = patient.PatientIntakesocialSecurityNumber?.value
        
        RadioGender.setSelectedWithTag(patient.PatientGenderTag!)
        ReasonForAppoinment.text = patient.PatientIntakeAppoinment?.value
        
        if patient.ReferedBywhomeTag == 8 || patient.ReferedBywhomeTag == 9 {
            HowdidYouHearAboutUs.text = patient.othersDetails?.value
        } else {
            HowdidYouHearAboutUs.text = patient.referredByDetails?.value
        }
        
        AnyFamilyMembers.text = patient.PatientIntakeFamilyMember?.value
        
        RadioDoYouHaveInsurance.setSelectedWithTag(patient.PatientInsuranceTag!)
        
        InsuranceCompany.text = patient.InsuranceCoName
        SubscriberName.text = patient.InsuranceSubscriberName?.value
        SubscriberDOB.text = patient.InsuranceDOB?.value
        RelationToSubscriber.text = patient.InsuranceRelation?.value
        SubscriberId.text = patient.InsuranceSubsciberID?.value
        insuranceCoPhone.text = patient.InsuranceCoPhone?.value
        EmployerName.text = patient.InsuranceGroupName?.value
        AccountNumber.text = patient.InsuranceAccountNum?.value
        
        sawADentist.text = patient.DentalHistoryHowLongSawADentistCount + "  " + patient.DentalHistoryHowLongSawADentistPeriod
        
        CleaningOrProblem.text = patient.DentalCleaningOrProbString?.value
        RegularDentalcleaning.text = patient.DentalHistoryHaveYouRegularWithDental?.value
        ScaleAboutSmile.text = patient.DentalHistoryOnAScale?.value
        ChangeAbtSmile.text = patient.DentalHistoryChangeAnyAboutSmile?.value
        anyPartialDenture.text = patient.dentalHistory2[0].answer?.value
        
        doYouGrind.text =  patient.dentalHistory2[1].answer?.value
        anyDiscomfort.text =  patient.dentalHistory2[2].answer?.value
        doYouGumsBleed.text =  patient.dentalHistory2[3].answer?.value
        
        // premed ques
        HaveYouToldMedicalDoctor.text   = patient.premedQues[0].answer?.value
        DoYouHaveAnyCardiac.text = patient.premedQues[1].answer?.value
        DoYouHaveanyAllergic.text = patient.premedQues[2].answer?.value
        
        Appoinmentdate.text = patient.AppoinmetDate?.value
        AppoinmentTime.text = patient.AppoinmetTime?.value
        patientPrintName.text = patient.fullName
        dateOfSign.text = patient.dateToday
        
        PatientSign.image = patient.AppointmentSign
        labelReferredBy.text = patient.whoReferred
    }
    
    #if AUTO
    override func onSubmitButtonPressed(_ sender: UIButton) {
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        BRProgressHUD.show()
        ServiceManager.sendPatientDetails(patient: patient, completion: { (success, error) in
            BRProgressHUD.hide()
            if success {
                let formNames = (self.patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
                let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: formNames.contains(kNewPatientIntakeForm) ? "DETAILS SUBMITTED SUCCESSFULLY" : "DETAILS UPDATED", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    super.onSubmitButtonPressed(sender)
                }
                alertController.addAction(alertOkAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
                let alert = Extention.alert(error!.localizedDescription.uppercased())
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    #endif
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
