//
//  AddressInfoViewController.swift
//  Luxe Smile
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AddressInfoVC: PDViewController {
    
    
    @IBOutlet weak var textFieldAddressLine: PDTextField!
    
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var textFieldEmail: PDTextField!
    @IBOutlet weak var textFieldPhone: PDTextField!
//    @IBOutlet weak var textFieldSecurityNumber: PDTextField!
    
    var arrayStates : [String] = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.patient.dentalHistory2.count == 0 {
            PDQuestion.fetchQuestionsPreviousDentalForm2 { (result, success) -> Void in
                if success {
                    self.patient.dentalHistory2.append(contentsOf: result!)
                }
            }
        }
        if self.patient.premedQues.count == 0 {
            PDQuestion.fetchQuestionsPremedQuesForm { (result, success) -> Void in
                if success {
                    self.patient.premedQues.append(contentsOf: result!)
                }
            }
        }
        if self.patient.medicalHistoryQuestions1.count == 0 {
            PDQuestion.fetchQuestionsForm1 { (result, success) -> Void in
                if success {
                    self.patient.medicalHistoryQuestions1.append(contentsOf: result!)
                }
            }
        }
        if self.patient.medicalHistoryQuestions2.count == 0 {
            PDOption.fetchQuestionsForm2 { (result, success) -> Void in
                if success {
                    self.patient.medicalHistoryQuestions2.append(contentsOf: result!)
                }
            }
        }
        if self.patient.medicalHistoryQuestions3.count == 0 {
            PDOption.fetchQuestionsForm3 { (result, success) -> Void in
                if success {
                    self.patient.medicalHistoryQuestions3.append(contentsOf: result!)
                    let objOthers = PDOption(value: "Others")
                    self.patient.medicalHistoryQuestions3.append(objOthers)
                }
            }
        }
        if self.patient.medicalHistoryQuestions4.count == 0 {
            PDOption.fetchQuestionsForm4 { (result, success) -> Void in
                if success {
                    self.patient.medicalHistoryQuestions4.append(contentsOf: result!)
                }
            }
        }
        
        
        
//        textFieldSecurityNumber.placeholder = is18YearsOld ? "SOCIAL SECURITY NUMBER *" : "SOCIAL SECURITY NUMBER"
        
        StateListView.addStateListForTextField(textFieldState)
        #if AUTO
            if let patientDetails = patient.patientDetails {
                textFieldCity.text = patient.PatientIntakecity == nil ? patientDetails.city : patient.PatientIntakecity
                textFieldState.text = patient.PatientIntakestate == nil ? patientDetails.state : patient.PatientIntakestate
                textFieldZipCode.text = patient.PatientIntakezipCode == nil ? patientDetails.zipCode : patient.PatientIntakezipCode
                textFieldAddressLine.text = patient.PatientIntakeaddressLine == nil ? patientDetails.address : patient.PatientIntakeaddressLine
                textFieldPhone.text = patient.PatientIntakephoneNumber == nil ? patientDetails.homePhone : patient.PatientIntakephoneNumber
                textFieldEmail.text = patient.PatientIntakeemail == nil ? patientDetails.email : patient.PatientIntakeemail
//                textFieldSecurityNumber.text = patient.PatientIntakesocialSecurityNumber == nil ? patientDetails.socialSecurityNumber : patient.PatientIntakesocialSecurityNumber
            } else {
                loadValues()
            }
        #else
            loadValues()
        #endif
        
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        textFieldAddressLine.text = patient.PatientIntakeaddressLine
        if let state = patient.PatientIntakestate, !state.isEmpty {
            textFieldState.text = state
        }
//        textFieldSecurityNumber.text = patient.PatientIntakesocialSecurityNumber
        
        textFieldZipCode.text = patient.PatientIntakezipCode
        textFieldCity.text = patient.PatientIntakecity
        textFieldEmail.text = patient.PatientIntakeemail
        textFieldPhone.text = patient.PatientIntakephoneNumber
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    func setValues() {
        patient.PatientIntakeaddressLine = textFieldAddressLine.text
        patient.PatientIntakestate = textFieldState.text
//        if !textFieldSecurityNumber.isEmpty {
//            patient.PatientIntakesocialSecurityNumber = textFieldSecurityNumber.text
//        } else {
//            patient.PatientIntakesocialSecurityNumber = nil
//        }
        
        patient.PatientIntakezipCode = textFieldZipCode.text
        patient.PatientIntakecity = textFieldCity.text
        patient.PatientIntakeemail = textFieldEmail.text?.lowercased()
        patient.PatientIntakephoneNumber = textFieldPhone.text
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
//        } else if (!textFieldSecurityNumber.isEmpty || is18YearsOld) && !textFieldSecurityNumber.text!.isSocialSecurityNumber {
//            let alert = Extention.alert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
//            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER VALID EMAIL")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            let contactInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInTakeAddressInfoVC2") as! AddressInfoVC2
            contactInfoVC.patient = patient
            self.navigationController?.pushViewController(contactInfoVC, animated: true)
        }
    }
        
    func findEmptyTextField() -> UITextField? {
        let textFields = patient.is18YearsOld ? [textFieldCity, textFieldState, textFieldZipCode, textFieldAddressLine] : [textFieldCity, textFieldState, textFieldZipCode, textFieldAddressLine]
        for textField in textFields {
            if textField!.isEmpty {
                return textField
            }
        }
        return nil
    }
}

extension AddressInfoVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
        
//        if textField == textFieldSecurityNumber {
//            return textField.formatSocialSecurityNumber(range, string: string)
//        }
        
//        if textField == textFieldSecurityNumber {
//        
//        return textField.forSocialSecurityNumbers(range, string: string)
//        
//        }
        
        if textField == textFieldPhone {
           
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
