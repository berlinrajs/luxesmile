//
//  NewPatient3ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrimaryInsuranceVC: PDViewController {
    
    @IBOutlet weak var textfieldInsuranceCompanyName : PDTextField!
  
    @IBOutlet weak var textFieldSubscibername: PDTextField!
    
    @IBOutlet weak var textFieldInsuredDOB: PDTextField!
    
    @IBOutlet weak var textfieldRelationship : PDTextField!
 
    
    @IBOutlet weak var insuredPersonBtn: RadioButton!
    
    @IBOutlet var datePicker: UIDatePicker!
    
    @IBOutlet var toolBar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
//     
//        
//        datePicker.maximumDate = nil
//        textFieldInsuredDOB.inputView = datePicker
        
        
      
        DateInputView.addDatePickerForTextField(textFieldInsuredDOB)
        
        loadValues()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadValues() {
        textfieldInsuranceCompanyName.text = patient.InsuranceCoName
        textFieldSubscibername.text = patient.InsuranceSubscriberName
        textFieldInsuredDOB.text = patient.InsuranceDOB
        textfieldRelationship.text = patient.InsuranceRelation
        if textfieldRelationship.text == "SELF" {
            insuredPersonBtn.isSelected = true
        }
    }
    
    func setValues() {
        patient.InsuranceCoName = textfieldInsuranceCompanyName.text
        patient.InsuranceSubscriberName = textFieldSubscibername.text
        patient.InsuranceDOB = textFieldInsuredDOB.text
        patient.InsuranceRelation = textfieldRelationship.text
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }

    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        
        
        if textfieldInsuranceCompanyName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER INSURANCE COMPANY NAME")
            self.present(alert, animated: true, completion: nil)
        }  else {
            
            setValues()
            
            
            let InsuranceVC = self.storyboard?.instantiateViewController(withIdentifier: "kPrimaryInsuranceVC2") as! PrimaryInsuranceVC2
            InsuranceVC.patient = patient
            self.navigationController?.pushViewController(InsuranceVC, animated: true)
            
        }
    }
    
    @IBAction func radioInsuredPersonAction (_ sender : RadioButton){
        
        if sender.tag == 1{
          
            textFieldSubscibername.text = patient.fullName
            textFieldInsuredDOB.text = patient.dateOfBirth
            textfieldRelationship.text = "SELF"

        }else{
            
            textFieldSubscibername.text = ""
            textFieldInsuredDOB.text = ""
            textfieldRelationship.text = ""
            
           
        }
    }
    
    
    
//    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
//        
//        textFieldInsuredDOB.resignFirstResponder()
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldInsuredDOB.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//        
//    }
//    
//    @IBAction func datePickerDateChanged(sender: AnyObject) {
//        
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldInsuredDOB.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//        
//    }
    
}



extension PrimaryInsuranceVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

