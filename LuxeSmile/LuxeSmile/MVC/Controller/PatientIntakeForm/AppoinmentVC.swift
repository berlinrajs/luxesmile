//
//  AppoinmentVC.swift
//  LuxeSmile
//
//  Created by SRS on 18/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//kAppointmentVC

import UIKit

class AppoinmentVC: PDViewController {
    
    
    @IBOutlet weak var signature: SignatureView!
    
    @IBOutlet weak var labelDate: DateLabel!
    
    @IBOutlet weak var pickATime: PDTextField!
    
    @IBOutlet weak var appointmentTime: PDTextField!
    
    
    var selectedButton : RadioButton!
    var fetchCompleted : Bool! = true
    
    @IBOutlet weak var tableViewQuestions: UITableView!
    
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    
    @IBOutlet weak var buttonVerified: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        
        // Do any additional setup after loading the view.
        // DateInputView.addDatePickerForTextField(appointmentTime)
        
        PickADayInputView.addPickTimePickerForTextField(appointmentTime)
        
        DateAndTimeInputView.addDatePickerForTextField(pickATime)
        loadValues()
    }
    
    func loadValues() {
        appointmentTime.text = patient.AppoinmetDate
        pickATime.text = patient.AppoinmetTime
    }
    
    func setValues() {
        patient.AppoinmetDate = appointmentTime.text
        patient.AppoinmetTime = pickATime.text
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func DoneBtnAction(_ sender: AnyObject) {
        
        
        if self.fetchCompleted == true {
            
            if !buttonVerified.isSelected {
                let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                self.present(alert, animated: true, completion: nil)
            }
            else if !signature.isSigned() {
                
                let alert = Extention.alert("PLEASE SIGN THE FORM")
                self.present(alert, animated: true, completion: nil)
                
            }else if labelDate.text == "Tap to date" {
                let alert = Extention.alert("PLEASE SELECT DATE")
                self.present(alert, animated: true, completion: nil)
            } else {
                
                setValues()
                
                patient.AppointmentSign = signature.signatureImage()
                
                let medicalHistoryStep1VC = mainStoryBoard.instantiateViewController(withIdentifier: "kMedicalHistoryStep1VC") as! MedicalHistoryStep1ViewController
                medicalHistoryStep1VC.patient = patient
                medicalHistoryStep1VC.isFromPatientIntakeFrom = true
                self.navigationController?.pushViewController(medicalHistoryStep1VC, animated: true)
                
                
                
            }
            
        }
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        
        let obj = self.patient.premedQues[selectedButton.tag]
        if !textViewAnswer.isEmpty && textViewAnswer.text != "IF YES TYPE HERE" {
            obj.answer = textViewAnswer.text
            obj.selectedOption = true
            
            
            
        } else {
            selectedButton.isSelected = false
            obj.selectedOption = false
        }
        textViewAnswer.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
    
    
    func findEmptyValue() -> PDQuestion? {
        for question in self.patient.premedQues {
            if question.selectedOption == nil {
                return question
            }
        }
        return nil
    }
    
    
}

extension AppoinmentVC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "IF YES TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        if textView.text == "TYPE HERE*" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "IF YES TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
        
        if textView.text.isEmpty {
            textView.text = "TYPE HERE*"
            textView.textColor = UIColor.lightGray
        }
        
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension AppoinmentVC : PreviousDentalHistoryCellDelegate {
    
    func radioButtonAction(_ sender: RadioButton) {
        
        selectedButton = sender
        
        let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
        self.viewPopup.frame = frameSize
        self.viewPopup.center = self.view.center
        self.viewShadow.addSubview(self.viewPopup)
        self.viewPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textViewAnswer.text = "IF YES TYPE HERE"
        textViewAnswer.textColor = UIColor.lightGray
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopup.transform = CGAffineTransform.identity
        UIView.commitAnimations()
        
    }
}


extension AppoinmentVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.patient.premedQues.count
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPreviouDentalHistory", for: indexPath) as! PreviousDentalHistoryCell
        let obj = self.patient.premedQues[indexPath.row]
        cell.configureCell(obj)
        cell.buttonYes.tag = indexPath.row
        cell.buttonNo.tag = indexPath.row
        cell.delegate = self
        return cell
        
        
    }
}

extension AppoinmentVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

class PickADayInputView: UIView {
    var toolbar: UIToolbar!
    var pickerMonth : UIPickerView!
    var textField: UITextField!
    
    
    var arrayMonths = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        pickerMonth = UIPickerView(frame: frame)
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        let barButtonClear = UIBarButtonItem(title: "Clear", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPressed))
        toolbar.items = [barButtonClear, UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        self.addSubview(pickerMonth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func cancelPressed() {
        textField.resignFirstResponder()
        textField.text = ""
    }
    
    func donePressed() {
        let string1 = arrayMonths[pickerMonth.selectedRow(inComponent: 0)]
        textField.text = string1
        textField.resignFirstResponder()
    }
    
    class func addPickTimePickerForTextField(_ textField: UITextField) {
        let monthListView = PickADayInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = monthListView
        textField.inputAccessoryView = monthListView.toolbar
        monthListView.textField = textField
    }
}

extension PickADayInputView : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //  let string1 = arrayMonths[row]
        
        //let index1 = string1.startIndex.advancedBy(3)
        
        // textField.text = string1.substringWithRange(string1.startIndex ..< string1.startIndex.advancedBy(3))
        
        textField.text = arrayMonths[row]
    }
}



