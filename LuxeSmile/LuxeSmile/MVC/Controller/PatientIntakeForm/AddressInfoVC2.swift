//
//  AddressInfoViewController.swift
//  Luxe Smile
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AddressInfoVC2: PDViewController {

    @IBOutlet weak var textFieldReasonForAppoinment: PDTextField!
    
    @IBOutlet weak var textFieldFamilyMembers: PDTextField!

    @IBOutlet weak var textFieldWorkPhone: PDTextField!
    
    @IBOutlet weak var textFieldCellPhone: PDTextField!
    
    @IBOutlet weak var RadioGender: RadioButton!

    @IBOutlet weak var RadioHowDidYouHearAboutUs: RadioButton!
    
    @IBOutlet weak var RadioDoYouHaveInsurance: RadioButton!
    
    @IBOutlet weak var textFieldWhoReferred: MCTextField!
    
//    var arrayStates : [String] = [String]()
    
//    var referenceDetails : String!
//    var othersDetails : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        #if AUTO
            if let patientDetails = patient.patientDetails {
                if let genderTag = patient.PatientGenderTag {
                    RadioGender.setSelectedWithTag(genderTag)
                } else if let gender = patientDetails.gender {
                    RadioGender.setSelectedWithTag(gender.index)
                }
                textFieldCellPhone.text = patient.PatientIntakeCellPhone == nil ? patientDetails.cellPhone : patient.PatientIntakeCellPhone
                textFieldWorkPhone.text = patient.PatientIntakeWorkPhone == nil ? patientDetails.workPhone :  patient.PatientIntakeWorkPhone
            } else {
                loadValues()
            }
        #else
            loadValues()
        #endif
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func radioReferedByTag(_ sender: AnyObject) {
        
        
        switch sender.tag {
        case 1:
            patient.referredByDetails = "YELLOW PAGES"
        case 2:
            patient.referredByDetails = "GOOGLE"
        case 3:
            patient.referredByDetails = "YAHOO"
        case 4:
            patient.referredByDetails = "DEXKNOWS"
        case 5:
            patient.referredByDetails = "WALK IN/DRIVE BY"
        case 6:
            patient.referredByDetails = "INSURANCE"
        case 7:
            patient.referredByDetails = "MAILER"
        case 8:
            
          
            PopupTextField.sharedInstance.showWithTitle("", placeHolder: "REFERRED BY", keyboardType: UIKeyboardType.default, textFormat: .default, inViewController: nil, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    
                    self.patient.othersDetails = textField.text
                    
                }else{
                    self.patient.othersDetails = ""
                    self.RadioHowDidYouHearAboutUs.deselectAllButtons()
                }
            })
            
            
        case 9:
            
            PopupTextField.sharedInstance.showWithTitle("", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.default, textFormat: .default, inViewController: nil, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    
                    self.patient.othersDetails = textField.text
                    
                }else{
                    self.patient.othersDetails = ""
                    self.RadioHowDidYouHearAboutUs.deselectAllButtons()
                }
            })
            
        default:
            patient.referredByDetails = "OTHER"
        }
    }
    
    func loadValues() {
        textFieldWorkPhone.text = patient.PatientIntakeWorkPhone
        textFieldCellPhone.text = patient.PatientIntakeCellPhone
        textFieldReasonForAppoinment.text = patient.PatientIntakeAppoinment
        textFieldFamilyMembers.text = patient.PatientIntakeFamilyMember
        textFieldWhoReferred.text = patient.whoReferred
        
        if let genderTag = patient.PatientGenderTag {
            RadioGender.setSelectedWithTag(genderTag)
        }
        if let whomeTag = patient.ReferedBywhomeTag {
            RadioHowDidYouHearAboutUs.setSelectedWithTag(whomeTag)
        }
        if let insuranceTag = patient.PatientInsuranceTag {
            RadioDoYouHaveInsurance.setSelectedWithTag(insuranceTag)
        }
    }
    
    func setValues() {
        patient.PatientIntakeWorkPhone = textFieldWorkPhone.text
        patient.PatientIntakeCellPhone = textFieldCellPhone.text
        patient.PatientIntakeAppoinment = textFieldReasonForAppoinment.text
        patient.PatientIntakeFamilyMember = textFieldFamilyMembers.text
        patient.whoReferred = textFieldWhoReferred.text!
        
        if let selectedButton = RadioGender.selected {
            patient.PatientGenderTag = selectedButton.tag
        }
        if let selectedButton = RadioHowDidYouHearAboutUs.selected {
            patient.ReferedBywhomeTag = selectedButton.tag
        }
        if let selectedButton = RadioDoYouHaveInsurance.selected {
            patient.PatientInsuranceTag = selectedButton.tag
        }
    }

    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        
         if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
         }else if !textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
         } else if RadioGender.selected == nil{
            let alert = Extention.alert("PLEASE SELECT THE GENDER")
            self.present(alert, animated: true, completion: nil)
         }else if RadioDoYouHaveInsurance.selected == nil{
            let alert = Extention.alert("PLEASE SELECT THE INSURANCE OPTION")
            self.present(alert, animated: true, completion: nil)
         } else {
            
            setValues()
            
            if patient.PatientInsuranceTag == 1 {
                
                let InsuranceVC = self.storyboard?.instantiateViewController(withIdentifier: "kPrimaryInsuranceVC") as! PrimaryInsuranceVC
                InsuranceVC.patient = patient
                self.navigationController?.pushViewController(InsuranceVC, animated: true)
                
            } else {
                
                let dentalHistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "kPreviousDentalHistoryVC1") as! PreviousDentalHistoryVC1
                dentalHistoryVC.patient = patient
                self.navigationController?.pushViewController(dentalHistoryVC, animated: true)
                
                
            }
        }
    }
}

extension AddressInfoVC2 : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        
        if textField == textFieldCellPhone {
           
            return textField.formatPhoneNumber(range, string: string)
        }
        
        if textField == textFieldWorkPhone {
            
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
