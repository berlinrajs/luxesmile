//
//  PreviousDentalHistoryVC1.swift
//  LuxeSmile
//
//  Created by SRS on 18/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//kPreviousDentalHistoryVC1

import UIKit

class PreviousDentalHistoryVC1: PDViewController {
    
    
    @IBOutlet weak var textFieldLastSawDentistCount: PDTextField!
    
     @IBOutlet weak var textFieldLastSawDentistPeriod: PDTextField!
    
    @IBOutlet weak var ForCleaningOrProblem: RadioButton!
    
    @IBOutlet weak var textFieldHaveYouRegular: PDTextField!
    
    @IBOutlet weak var textFieldScale: PDTextField!
    
    @IBOutlet weak var textFieldChangeAnythingSmile: PDTextView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        CountInputView.addCountPickerForTextField(textFieldLastSawDentistCount)
        
        PeriodInputView.addPeriodPickerForTextField(textFieldLastSawDentistPeriod)
        
        SmilePicker.addSmilePickerForTextField(textFieldScale)
        loadValues()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func ActionForCleaningOrProb(_ sender: AnyObject) {
        
        
        if sender.tag == 1 {
        
       patient.DentalCleaningOrProbString = "CLEANING"
        
        } else if sender.tag == 2{
        
            patient.DentalCleaningOrProbString = "PROBLEM"

        
        } 
        
    }
    
    func loadValues() {
        if let text = patient.DentalHistoryChangeAnyAboutSmile, !text.isEmpty {
            textFieldChangeAnythingSmile.text = text
            textFieldChangeAnythingSmile.textColor = UIColor.black
        }
        
        textFieldLastSawDentistCount.text = patient.DentalHistoryHowLongSawADentistCount
        textFieldLastSawDentistPeriod.text = patient.DentalHistoryHowLongSawADentistPeriod
        textFieldHaveYouRegular.text = patient.DentalHistoryHaveYouRegularWithDental
        textFieldScale.text = patient.DentalHistoryOnAScale
        if let tag = patient.DentalHistoryCleaningOrProb {
            ForCleaningOrProblem.setSelectedWithTag(tag)
        }
    }

    
    func setValues() {
        patient.DentalHistoryHowLongSawADentistCount = textFieldLastSawDentistCount.text
        
        patient.DentalHistoryHowLongSawADentistPeriod = textFieldLastSawDentistPeriod.text
        
        patient.DentalHistoryHaveYouRegularWithDental = textFieldHaveYouRegular.text
        patient.DentalHistoryOnAScale = textFieldScale.text
        patient.DentalHistoryChangeAnyAboutSmile = textFieldChangeAnythingSmile.text == "TYPE HERE" ? "" : textFieldChangeAnythingSmile.text
        patient.DentalHistoryCleaningOrProb = ForCleaningOrProblem.selected == nil ? 0 : ForCleaningOrProblem.selected.tag
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func NextBtnPressed(_ sender: AnyObject) {
        
        if !textFieldLastSawDentistCount.isEmpty && ForCleaningOrProblem.selected == nil {
        let alert =
            Extention.alert("PLEASE SELECT FOR CLEANING OR PROBLEM OPTION")
            
            self.present(alert, animated: true, completion: nil)
        
        }  else {
        
            setValues()
            
            
            let dentalHistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "kPreviousDentalHistoryVC2") as! PreviousDentalHistoryVC2
            dentalHistoryVC.patient = patient
            self.navigationController?.pushViewController(dentalHistoryVC, animated: true)

        
        
        }
        
        
        
    }

}

extension PreviousDentalHistoryVC1 : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldScale {
            return textField.formatScale(range, string: string)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

class PeriodInputView: UIView {
    var toolbar: UIToolbar!
    var pickerMonth : UIPickerView!
    var textField: UITextField!
    
    
    var arrayMonths = ["Days","Week","Month","Year"]
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        pickerMonth = UIPickerView(frame: frame)
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        let barButtonClear = UIBarButtonItem(title: "Clear", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPressed))
        toolbar.items = [barButtonClear, UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        self.addSubview(pickerMonth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func cancelPressed() {
        textField.resignFirstResponder()
        textField.text = ""
    }
    
    func donePressed() {
        let string1 = arrayMonths[pickerMonth.selectedRow(inComponent: 0)]
         textField.text = string1
        textField.resignFirstResponder()
    }
    
    class func addPeriodPickerForTextField(_ textField: UITextField) {
        let monthListView = PeriodInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = monthListView
        textField.inputAccessoryView = monthListView.toolbar
        monthListView.textField = textField
    }
}

extension PeriodInputView : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //  let string1 = arrayMonths[row]
        
        //let index1 = string1.startIndex.advancedBy(3)
        
        // textField.text = string1.substringWithRange(string1.startIndex ..< string1.startIndex.advancedBy(3))
        
        textField.text = arrayMonths[row]
    }
}

class SmilePicker: UIView {
    var toolbar: UIToolbar!
    var pickerMonth : UIPickerView!
    var textField: UITextField!
    
    
    var arrayScale = ["1"]
    
    
    override init(frame: CGRect) {
        
        
        for i in 2...10 {
            
            let myString = String(i)
            arrayScale.append(myString)
        }
        
        super.init(frame: frame)
        pickerMonth = UIPickerView(frame: frame)
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        let barButtonClear = UIBarButtonItem(title: "Clear", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPressed))
        toolbar.items = [barButtonClear, UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        self.addSubview(pickerMonth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func cancelPressed() {
        textField.resignFirstResponder()
        textField.text = ""
    }
    
    func donePressed() {
        let string1 = arrayScale[pickerMonth.selectedRow(inComponent: 0)]
        textField.text = string1
        textField.resignFirstResponder()
    }
    
    class func addSmilePickerForTextField(_ textField: UITextField) {
        let monthListView = SmilePicker(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = monthListView
        textField.inputAccessoryView = monthListView.toolbar
        monthListView.textField = textField
    }
}

extension SmilePicker : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayScale.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayScale[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //  let string1 = arrayMonths[row]
        
        //let index1 = string1.startIndex.advancedBy(3)
        
        // textField.text = string1.substringWithRange(string1.startIndex ..< string1.startIndex.advancedBy(3))
        
        textField.text = arrayScale[row]
    }
}

extension PreviousDentalHistoryVC1 : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return textView.text.characters.count + (text.characters.count - range.length) <= 50
    }
}


