//
//  RecordsTransferViewController.swift
//  FusionDental
//
//  Created by Office on 3/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RecordsTransferViewController: PDViewController {

    @IBOutlet weak var textFieldPreviousClinic: PDTextField!

    @IBOutlet weak var textFieldAddressLine: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var textFieldWorkPhone: PDTextField!
    @IBOutlet weak var labelDate: PDLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBack?.isHidden = navigationController?.viewControllers.count > 4 ? true : false
        
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(RecordsTransferViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        StateListView.addStateListForTextField(textFieldState)
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.black
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
        patient.previousClinicName = textFieldPreviousClinic.text
        patient.city = textFieldCity.text
        patient.state = textFieldState.text
        patient.zipCode = textFieldZipCode.text
        patient.addressLine = textFieldAddressLine.text
        patient.phoneNumber = textFieldWorkPhone.text
    }
    
    func loadValues() {
        textFieldPreviousClinic.text = patient.previousClinicName
        textFieldCity.text = patient.city
        if let state = patient.state {
            textFieldState.text = state
        }
        textFieldZipCode.text = patient.zipCode
        textFieldAddressLine.text = patient.addressLine
        textFieldWorkPhone.text = patient.phoneNumber
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID OFFICE PHONE")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            patient.signature1 = signatureView.signatureImage()
            let dentalRecordsFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kDentalRecordsFormVC") as! RecordsTransferFormViewController
            dentalRecordsFormVC.patient = patient
            self.navigationController?.pushViewController(dentalRecordsFormVC, animated: true)
        }
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldPreviousClinic, textFieldCity, textFieldState, textFieldZipCode, textFieldAddressLine]
        for textField in textFields {
            if textField!.isEmpty {
                return textField
            }
        }
        return nil
    }

}


extension RecordsTransferViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        } else if textField == textFieldWorkPhone{
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
}
