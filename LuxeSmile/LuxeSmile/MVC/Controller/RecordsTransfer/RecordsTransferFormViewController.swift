//
//  RecordsTransferFormViewController.swift
//  FusionDental
//
//  Created by Office on 3/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class RecordsTransferFormViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()
    
    
    @IBOutlet var labelText1: UILabel!
    @IBOutlet var labelText2: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet var labelDate: UILabel!

    @IBOutlet weak var PatientSignName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        PatientSignName.text = patient.fullName
        
        var patientInfo = "Patient Name: "
        
        let patientName = getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " Date of Birth: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateToday)))
        
        let attributedString1 = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString1.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        labelText1.attributedText = attributedString1
        
        textRanges.removeAll()
        var patientDetails = "Previous Dentist or Practice Name:"
        
        patientDetails = patientDetails + " \(getText(patient.previousClinicName))"
        textRanges.append(patientDetails.rangeOfText(getText(patient.previousClinicName)))
        
        
        patientDetails = patientDetails + " Address: \(getText(patient.addressLine))"
        textRanges.append(patientDetails.rangeOfText(getText(patient.addressLine)))
        
        patientDetails = patientDetails + " City: \(getText(patient.city))"
        textRanges.append(patientDetails.rangeOfText(getText(patient.city)))
        
        patientDetails = patientDetails + " State: \(getText(patient.state))"
        textRanges.append(patientDetails.rangeOfText(getText(patient.state)))
        
        patientDetails = patientDetails + " Zip Code: \(getText(patient.zipCode))"
        textRanges.append(patientDetails.rangeOfText(getText(patient.zipCode)))
        
        patientDetails = patientDetails + " Office Phone number: \(getText(patient.phoneNumber))"
        textRanges.append(patientDetails.rangeOfText(getText(patient.phoneNumber)))
        
        let attributedString2 = NSMutableAttributedString(string: patientDetails)
        for range in textRanges {
            attributedString2.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment = NSTextAlignment.justified
        attributedString2.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedString2.length))
        labelText2.attributedText = attributedString2
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
