//
//  ParentInfoViewController.swift
//  ProDental
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ParentInfo1ViewController: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldRelation: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
        loadValues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValues() {
        textFieldFirstName.text = patient.parentFirstName
        textFieldLastName.text = patient.parentLastName
        textFieldRelation.text = patient.relation
    }
    
    func setValues() {
        patient.parentFirstName = textFieldFirstName.text
        patient.parentLastName = textFieldLastName.text
        patient.relation = textFieldRelation.text
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER FIRST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER LAST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldRelation.isEmpty {
            let alert = Extention.alert("PLEASE ENTER RELATIONSHIP")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            let dentalRecordsVC = self.storyboard?.instantiateViewController(withIdentifier: "kDentalRecordsVC") as! RecordsTransferViewController
            dentalRecordsVC.patient = self.patient
            self.navigationController?.pushViewController(dentalRecordsVC, animated: true)
        }
    }


}
