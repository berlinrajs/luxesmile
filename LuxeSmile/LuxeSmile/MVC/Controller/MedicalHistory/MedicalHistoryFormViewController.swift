//
//  MedicalHistoryFormViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryFormViewController: PDViewController {

    
//    @IBOutlet weak var constraintTableViewForm1Height: NSLayoutConstraint!
//    @IBOutlet weak var constraintTableViewForm4Height: NSLayoutConstraint!
    
    @IBOutlet weak var PatientName: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBirthDate: UILabel!
    @IBOutlet weak var labelDateCreated: UILabel!

    @IBOutlet weak var buttonAlergicOthers: UIButton!
    @IBOutlet weak var labelAlergicOthers: PDLabel!
    
    @IBOutlet weak var radioButtonSubstances: RadioButton!
    @IBOutlet weak var labelSubstances: PDLabel!
    
    @IBOutlet weak var radioButtonIllness: RadioButton!
    @IBOutlet weak var labelIllness: PDLabel!
    
    @IBOutlet weak var textViewComments: PDTextView!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet var arrayButtons: [UIButton]!
    @IBOutlet weak var logoWithAddress: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //constraintTableViewForm1Height.constant = CGFloat(patient.medicalHistoryQuestions1.count * 26)
       // constraintTableViewForm4Height.constant = (19 * 14) + 18
        
        PatientName.text = patient.fullName

        labelName.text = "Patient Name: \(patient.fullName)"
        labelBirthDate.text = "Birth Date: \(patient.dateOfBirth)"
        labelDateCreated.text = "Date Created: \(patient.dateToday)"
        
        if let alergic = patient.othersTextForm3 {
            buttonAlergicOthers.isSelected = true
            labelAlergicOthers.text = " " + alergic
        }
        if let substances = patient.controlledSubstances {
            radioButtonSubstances.isSelected = true
            labelSubstances.text = " " + substances
        } else if patient.controlledSubstancesClicked != nil {
            radioButtonSubstances.isSelected = false
        }
        
        if let illness = patient.otherIllness {
            radioButtonIllness.isSelected = true
            labelIllness.text = " " + illness
        } else {
            radioButtonIllness.isSelected = false
        }
        
        if let comment = patient.comments {
            textViewComments.text = comment
        }
        
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        for (idx, buttonQuestion) in self.arrayButtons.enumerated() {
            let obj = patient.medicalHistoryQuestions2[idx]
            buttonQuestion.setTitle(" " + obj.question, for: UIControlState())
            if let selected = obj.isSelected {
                buttonQuestion.isSelected = selected
            } else {
                buttonQuestion.isSelected = false
            }
        }
        
        if isFromPatientIntakeFrom {
            buttonSubmit?.isHidden = true
            buttonBack?.isHidden = true
            logoWithAddress.isHidden = true
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MedicalHistoryFormViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 1 ? patient.medicalHistoryQuestions2.count : patient.medicalHistoryQuestions3.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMedicalHistory", for: indexPath) as! MedicalHistoryCollectionViewCell
        let obj = collectionView.tag == 1 ? patient.medicalHistoryQuestions2[indexPath.item] : patient.medicalHistoryQuestions3[indexPath.item]
        cell.configureCellOption(obj)
        return cell
    }
}

extension MedicalHistoryFormViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return patient.medicalHistoryQuestions1.count
        } else {
            let currentIndex = tableView.tag - 2
            let objects = patient.medicalHistoryQuestions4.filter { (obj) -> Bool in
                return obj.index >= (currentIndex * 19) && obj.index < ((currentIndex + 1) * 19)
            }
            return tableView.tag == 5 ? objects.count + 1 : objects.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryFrom1", for: indexPath) as! MedicalHistoryFormTableViewCell1
            let obj = patient.medicalHistoryQuestions1[indexPath.row]
            cell.configureCell(obj)
            return cell
        } else {
            let currentIndex = tableView.tag - 2
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryFrom4", for: indexPath) as! MedicalHistoryStep1TableViewCell
            let index = (currentIndex * 19) + indexPath.row
            let obj = patient.medicalHistoryQuestions4[index]
            cell.configureCellOption(obj)
            let stringObj = ["AIDS/HIV Positive", "Artificial heart valve", "Artificial Joints", "Cancer", "Chemotherapy", "Diabetes", "Drug Addiction", "Excessive Bleeding", "Heart Attack / Failure", "Heart Pacemaker", "Heart Trouble / Disease", "Hepatites A", "Hepatites B or C", "High Blood Pressure", "Tuberculosis"]
            if let selected = obj.isSelected {
                cell.labelTitle.textColor = selected && stringObj.contains(obj.question) ? UIColor.red : UIColor.black
                cell.labelTitle.font = UIFont(name:selected ? "WorkSans-Medium" :  "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.isSelected = selected
            } else {
                cell.labelTitle.textColor = UIColor.black
                cell.labelTitle.font = UIFont(name: "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.deselectAllButtons()
            }
            return cell
        }
    }
}
