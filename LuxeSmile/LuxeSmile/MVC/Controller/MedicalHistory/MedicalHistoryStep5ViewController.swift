//
//  MedicalHistoryStep5ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep5ViewController: PDViewController {
    
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: PDLabel!
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewOthers: PDTextView!
    @IBOutlet weak var textViewComments: PDTextView!
    
    @IBOutlet weak var buttonNext: PDButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MedicalHistoryStep5ViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        if isFromPatientIntakeFrom {
            buttonNext?.setTitle("NEXT", for: UIControlState())
        }
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        if let comments = patient.comments, !comments.isEmpty {
            textViewComments.text = comments
            textViewComments.textColor = UIColor.black
        }
        
        if let _ = patient.otherIllness {
            radioButton.isSelected = true
        }
    }
    
    func setValues() {
        if textViewComments.text != "COMMENTS" {
            patient.comments = textViewComments.text
        } else {
            patient.comments = nil
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.black
    }
    
    @IBAction func buttonActionSubmit(_ sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            patient.signature1 = signatureView.signatureImage()
            if isFromPatientIntakeFrom {
                let privacy = mainStoryBoard.instantiateViewController(withIdentifier: "Privacy1VC") as! PrivacyPractice1ViewController
                privacy.patient = patient
                privacy.isFromPatientIntakeFrom = isFromPatientIntakeFrom
                self.navigationController?.pushViewController(privacy, animated: true)
            } else {
                let medicalHistoryFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalHistoryFormVC") as! MedicalHistoryFormViewController
                medicalHistoryFormVC.patient = patient
                self.navigationController?.pushViewController(medicalHistoryFormVC, animated: true)
            }
        }
    }
    
    func showPopup(_ tag : Int) {
        let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
        self.viewPopup.frame = frameSize
        self.viewPopup.center = self.view.center
        self.viewShadow.addSubview(self.viewPopup)
        self.viewPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textViewOthers.text = "IF YES TYPE HERE"
        textViewOthers.tag = tag
        textViewOthers.textColor = UIColor.lightGray
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopup.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionYesNo(_ sender: RadioButton) {
        if sender.tag == 2 {
            textViewOthers.resignFirstResponder()
            self.viewPopup.removeFromSuperview()
            self.viewShadow.isHidden = true
            patient.otherIllness = nil
        } else {
            showPopup(2)
        }
    }
    
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        if !textViewOthers.isEmpty && textViewOthers.text != "IF YES TYPE HERE" {
            patient.otherIllness = textViewOthers.text
        } else {
            patient.otherIllness = nil
            radioButton.setSelectedWithTag(2)
        }
        textViewOthers.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
}

extension MedicalHistoryStep5ViewController : UITextViewDelegate {
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == textViewOthers {
            if textView.text == "IF YES TYPE HERE" {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        } else {
            if textView.text == "COMMENTS" {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = textView == textViewOthers ? "IF YES TYPE HERE" : "COMMENTS"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return textView.text.characters.count + (text.characters.count - range.length) <= 50
    }
}
