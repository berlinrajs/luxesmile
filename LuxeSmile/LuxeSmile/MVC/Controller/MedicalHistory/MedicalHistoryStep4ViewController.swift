//
//  MedicalHistoryStep4ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep4ViewController: PDViewController {

    var currentIndex : Int = 0
    var fetchCompleted : Bool! = true
    
    @IBOutlet weak var buttonNext: PDButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonVerified: UIButton!
    
      @IBOutlet weak var buttonBackOutlet: PDButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.stopAnimating()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction override func buttonBackAction(_ sender: AnyObject) {
        if currentIndex == 0 {
            super.buttonBackAction(sender)
        } else {
            
           buttonBackOutlet.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            self.buttonVerified.isSelected = true
            self.activityIndicator.startAnimating()
            let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.activityIndicator.stopAnimating()
                self.currentIndex = self.currentIndex - 1
                self.tableViewQuestions.reloadData()
                self.buttonBackOutlet.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
        }
    }
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if self.fetchCompleted == true {
            if ((currentIndex + 1) * 20) < self.patient.medicalHistoryQuestions4.count {
//                if let _ = findEmptyValue() {
//                    let alert = Extention.alert("MISSING INFORMATION.\nPLEASE SELECT YES OR NO")
//                    self.presentViewController(alert, animated: true, completion: nil)
//                } else {
                if !buttonVerified.isSelected {
                    let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                    self.present(alert, animated: true, completion: nil)
                } else {
                    buttonBackOutlet.isUserInteractionEnabled = false
                    buttonNext.isUserInteractionEnabled = false
                    self.buttonVerified.isSelected = false
                    self.activityIndicator.startAnimating()
                    let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        self.activityIndicator.stopAnimating()
                        self.currentIndex = self.currentIndex + 1
                        self.tableViewQuestions.reloadData()
                        self.buttonBackOutlet.isUserInteractionEnabled = true
                        self.buttonNext.isUserInteractionEnabled = true
                    }
                }
                
            } else {
                if !buttonVerified.isSelected {
                    let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let medicalHistoryStep5VC = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalHistoryStep5VC") as! MedicalHistoryStep5ViewController
                    medicalHistoryStep5VC.patient = patient
                    medicalHistoryStep5VC.isFromPatientIntakeFrom = isFromPatientIntakeFrom
                    self.navigationController?.pushViewController(medicalHistoryStep5VC, animated: true)
                }
            }
        }
    }
    
    func findEmptyValue() -> PDOption? {
        let objects = self.patient.medicalHistoryQuestions4.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 20) && obj.index < ((currentIndex + 1) * 20)
        }
        for question in objects {
            if question.isSelected == nil {
                return question
            }
        }
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension MedicalHistoryStep4ViewController : MedicalHistoryCellDelegate {
    func radioButtonAction(_ sender: RadioButton) {
        self.tableViewQuestions.reloadData()
    }
}


extension MedicalHistoryStep4ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let objects = self.patient.medicalHistoryQuestions4.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 20) && obj.index < ((currentIndex + 1) * 20)
        }
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep4", for: indexPath) as! MedicalHistoryStep1TableViewCell
        let index = (currentIndex * 20) + indexPath.row
        let obj = self.patient.medicalHistoryQuestions4[index]
        cell.configureCellOption(obj)
        if let selected = obj.isSelected {
            cell.buttonYes.isSelected = selected
        } else {
            cell.buttonYes.deselectAllButtons()
        }
        cell.buttonYes.tag = index
        cell.buttonNo.tag = index
        cell.delegate = self
        return cell
    }
}
