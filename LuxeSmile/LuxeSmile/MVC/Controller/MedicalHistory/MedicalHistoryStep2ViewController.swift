//
//  MedicalHistoryStep2ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep2ViewController: PDViewController {
    
    
    var fetchCompleted : Bool! = true
    
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var viewTableBG: PDView!
    
    @IBOutlet weak var radioButtonAlergetic: RadioButton!
    @IBOutlet weak var tableViewAlergetic: UITableView!
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewOthers: PDTextView!
    var othersObj : PDOption!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTableBG.borderColor = UIColor.white.withAlphaComponent(0.5)
        othersObj = self.patient.medicalHistoryQuestions3.filter({ (option) -> Bool in
            return option.question == "Others"
        }).first!
        // Do any additional setup after loading the view.
        loadValues()
        
    }
    
    func loadValues() {
        viewTableBG.borderColor = patient.isWomen == true ? UIColor.white : UIColor.white.withAlphaComponent(0.5)
        radioButton.isSelected = patient.isWomen
        tableViewQuestions.reloadData()
        if let controlledSubstance = patient.controlledSubstances, !controlledSubstance.isEmpty {
            radioButtonAlergetic.isSelected = true
        } else if patient.controlledSubstancesClicked != nil && patient.controlledSubstancesClicked == true {
            radioButtonAlergetic.isSelected = false
        }
    }
    
    func findEmptyValue() -> PDOption? {
        for question in self.patient.medicalHistoryQuestions2 {
            if question.isSelected == true {
                return question
            }
        }
        return nil
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if self.fetchCompleted == true {
            func gotoNextView() {
                if patient.controlledSubstancesClicked == nil {
                    let alert = Extention.alert("PLEASE ANSWER ALL QUESTIONS")
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let medicalHistoryStep4VC = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalHistoryStep4VC") as! MedicalHistoryStep4ViewController
                    medicalHistoryStep4VC.patient = patient
                    medicalHistoryStep4VC.isFromPatientIntakeFrom = isFromPatientIntakeFrom
                    self.navigationController?.pushViewController(medicalHistoryStep4VC, animated: true)
                }
                
            }
            if radioButton.selected.tag == 1 {
                if let _ = findEmptyValue() {
                    gotoNextView()
                } else {
                    let alert = Extention.alert("IF YOU ARE WOMAN PLEASE SELECT ANY OF THE FOLLOWING")
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                gotoNextView()
            }
        }
    }
    
    @IBAction func buttonActionWomen(_ sender: RadioButton) {
        patient.isWomen = sender.tag == 1
        if sender.tag == 2 {
            for obj in self.patient.medicalHistoryQuestions2 {
                obj.isSelected = nil
            }
            viewTableBG.borderColor = UIColor.white.withAlphaComponent(0.5)
        } else {
            viewTableBG.borderColor = UIColor.white
        }
        tableViewQuestions.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        if textViewOthers.tag == 1
        {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES TYPE HERE" {
                patient.othersTextForm3 = textViewOthers.text
                othersObj.isSelected = true
                self.tableViewAlergetic.reloadData()
            }
        } else {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES TYPE HERE" {
                patient.controlledSubstances = textViewOthers.text
            } else {
                patient.controlledSubstances = nil
                radioButtonAlergetic.setSelectedWithTag(2)
            }
        }
        textViewOthers.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        patient.controlledSubstancesClicked = true
        if sender.tag == 1 {
            showPopup(2)
        }
        else
        {
            patient.controlledSubstances = nil

        }
    }
    
    func showPopup(_ tag : Int) {
        let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
        self.viewPopup.frame = frameSize
        self.viewPopup.center = self.view.center
        self.viewShadow.addSubview(self.viewPopup)
        self.viewPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textViewOthers.text = "IF YES TYPE HERE"
        textViewOthers.tag = tag
        textViewOthers.textColor = UIColor.lightGray
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopup.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
}

extension MedicalHistoryStep2ViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "IF YES TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "IF YES TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return textView.text.characters.count + (text.characters.count - range.length) <= 50
    }
}

extension MedicalHistoryStep2ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            if radioButton.selected.tag == 1 {
                if indexPath.row == 3 {
                    self.patient.medicalHistoryQuestions2[0].isSelected = false
                    self.patient.medicalHistoryQuestions2[1].isSelected = false
                    self.patient.medicalHistoryQuestions2[2].isSelected = false
                    
                    let obj = self.patient.medicalHistoryQuestions2[indexPath.row]
                    obj.isSelected = obj.isSelected != nil && obj.isSelected == true ? false : true
                } else {
                    self.patient.medicalHistoryQuestions2[3].isSelected = false
                    
                    let obj = self.patient.medicalHistoryQuestions2[indexPath.row]
                    obj.isSelected = obj.isSelected != nil && obj.isSelected == true ? false : true
                }
            }
        } else {
            let obj = self.patient.medicalHistoryQuestions3[indexPath.row]
            if obj.question == "Others" {
                if obj.isSelected == nil || obj.isSelected == false {
                    self.showPopup(1)
                } else {
                    patient.othersTextForm3 = nil
                    obj.isSelected = obj.isSelected != nil && obj.isSelected == true ? false : true
                }
            } else {
                obj.isSelected = obj.isSelected != nil && obj.isSelected == true ? false : true
            }
        }
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.tag == 1 ? 35.0 : 40.0
    }
}

extension MedicalHistoryStep2ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.tag == 1 ? self.patient.medicalHistoryQuestions2.count : self.patient.medicalHistoryQuestions3.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep2", for: indexPath) as! MedicalHistoryStep2TableViewCell
            let obj = self.patient.medicalHistoryQuestions2[indexPath.row]
            cell.buttonCheckbox.isSelected = obj.isSelected != nil && obj.isSelected == true
            cell.buttonCheckbox.isEnabled = radioButton.selected.tag == 1
            cell.labelTitle.isEnabled = radioButton.selected.tag == 1
            cell.configureCell(obj)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep3", for: indexPath) as! MedicalHistoryStep2TableViewCell
            let obj = self.patient.medicalHistoryQuestions3[indexPath.row]
            cell.buttonCheckbox.isSelected = obj.isSelected != nil && obj.isSelected == true
            cell.configureCell(obj)
            
            return cell
        }
    }
}
