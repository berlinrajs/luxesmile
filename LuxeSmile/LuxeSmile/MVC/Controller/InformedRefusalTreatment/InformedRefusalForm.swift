//
//  InformedRefusalForm.swift
//  LuxeSmile
//
//  Created by SRS on 19/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InformedRefusalForm: PDViewController {
    
    
    @IBOutlet var dentistRecomend: [FormLabel]!
    
    
    @IBOutlet var needOfService: [FormLabel]!
    
    @IBOutlet var risk: [FormLabel]!
    
    
    @IBOutlet weak var labelDate1: FormLabel!
    
    @IBOutlet weak var labelDate2: FormLabel!

    @IBOutlet weak var parentSign: UIImageView!
    
    @IBOutlet weak var guardianRelationship: FormLabel!
    
    @IBOutlet weak var witnessSign: UIImageView!
    
    
    @IBOutlet weak var btn1: UIButton!
    
    @IBOutlet weak var btn2: UIButton!
    
    @IBOutlet weak var btn3: UIButton!
    
    @IBOutlet weak var PatientSignName: UILabel!
    
     @IBOutlet weak var RadioDocName: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        // Do any additional setup after loading the view.

        
        RadioDocName.setSelectedWithTag(patient.doctorNameTag!)
        
        PatientSignName.text = patient.fullName
        
          patient.informedRefusaldentistRecomend == "" ?  patient.getTextNA.setTextForArrayOfLabels(dentistRecomend) :  patient.informedRefusaldentistRecomend.setTextForArrayOfLabels(dentistRecomend)
        
         patient.informedRefusalneedForService == "" ?  patient.getTextNA.setTextForArrayOfLabels(needOfService) :  patient.informedRefusalneedForService.setTextForArrayOfLabels(needOfService)
        
        patient.informedRefusaltextViewRisk == "" ?  patient.getTextNA.setTextForArrayOfLabels(risk) :  patient.informedRefusaltextViewRisk.setTextForArrayOfLabels(risk)
    
        
        btn1.isSelected = patient.BtnSelected1
         btn2.isSelected = patient.BtnSelected2
         btn3.isSelected = patient.BtnSelected3
        
        
        labelDate1.text = patient.todayDateTime
        labelDate2.text = patient.todayDateTime
        
        guardianRelationship.text = patient.guardianRelationship.value
        
        
        parentSign.image = patient.InformedSign1
        witnessSign.image = patient.InformedSign2
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
