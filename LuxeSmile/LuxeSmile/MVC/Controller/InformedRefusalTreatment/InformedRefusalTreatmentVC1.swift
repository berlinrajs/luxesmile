//
//  InformedRefusalTreatmentVC1.swift
//  LuxeSmile
//
//  Created by SRS on 18/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InformedRefusalTreatmentVC1: PDViewController {
    
    @IBOutlet weak var dentistRecomend: PDTextView!
    
    @IBOutlet weak var needForService: PDTextView!
    
    @IBOutlet weak var textViewRisk: PDTextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValues() {
        if let text = patient.informedRefusaldentistRecomend, !text.isEmpty {
            dentistRecomend.text = text
            dentistRecomend.textColor = UIColor.black
        }
        if let text = patient.informedRefusalneedForService, !text.isEmpty {
            needForService.text = text
            needForService.textColor = UIColor.black
        }
        if let text = patient.informedRefusaltextViewRisk, !text.isEmpty {
            textViewRisk.text = text
            textViewRisk.textColor = UIColor.black
        }
    }
    
    func setValues() {
        patient.informedRefusaldentistRecomend = dentistRecomend.text == "TYPE HERE" ? "" : dentistRecomend.text
        
        patient.informedRefusalneedForService = needForService.text == "TYPE HERE" ? "" : needForService.text
        
        patient.informedRefusaltextViewRisk = textViewRisk.text == "TYPE HERE" ? "" : textViewRisk.text
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }

    @IBAction func nextBtnAction(_ sender: AnyObject) {
      
     
         setValues()
        
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "kInformedRefusalVC2") as! InformedRefusalTreatmentVC2
        nextVC.patient = patient
        self.navigationController?.pushViewController(nextVC, animated: true)
        
    }
}


extension InformedRefusalTreatmentVC1 : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
        
        
        
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

