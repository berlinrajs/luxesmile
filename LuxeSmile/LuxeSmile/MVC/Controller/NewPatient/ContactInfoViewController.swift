//
//  ContactInfoViewController.swift
//  Luxe Smile
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ContactInfoViewController: PDViewController {
    @IBOutlet weak var textFieldEmployer: PDTextField!
    @IBOutlet weak var textFieldEmployerPhoneNumber: PDTextField!
    @IBOutlet weak var textFieldEmergencyContact: PDTextField!
    @IBOutlet weak var textFieldEmergencyContactNumber: PDTextField!
    
    @IBOutlet weak var textFieldWhoReferred: MCTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldWhoReferred.textFormat = .default
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if !textFieldEmployerPhoneNumber.isEmpty && !textFieldEmployerPhoneNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID EMPLOYER PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldEmergencyContact.isEmpty {
            let alert = Extention.alert("PLEASE ENTER EMERGENCY CONTACT")
            self.present(alert, animated: true, completion: nil)
        }  else if textFieldEmergencyContactNumber.isEmpty {
            let alert = Extention.alert("PLEASE ENTER EMERGENCY CONTACT NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldEmergencyContactNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID EMERGENCY CONTACT NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else {
            if !textFieldEmployer.isEmpty {
                patient.employerName = textFieldEmployer.text
            } else {
                patient.employerName = nil
            }
            if !textFieldEmployerPhoneNumber.isEmpty {
                patient.employerPhoneNumber = textFieldEmployerPhoneNumber.text
            } else {
                patient.employerPhoneNumber = nil
            }
            patient.emergencyContactName = textFieldEmergencyContact.text
            patient.emergencyContactPhoneNumber = textFieldEmergencyContactNumber.text
            patient.whoReferred = textFieldWhoReferred.text!
            let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kDiseaseInfoVC") as! DiseaseInfoViewController
            diseaseInfoVC.patient = patient
            self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
        }
    }
}

extension ContactInfoViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == textFieldEmergencyContactNumber || textField == textFieldEmployerPhoneNumber) {
            
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
