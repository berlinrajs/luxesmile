//
//  DiseaseInfoViewController.swift
//  Luxe Smile
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DiseaseInfoViewController: PDViewController {
    
    @IBOutlet weak var textViewReasonForTodayDentalVisit: PDTextView!
    @IBOutlet weak var textFieldLastDentalVisit: PDTextField!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!

    @IBOutlet weak var buttonConcerns: RadioButton!
    @IBOutlet weak var radioButtonPain: RadioButton!
    
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewOthers: PDTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.maximumDate = Date()

        
        textFieldLastDentalVisit.inputView = datePicker
        textFieldLastDentalVisit.inputAccessoryView = toolBar
        textFieldLastDentalVisit.placeholder = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        if !textFieldLastDentalVisit.isEmpty {
            patient.lastDentalVisit = textFieldLastDentalVisit.text
        } else {
            patient.lastDentalVisit = nil
        }
        if textViewReasonForTodayDentalVisit.text != "Briefly explain" {
            patient.reasonForTodayVisit = textViewReasonForTodayDentalVisit.text
        } else {
            patient.reasonForTodayVisit = nil
        }
        patient.isHavingPain = radioButtonPain.selected.tag == 1
        let agreementVC = self.storyboard?.instantiateViewController(withIdentifier: "kAgreementVC") as! AgreementViewController
        agreementVC.patient = patient
        self.navigationController?.pushViewController(agreementVC, animated: true)
    }
    
    
    @IBAction func buttonActionYesNo(_ sender: RadioButton) {
        if sender.tag == 1 {
            showPopup(1)
        }else{
            patient.painLocation = nil
        }
    }
    
    
    @IBAction func buttonActionConcerns(_ sender: RadioButton) {
        if sender.tag == 1 {
            showPopup(2)
        }
        else
        {
            patient.anyConcerns = nil
        }
    }
    
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        if textViewOthers.tag == 1 {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES, WHERE" {
                patient.painLocation = textViewOthers.text
            } else {
                patient.painLocation = nil
                radioButtonPain.setSelectedWithTag(2)
            }
        } else {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES TYPE HERE" {
                patient.anyConcerns = textViewOthers.text
            } else {
                patient.anyConcerns = nil
                buttonConcerns.setSelectedWithTag(2)
            }
        }
        textViewOthers.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
    
    func showPopup(_ tag : Int) {
        let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
        self.viewPopup.frame = frameSize
        self.viewPopup.center = self.view.center
        self.viewShadow.addSubview(self.viewPopup)
        self.viewPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textViewOthers.text = tag == 1 ? "IF YES, WHERE" : "IF YES TYPE HERE"
        textViewOthers.tag = tag
        textViewOthers.textColor = UIColor.lightGray
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopup.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }

    
    @IBAction func toolbarDoneButtonAction(_ sender: AnyObject) {
        textFieldLastDentalVisit.resignFirstResponder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastDentalVisit.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
    
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastDentalVisit.text = dateFormatter.string(from: datePicker.date).uppercased()
    }

}


extension DiseaseInfoViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == textViewOthers {
            if textView.text == "IF YES TYPE HERE" || textView.text == "IF YES, WHERE" {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        } else {
            if textView.text == "Briefly explain" {
                textView.text = ""
                textView.textColor = UIColor.white
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = textView == textViewOthers ? textViewOthers.tag == 1 ? "IF YES, WHERE" : "IF YES TYPE HERE" : "Briefly explain"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return textView.text.characters.count + (text.characters.count - range.length) <= 140
        
    }
}
