//
//  AddressInfoViewController.swift
//  Luxe Smile
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AddressInfoViewController: PDViewController {
    
    
    @IBOutlet weak var textFieldAddressLine: PDTextField!
    
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var textFieldEmail: PDTextField!
    @IBOutlet weak var textFieldPhone: PDTextField!
//    @IBOutlet weak var textFieldSecurityNumber: PDTextField!
    
    var arrayStates : [String] = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        textFieldSecurityNumber.placeholder = is18YearsOld ? "SOCIAL SECURITY NUMBER *" : "SOCIAL SECURITY NUMBER"
        
        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
//        } else if (!textFieldSecurityNumber.isEmpty || is18YearsOld) && !textFieldSecurityNumber.text!.isSocialSecurityNumber {
//            let alert = Extention.alert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
//            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER VALID EMAIL")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.addressLine = (textFieldAddressLine.text!)
            patient.state = textFieldState.text
//            if !textFieldSecurityNumber.isEmpty {
//                patient.socialSecurityNumber = textFieldSecurityNumber.text
//            } else {
//                patient.socialSecurityNumber = nil
//            }
            
            patient.zipCode = textFieldZipCode.text
            patient.city = textFieldCity.text
            patient.email = textFieldEmail.text?.lowercased()
            patient.phoneNumber = textFieldPhone.text
            let contactInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kContactInfoVC") as! ContactInfoViewController
            contactInfoVC.patient = patient
            self.navigationController?.pushViewController(contactInfoVC, animated: true)
        }
    }
        
    func findEmptyTextField() -> UITextField? {
        let textFields = patient.is18YearsOld ? [textFieldCity, textFieldPhone, textFieldState, textFieldZipCode, textFieldAddressLine] : [textFieldCity,  textFieldPhone, textFieldState, textFieldZipCode, textFieldAddressLine]
        for textField in textFields {
            if textField!.isEmpty {
                return textField
            }
        }
        return nil
    }
}

extension AddressInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
        
//        if textField == textFieldSecurityNumber {
//            return textField.formatSocialSecurityNumber(range, string: string)
//        }
        if textField == textFieldPhone {
           
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
