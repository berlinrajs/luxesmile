//
//  ParentInfoViewController.swift
//  Luxe Smile
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ParentInfoViewController: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.maximumDate = Date()
        textFieldDateOfBirth.inputView = datePicker
        textFieldDateOfBirth.inputAccessoryView = toolBar
        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        func gotoAddressInfoVC() {
            let addressInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kAddressInfoVC") as! AddressInfoViewController
            addressInfoVC.patient = patient
            self.navigationController?.pushViewController(addressInfoVC, animated: true)
        }
        
        self.view.endEditing(true)
        if buttonYes.isSelected {
            if textFieldFirstName.isEmpty {
                let alert = Extention.alert("PLEASE ENTER FIRST NAME")
                self.present(alert, animated: true, completion: nil)
            } else if textFieldLastName.isEmpty {
                let alert = Extention.alert("PLEASE ENTER LAST NAME")
                self.present(alert, animated: true, completion: nil)
            } else if textFieldDateOfBirth.isEmpty {
                let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
                self.present(alert, animated: true, completion: nil)
            } else {
                patient.parentFirstName = textFieldFirstName.text
                patient.parentLastName = textFieldLastName.text
                patient.parentDateOfBirth = textFieldDateOfBirth.text
                gotoAddressInfoVC()
            }
        } else {
            gotoAddressInfoVC()
        }
    }
    
    @IBAction func buttonActionYesNo(_ sender: UIButton) {
        sender.isSelected = true
        if sender == buttonYes {
            buttonNo.isSelected = false
            textFieldFirstName.isEnabled = true
            textFieldLastName.isEnabled = true
            textFieldDateOfBirth.isEnabled = true
        } else {
            buttonYes.isSelected = false
            textFieldFirstName.isEnabled = false
            textFieldLastName.isEnabled = false
            textFieldDateOfBirth.isEnabled = false
            textFieldLastName.text = ""
            textFieldFirstName.text = ""
            textFieldDateOfBirth.text = ""
        }
    }
    
    
    @IBAction func toolbarDoneButtonAction(_ sender: AnyObject) {
        textFieldDateOfBirth.resignFirstResponder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldDateOfBirth.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
    
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldDateOfBirth.text = dateFormatter.string(from: datePicker.date).uppercased()
    }

}
