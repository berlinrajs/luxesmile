//
//  HippaStep2VC.swift
//  Luxe Smile
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HippaStep2VC: PDViewController {

    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonText: UIButton!
    @IBOutlet weak var labelPhone: FormLabel!
    @IBOutlet weak var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelPhone.text = patient.phoneNumber
        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func nextAction(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            if !buttonText.isSelected && !buttonCall.isSelected {
                let alert = Extention.alert("PLEASE SELECT ANY OF THE CONTACT OPTIONS")
                self.present(alert, animated: true, completion: nil)
            } else {
                patient.signature4 = signatureView.signatureImage()
                patient.isCallAvail = buttonCall.isSelected
                patient.isTextAvail = buttonText.isSelected
                let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kHippaStep3VC") as! HippaStep3VC
                step3VC.patient = patient
                navigationController?.pushViewController(step3VC, animated: true)
            }
        }
    }
    
    @IBAction func buttonSelected(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
