//
//  CreditCardAuthorizationStep1ViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/11/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CreditCardAuthorizationStep1ViewController: PDViewController {

    @IBOutlet weak var textFieldResponsibleParty: PDTextField!
    @IBOutlet weak var textFieldAddress: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var textFieldPhone: PDTextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBack?.isHidden = isFromPreviousForm
        
        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
        if self.patient.creditCardQuestions.count == 0 {
            let questions = ["Charge my credit card for the unpaid balance due after the dental benefit payment is received for this treatment plan only.",
                             "Keep my signature on file and charge my credit card for any unpaid balance due after the dental benefit payment is received.",
                             "Charge my credit card per the amount and frequency determined below:"]
            
            for (_, question) in questions.enumerated() {
                let obj = PDQuestion()
                obj.question = question
                obj.selectedOption = false
                self.patient.creditCardQuestions.append(obj)
            }
        }
        loadValues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValues() {
        textFieldCity.text = patient.city
        textFieldZipCode.text = patient.zipCode
        textFieldAddress.text = patient.addressLine
        textFieldResponsibleParty.text = patient.responsibleParty
        textFieldPhone.text = patient.phoneNumber
        if let state = patient.state, !state.isEmpty {
            textFieldState.text = state
        }
        
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    func setValues() {
        patient.city = textFieldCity.text
        patient.state = textFieldState.text
        patient.zipCode = textFieldZipCode.text
        patient.addressLine = textFieldAddress.text
        patient.responsibleParty = textFieldResponsibleParty.text
        patient.phoneNumber = textFieldPhone.text
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            let creditCardStep2VC = self.storyboard?.instantiateViewController(withIdentifier: "kCreditCardAuthorizationStep2VC") as! CreditCardAuthorizationStep2ViewController
            creditCardStep2VC.patient = self.patient
            self.navigationController?.pushViewController(creditCardStep2VC, animated: true)
        }
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldResponsibleParty, textFieldCity, textFieldState, textFieldZipCode, textFieldAddress]
        for textField in textFields {
            if textField!.isEmpty {
                return textField
            }
        }
        return nil
    }

}


extension CreditCardAuthorizationStep1ViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        } else {
            return textField.formatPhoneNumber(range, string: string)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
