//
//  CreditCardAuthorizationStep3ViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CreditCardAuthorizationStep3ViewController: PDViewController {
    
    @IBOutlet weak var radioButtonCardType: RadioButton!
    @IBOutlet weak var textFieldCardHolderName: PDTextField!
    @IBOutlet weak var textFieldCCV: PDTextField!
    @IBOutlet weak var textFieldExpDate: PDTextField!
    @IBOutlet weak var textFieldCardNumber: PDTextField!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    var datePicker : DatePickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        //        DateInputView.addDatePickerForTextField(textFieldExpDate, minimumDate: NSDate(), maximumDate: nil, dateFormat: nil)
        datePicker = DatePickerView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        let gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
        
        let year = gregorian.component(Calendar.Component.year, from: Date())
        datePicker.minYear = year
        datePicker.maxYear = year + 5
        datePicker.selectToday()
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
        textFieldExpDate.inputView = datePicker
        textFieldExpDate.inputAccessoryView = toolbar
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func donePressed() {
        textFieldExpDate.resignFirstResponder()
        textFieldExpDate.text = datePicker.date
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
        patient.cardDetails.cardHolderName = textFieldCardHolderName.text
        patient.cardDetails.ccvCode = textFieldCCV.text
        if let selectedButton = radioButtonCardType.selected {
            patient.cardDetails.cardType = selectedButton.tag
        }
        patient.cardDetails.cardNumber = textFieldCardNumber.text
        patient.cardDetails.expiryDate = textFieldExpDate.text
    }
    
    func loadValues() {
        textFieldCardHolderName.text = patient.cardDetails.cardHolderName
        textFieldCCV.text = patient.cardDetails.ccvCode
        textFieldCardNumber.text = patient.cardDetails.cardNumber
        textFieldExpDate.text = patient.cardDetails.expiryDate
        if let cardType = patient.cardDetails.cardType {
            radioButtonCardType.setSelectedWithTag(cardType)
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if radioButtonCardType.selected == nil {
            let alert = Extention.alert("PLEASE SELECT CARD TYPE")
            self.present(alert, animated: true, completion: nil)
        } else if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        }  else if textFieldCCV.text!.characters.count < 3 {
            let alert = Extention.alert("PLEASE ENTER VALID CCV")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            
            setValues()
            if !textFieldCardNumber.text!.isCreditCard && patient.cardDetails.cardType != 3{
                let alert = Extention.alert("PLEASE ENTER VALID CARD NUMBER")
                self.present(alert, animated: true, completion: nil)
            }
                
            else if !textFieldCardNumber.text!.isAmericanCreditCard && patient.cardDetails.cardType == 3 {
                let alert = Extention.alert("PLEASE ENTER VALID CARD NUMBER")
                self.present(alert, animated: true, completion: nil)
            } else if textFieldCCV.text!.characters.count < 4 && patient.cardDetails.cardType == 3 {
                let alert = Extention.alert("PLEASE ENTER VALID CCV")
                self.present(alert, animated: true, completion: nil)
            }else if textFieldCCV.text!.characters.count > 3 && patient.cardDetails.cardType != 3 {
                let alert = Extention.alert("PLEASE ENTER VALID CCV")
                self.present(alert, animated: true, completion: nil)
            } else {
                
                patient.signature1 = signatureView.signatureImage()
                
                let creditCardFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kCreditCardAuthorizationFormVC") as! CreditCardAuthorizationFormViewController
                creditCardFormVC.patient = self.patient
                self.navigationController?.pushViewController(creditCardFormVC, animated: true)
            }
        }
    }
    
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldCardHolderName, textFieldCCV, textFieldExpDate, textFieldCardNumber]
        for textField in textFields {
            if textField!.isEmpty {
                return textField
            }
        }
        return nil
    }
    
    
    
}


extension CreditCardAuthorizationStep3ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == textFieldCardNumber && radioButtonCardType.selected != nil && radioButtonCardType.selected.tag == 3 {
            return textField.formatAmericanCreditCardNumber(range, string: string)
            
        } else if textField == textFieldCardNumber {
            return textField.formatCreditCardNumber(range, string: string)
            
        }
        
        if textField == textFieldCCV {
            
            return textField.formatNumbers(range, string: string, count: ((radioButtonCardType.selected != nil && radioButtonCardType.selected.tag == 3) ? 4 : 3 ))
            
            
        }
        return true
    }
    //
    //        if textField == textFieldCardNumber {
    //            return textField.formatCreditCardNumber(range, string: string)
    //        } else {
    //            return textField.formatNumbers(range, string: string, count: ((radioButtonCardType.selectedButton != nil && radioButtonCardType.selectedButton.tag == 3) ? 4 : 3 ))
    //        }
    //  }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
