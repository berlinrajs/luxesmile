//
//  CreditCardAuthorizationStep2ViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/11/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CreditCardAuthorizationStep2ViewController: PDViewController {

    @IBOutlet weak var viewPayment: PDView!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var textFieldDepositAmount: PDTextField!
    @IBOutlet weak var textFieldPaymentAmount: PDTextField!
    @IBOutlet weak var radioButtonFrequency: RadioButton!
    @IBOutlet weak var radioButtonLength: RadioButton!
    
    
//    var creditCardQuestions : [PDQuestion] = [PDQuestion]()

    override func viewDidLoad() {
        super.viewDidLoad()
        if patient.cardDetails == nil {
            patient.cardDetails = CreditCard()
        }
        loadValues()
        self.tableViewQuestions.reloadData()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
        patient.cardDetails.depositAmount = textFieldDepositAmount.text
        patient.cardDetails.planAmount = textFieldPaymentAmount.text
        if let selectedButton = radioButtonFrequency.selected {
            self.patient.cardDetails.lengthMethod = selectedButton.title(for: .normal)!.lowercased()
        }
    }
    
    func loadValues() {
        textFieldDepositAmount.text = patient.cardDetails.depositAmount
        textFieldPaymentAmount.text = patient.cardDetails.planAmount
        if let lengthMethod = self.patient.cardDetails.lengthMethod, !lengthMethod.isEmpty {
            radioButtonFrequency.setSelectedWithTag(lengthMethod.contains("monthly") ? 1 : 2)
        }
        if let lengthOfTime = self.patient.cardDetails.lengthOfTime, !lengthOfTime.isEmpty {
            radioButtonLength.setSelectedWithTag(lengthOfTime.contains("months") ? 0 : 1)
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }

    @IBAction func radioButtonActionLengthOfTime(_ sender: RadioButton) {
        PopupTextField.sharedInstance.showWithPlaceHolder("# OF\(sender.title(for: UIControlState())!)", keyboardType: .numberPad, textFormat: .number) { (poupView, textField, isEdited) in
            if isEdited {
                self.patient.cardDetails.lengthOfTime = "\(textField.text!) \(sender.title(for: .normal)!.lowercased())"
            } else {
                sender.deselectAllButtons()
                self.patient.cardDetails.lengthOfTime = ""
            }
        }
    }
    
    
    func findSelectedQuestion() -> PDQuestion? {
        for question in self.patient.creditCardQuestions {
            if question.selectedOption == true {
                return question
            }
        }
        return nil
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        func gotoNextView() {
            let creditCardStep3VC = self.storyboard?.instantiateViewController(withIdentifier: "kCreditCardAuthorizationStep3VC") as! CreditCardAuthorizationStep3ViewController
            creditCardStep3VC.patient = self.patient
            self.navigationController?.pushViewController(creditCardStep3VC, animated: true)
        }
        
        if self.findSelectedQuestion() == nil {
            let alert = Extention.alert("PLEASE SELECT ALL THAT APPLY")
            self.present(alert, animated: true, completion: nil)
        } else {
            if viewPayment.isUserInteractionEnabled == false {
                gotoNextView()
            } else {
                if textFieldDepositAmount.isEmpty || textFieldPaymentAmount.isEmpty {
                    let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
                    self.present(alert, animated: true, completion: nil)
                } else if let _ = radioButtonFrequency.selected {
                    if let _ = radioButtonLength.selected {
                        setValues()
                        
                        gotoNextView()
                    } else {
                        let alert = Extention.alert("PLEASE SELECT LENGTH OF TIME")
                        self.present(alert, animated: true, completion: nil)
                    }
                } else {
                    let alert = Extention.alert("PLEASE SELECT FREQUENCY")
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}

extension CreditCardAuthorizationStep2ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = patient.creditCardQuestions[indexPath.row]
        obj.selectedOption = !obj.selectedOption!
        tableView.reloadData()
    }
}

extension CreditCardAuthorizationStep2ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalClearance", for: indexPath) as! MedicalHistoryStep2TableViewCell
        let obj = patient.creditCardQuestions[indexPath.row]
        cell.labelTitle.text = obj.question
        cell.buttonCheckbox.isSelected = obj.selectedOption!
        if (indexPath.row == 2 && obj.selectedOption == true) {
            viewPayment.alpha = 1.0
            viewPayment.isUserInteractionEnabled = true
        } else if (indexPath.row == 2 && obj.selectedOption == false) {
            viewPayment.alpha = 0.4
            viewPayment.isUserInteractionEnabled = false
            radioButtonLength.deselectAllButtons()
            radioButtonFrequency.deselectAllButtons()
            textFieldDepositAmount.text = ""
            textFieldPaymentAmount.text = ""
        }
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
        
    }
}

extension CreditCardAuthorizationStep2ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textField.formatAmount(range, string: string)
    }
}
