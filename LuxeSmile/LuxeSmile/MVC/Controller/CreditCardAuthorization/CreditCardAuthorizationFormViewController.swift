//
//  CreditCardAuthorizationFormViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CreditCardAuthorizationFormViewController: PDViewController {
    
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelResponsiblePartyName: UILabel!
    @IBOutlet weak var labelBillingAddress: UILabel!
    @IBOutlet weak var labelCityState: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!

    @IBOutlet var buttonCreditCard: [UIButton]!
    @IBOutlet weak var labelDepositAmount: UILabel!
    @IBOutlet weak var labelPlanAmount: UILabel!
    @IBOutlet weak var labelFrequency: UILabel!
    @IBOutlet weak var labelLengthOfTime: UILabel!
    @IBOutlet weak var radioButtonCardType: RadioButton!
    @IBOutlet weak var labelCardNumber: UILabel!
    @IBOutlet weak var labelCardHolderName: UILabel!
    @IBOutlet weak var labelExpDate: UILabel!
    @IBOutlet weak var labelCCV: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let patientName = "\(patient.firstName) \(patient.lastName)"
        labelPatientName.text = patientName
        labelResponsiblePartyName.text = patient.responsibleParty
        labelBillingAddress.text = patient.addressLine
        labelCityState.text = "\(patient.city), \(patient.state), \(patient.zipCode)"
        labelPhone.text = patient.phoneNumber
        buttonCreditCard[0].isSelected = patient.creditCardQuestions[0].selectedOption!
        buttonCreditCard[1].isSelected = patient.creditCardQuestions[1].selectedOption!
        buttonCreditCard[2].isSelected = patient.creditCardQuestions[2].selectedOption!
        if patient.creditCardQuestions[2].selectedOption == true {
            labelDepositAmount.text = patient.cardDetails.depositAmount?.value
            labelPlanAmount.text = patient.cardDetails.planAmount?.value
            labelFrequency.text = patient.cardDetails.lengthMethod?.value
            labelLengthOfTime.text = patient.cardDetails.lengthOfTime?.value

        }
        radioButtonCardType.setSelectedWithTag(patient.cardDetails.cardType)
        labelCardNumber.text = patient.cardDetails.cardNumber?.value
        labelCardHolderName.text = patient.cardDetails.cardHolderName?.value
        labelExpDate.text = patient.cardDetails.expiryDate?.value
        labelCCV.text = patient.cardDetails.ccvCode?.value
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
