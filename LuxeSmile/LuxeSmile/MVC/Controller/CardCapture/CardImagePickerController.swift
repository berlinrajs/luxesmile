//
//  CardImagePickerController.swift
//  TotalHealthDentalAuto
//
//  Created by Office on 10/10/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
import AVFoundation


class CardImagePickerController: UIViewController {
    
    var captureSession = AVCaptureSession()
    var stillImageOutput: AVCaptureStillImageOutput!
    
    @IBOutlet var previewView: UIView!
    
    var isFrontCameraActive: Bool = false
    var capturePressed: Bool = false
    var completion: ((_ image: UIImage?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        captureSession.beginConfiguration()
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.previewView.layer.addSublayer(previewLayer!)
        
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        isFrontCameraActive = device?.position == AVCaptureDevicePosition.front ? true : false
        do {
            let input = try AVCaptureDeviceInput(device: device)
            captureSession.addInput(input)
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            captureSession.addOutput(stillImageOutput)
        } catch {
            
        }
        
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.backgroundColor = UIColor.clear
    }
    
    func present(inViewController viewController: UIViewController, completion: ((_ image: UIImage?) -> Void)?) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            viewController.present(self, animated: true) {
                self.completion = completion
            }
        }
    }
    
    @IBAction func toogleDevice() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        captureSession.beginConfiguration()
        
        for input in captureSession.inputs {
            captureSession.removeInput(input as! AVCaptureInput)
        }
        
        for device in AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as! [AVCaptureDevice] {
            if (device.position == AVCaptureDevicePosition.front && isFrontCameraActive == false) || (device.position == AVCaptureDevicePosition.back && isFrontCameraActive == true) {
                do {
                    let input = try AVCaptureDeviceInput(device: device)
                    captureSession.addInput(input)
                } catch {
                    
                }
            }
        }
        self.isFrontCameraActive = !isFrontCameraActive
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    
    @IBAction func captureImage() {
        capturePressed = true
        var videoConnection: AVCaptureConnection!
        
        for connection in stillImageOutput.connections as! [AVCaptureConnection] {
            for port in connection.inputPorts as! [AVCaptureInputPort] {
                if port.mediaType == AVMediaTypeVideo {
                    videoConnection = connection
                    break;
                }
            }
            if videoConnection != nil
            {
                break;
            }
        }
        print("about to request a capture from: \(stillImageOutput)")
        stillImageOutput.captureStillImageAsynchronously(from: videoConnection) { (buffer, error) in
            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)!
            let image = UIImage(data: imageData)
            self.completion?(CardImagePickerController.getCardImage(OfImage: image))
            self.capturePressed = false
        }
    }
    
    class func getCardImage(OfImage originalImage: UIImage?) -> UIImage? {
        if originalImage == nil {
            return nil
        } else {
            let deviceScale = UIScreen.main.scale
            
            UIGraphicsBeginImageContext(CGSize(width: deviceScale * 502, height: deviceScale * 326.5))
            originalImage!.draw(in: CGRect(x: -133 * deviceScale, y: -294 * deviceScale, width: deviceScale * 768, height: deviceScale * 1024))
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return image
        }
    }
    
    @IBAction func backAction() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        completion?(nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
