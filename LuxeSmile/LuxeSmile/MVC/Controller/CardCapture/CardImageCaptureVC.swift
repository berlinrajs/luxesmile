//
//  CardImageCaptureVC.swift
//  AceDental
//
//  Created by SRS Web Solutions on 29/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CardImageCaptureVC: PDViewController {

    @IBOutlet weak var imageViewFront: ActionImageView!
    @IBOutlet weak var imageViewBack: ActionImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    var isDrivingLicense: Bool = false
    var isFrontImageSelected: Bool = false
    var isBackImageSelected: Bool = false
    
    var frontPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseFront") : UIImage(named: "InsuranceFront")
        }
    }
    
    var backPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseBack") : UIImage(named: "InsuranceBack")
        }
    }
//    @IBOutlet weak var captureButtonFront: UIButton!
//    @IBOutlet weak var captureButtonBack: UIButton!

    var selectedButton: UIButton!
    
//    var overlayView: UIView {
//        get {
//            let imageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
//            imageView.backgroundColor = UIColor.clearColor()
//            imageView.image = UIImage(named: "overlay")
//            return imageView
//        }
//    }
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        labelTitle.text = isDrivingLicense ? "ADD/UPDATE DRIVING LICENSE" : "ADD/UPDATE INSURANCE CARD"
      
        imageViewFront.cornerRadius = 5.0
        imageViewBack.cornerRadius = 5.0
        imageViewBack.borderColor = UIColor.clear
        imageViewFront.borderColor = UIColor.clear
        
        imageViewFront.image = self.frontPlaceHolderImage
        imageViewBack.image = self.backPlaceHolderImage
        // Do any additional setup after loading the view.
        loadValues()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    func loadValues() {
        if let frontImage = patient.frontImage {
            isFrontImageSelected = true
            imageViewFront.image = frontImage
        } else {
            imageViewFront.image = self.frontPlaceHolderImage
        }
        if let backImage = patient.backImage {
            isBackImageSelected = true
            imageViewBack.image = backImage
        } else {
            imageViewBack.image = self.backPlaceHolderImage
        }
    }
    
    func setValues() {
        patient.frontImage = isFrontImageSelected ? self.imageViewFront.image : nil
        patient.backImage = isBackImageSelected ? self.imageViewBack.image : nil
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func nextAction(_ sender: AnyObject) {
        
        if isDrivingLicense && !isFrontImageSelected {
            let alert = Extention.alert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
            self.present(alert, animated: true, completion: nil)
        } else if !isDrivingLicense && !isFrontImageSelected  {
            let alert = Extention.alert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()

            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kCardImageFormVC") as! CardImageFormVC
            formVC.patient = self.patient
            formVC.isDrivingLicense = self.isDrivingLicense
            navigationController?.pushViewController(formVC, animated: true)
        }

        
//        if isDrivingLicense && !isFrontImageSelected {
//            let alert = Extention.alert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        } else if !isDrivingLicense && (!isFrontImageSelected || !isBackImageSelected) {
//            
//            let alert = Extention.alert("PLEASE CAPTURE FRONT AND BACK SIDE OF THE CARD")
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else {
//            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageFormVC") as! CardImageFormVC
//        formVC.patient = patient
//
//            formVC.isDrivingLicense = self.isDrivingLicense
//            formVC.frontImage = self.imageViewFront.image!
//            formVC.backImage = isBackImageSelected ? self.imageViewBack.image : nil
//            navigationController?.pushViewController(formVC, animated: true)
//        }
        
    }
    
    @IBAction func camButtonSelected(_ sender: UIButton) {
        let picker = CardImagePickerController(nibName: "CardImagePickerController", bundle: nil)
        picker.present(inViewController: self) { (image) in
            picker.dismiss(animated: true, completion: {
                if let img = image {
                    sender.isSelected = true
                    if sender.tag == 1 {
                        self.imageViewFront.image = img
                        self.isFrontImageSelected = true
                        self.imageViewFront.borderColor = UIColor.white
                    } else {
                        self.imageViewBack.image = img
                        self.isBackImageSelected = true
                        self.imageViewBack.borderColor = UIColor.white
                    }
                }
            })
        }
    }
}
