//
//  MedicalClearanceStep1ViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/10/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalClearanceStep1ViewController: PDViewController {
    
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var tableViewTreatment: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBack?.isHidden = isFromPreviousForm
        
        let patientName = "\(patient.firstName) \(patient.lastName)"

        labelText.text = labelText.text!.replacingOccurrences(of: "kPatientName", with: patientName).replacingOccurrences(of: "kDateofBirth", with: patient.dateOfBirth)
        if self.patient.clearanceQuestions.count == 0 {
            let questions = ["This patient may undergo dental treatment without special precautions or pre-medications.",
                             "This patient requires special precautions before treatment. Proceed with the following recommendation and/or precautions.",
                             "Do NOT proceed until (date)"]
            
            for question in questions {
                let obj = PDQuestion()
                obj.question = question
                obj.selectedOption = false
                self.patient.clearanceQuestions.append(obj)
            }
            self.patient.clearanceQuestions[1].isAnswerRequired = true
        }
        
        tableViewTreatment.reloadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        if self.patient.selectedOptions.count == 0 {
            let alert = Extention.alert("PLEASE SELECT ALL THAT APPLY")
            self.present(alert, animated: true, completion: nil)
        } else {
            let medicalClearanceStep2VC = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalClearanceStep2VC") as! MedicalClearanceStep2ViewController
            medicalClearanceStep2VC.patient = self.patient
            self.navigationController?.pushViewController(medicalClearanceStep2VC, animated: true)
        }
    }
}

extension MedicalClearanceStep1ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let titles = ["Prophylaxis", "Scaling and Root Planning", "Root Canal", "Extraction", "Crowns / Bridge", "Other"]
        let title = titles[indexPath.row]
        if self.patient.selectedOptions.contains(title) {
            self.patient.selectedOptions.remove(at: self.patient.selectedOptions.index(of: title)!)
        } else {
            self.patient.selectedOptions.append(title)
        }
        tableView.reloadData()
    }
}

extension MedicalClearanceStep1ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalClearance", for: indexPath) as! MedicalHistoryStep2TableViewCell
        let titles = ["Prophylaxis", "Scaling and Root Planning", "Root Canal", "Extraction", "Crowns / Bridge", "Other"]
        cell.labelTitle.text = titles[indexPath.row]
        cell.buttonCheckbox.isSelected = self.patient.selectedOptions.contains(titles[indexPath.row])
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear        
        return cell
        
    }
}
