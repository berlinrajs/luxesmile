//
//  MedicalClearanceFormViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/10/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalClearanceFormViewController: PDViewController {

    @IBOutlet var labelDate: [UILabel]!
    @IBOutlet weak var labelDoctorName: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    @IBOutlet var buttonTreatments: [UIButton]!
    @IBOutlet var buttonResponse: [UIButton]!
    @IBOutlet weak var labelUntilDate: UILabel!
    @IBOutlet weak var labelPhysicanName: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet var labelText: [UILabel]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for label in labelDate {
            label.text = patient.dateToday
        }
        labelDoctorName.text = patient.doctorName
        let patientName = "\(patient.firstName) \(patient.lastName)"
        labelName.text = patientName
        labelDOB.text = patient.dateOfBirth
        labelPhysicanName.text = patient.physicianName
        imageViewSignature.image = patient.signature1
        
        buttonTreatments[0].isSelected = patient.selectedOptions.contains("Prophylaxis")
        buttonTreatments[1].isSelected = patient.selectedOptions.contains("Scaling and Root Planning")
        buttonTreatments[2].isSelected = patient.selectedOptions.contains("Root Canal")
        buttonTreatments[3].isSelected = patient.selectedOptions.contains("Extraction")
        buttonTreatments[4].isSelected = patient.selectedOptions.contains("Crowns / Bridge")
        buttonTreatments[5].isSelected = patient.selectedOptions.contains("Other")
        
        for (idx, question) in patient.clearanceQuestions.enumerated() {
            buttonResponse[idx].isSelected = question.selectedOption!
            if question.selectedOption == true {
                if idx == 1 {
                    question.answer!.setTextForArrayOfLabels(labelText)
                } else if idx == 2 {
                    labelUntilDate.text = question.answer
                }
            }
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
