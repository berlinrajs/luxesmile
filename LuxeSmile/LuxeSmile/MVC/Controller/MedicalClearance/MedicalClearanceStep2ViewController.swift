//
//  MedicalClearanceStep2ViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/10/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalClearanceStep2ViewController: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var textFieldDate: PDTextField!
    @IBOutlet weak var textFieldName: PDTextField!
    
//    var clearanceQuestions : [PDQuestion] = [PDQuestion]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        
        DateInputView.addDatePickerForTextField(textFieldDate, minimumDate: Date(), maximumDate: nil, dateFormat: nil)
        loadValues()
//        self.tableViewQuestions.reloadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func findSelectedQuestion() -> PDQuestion? {
        for question in self.patient.clearanceQuestions {
            if question.selectedOption == true {
                return question
            }
        }
        return nil
    }
    
    func setValues() {
        patient.physicianName = textFieldName.text

    }
    
    func loadValues() {
        textFieldName.text = patient.physicianName
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }

    @IBAction func buttonActionNext(_ sender : AnyObject) {
        if self.findSelectedQuestion() == nil {
            let alert = Extention.alert("PLEASE SELECT ALL THAT APPLY")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            patient.signature1 = signatureView.image
            let medicalClearanceFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalClearanceFormVC") as! MedicalClearanceFormViewController
            medicalClearanceFormVC.patient = self.patient
            self.navigationController?.pushViewController(medicalClearanceFormVC, animated: true)
        }
    }
}

extension MedicalClearanceStep2ViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textFieldDate && !textFieldDate.isEmpty {
            let obj = self.patient.clearanceQuestions[2]
            obj.selectedOption = true
            obj.answer = textFieldDate.text
            tableViewQuestions.reloadData()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension MedicalClearanceStep2ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.patient.clearanceQuestions[indexPath.row]
        if obj.selectedOption == true {
            obj.selectedOption = false
            tableView.reloadData()
            if indexPath.row == 2 {
                textFieldDate.text = ""
            }
        } else {
            if indexPath.row == 2 {
                textFieldDate.isEnabled = true
                textFieldDate.becomeFirstResponder()
            } else if indexPath.row == 1 {
                PopupTextView.sharedInstance.show({ (textView, isEdited) in
                    if isEdited {
                        obj.selectedOption = true
                        obj.answer = textView.text
                    } else {
                        obj.selectedOption = false
                    }
                    tableView.reloadData()
                })
            } else {
                obj.selectedOption = true
                tableView.reloadData()
            }
        }
    }
}

extension MedicalClearanceStep2ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalClearance", for: indexPath) as! MedicalHistoryStep2TableViewCell
        let obj = self.patient.clearanceQuestions[indexPath.row]
        cell.labelTitle.text = obj.question
        cell.buttonCheckbox.isSelected = obj.selectedOption!
        textFieldDate.isEnabled = indexPath.row == 2 && obj.selectedOption == true
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
        
    }
}
