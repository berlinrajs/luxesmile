//
//  FinancialPolicyVC.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 6/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class FinancialPolicyVC: PDViewController {


    @IBOutlet weak var labelDate: DateLabel!
    
    @IBOutlet weak var imageSignatureView: SignatureView!
    @IBOutlet weak var buttonNext: PDButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageSignatureView.layer.cornerRadius = 3.0
        
        labelDate.todayDate = patient.dateToday
        buttonBack?.isHidden = isFromPreviousForm && !isFromPatientIntakeFrom
        if isFromPatientIntakeFrom {
            buttonNext?.setTitle("NEXT", for: UIControlState())
        }
    }

    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        
        
        self.view.endEditing(true)
        if !imageSignatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
            
        }else if !labelDate.dateTapped {
            
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        }else {
            
            patient.patientSignature = imageSignatureView.signatureImage()
            
            if isFromPatientIntakeFrom {
                let formVC = PatientInTakeStoryBoard.instantiateViewController(withIdentifier: "kPatientInTakeForm") as! PatientInTakeForm
                formVC.patient = patient
                self.navigationController?.pushViewController(formVC, animated: true)
            } else {
                let kFinancialForm = self.storyboard?.instantiateViewController(withIdentifier: "kFinancialPolicyFormVC") as! FinancialPolicyForm
                kFinancialForm.patient = patient
                self.navigationController?.pushViewController(kFinancialForm, animated: true)
            }
        }
    }
}
