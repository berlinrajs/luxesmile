//
//  FinancialPolicyForm.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 6/24/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class FinancialPolicyForm: PDViewController {

    @IBOutlet weak var logoWithAddress: UIImageView!
    
    @IBOutlet weak var doctorName: UILabel!
    
    @IBOutlet weak var signatureView: UIImageView!
    
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var patientName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signatureView.image = patient.patientSignature
        
        labelDate.text = patient.dateToday
        
        doctorName.text = patient.doctorName
        
        patientName.text = patient.fullName
        if isFromPatientIntakeFrom {
            buttonSubmit?.isHidden = true
            buttonBack?.isHidden = true
            logoWithAddress.isHidden = true
        }
        
    }


}
