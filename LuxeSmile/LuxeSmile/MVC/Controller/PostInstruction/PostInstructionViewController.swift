//
//  PostInstructionViewController.swift
//  LuxeSmile
//
//  Created by Bala Murugan on 7/15/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PostInstructionViewController: PDViewController {

    @IBOutlet weak var signatureViewPatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!

    @IBOutlet weak var replaceDoctorName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday

        replaceDoctorName.text = replaceDoctorName.text?.replacingOccurrences(of: "Dr. Burghardt", with: patient.doctorName)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if !signatureViewPatient.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: "PostInstructionFormVC") as! PostInstructionFormViewController
            step1VC.signatureImage = signatureViewPatient.signatureImage()
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
            
        }
    }

}
