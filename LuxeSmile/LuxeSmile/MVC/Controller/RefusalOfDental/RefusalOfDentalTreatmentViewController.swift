//
//  RefusalOfDentalTreatmentViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 3/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class RefusalOfDentalTreatmentViewController: PDViewController {
    
    
    
    @IBOutlet weak var labelText1: UILabel!
    @IBOutlet weak var labelText2: UILabel!
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var labelDate1: PDLabel!
    @IBOutlet weak var labelDate2: PDLabel!
    @IBOutlet weak var textViewTreatment: PDTextView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonBack?.isHidden = isFromPreviousForm
        signatureView1.layer.cornerRadius = 3.0
        signatureView2.layer.cornerRadius = 3.0
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(RefusalOfDentalTreatmentViewController.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate1.addGestureRecognizer(tapGesture1)
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(RefusalOfDentalTreatmentViewController.setDateOnLabel2))
        tapGesture2.numberOfTapsRequired = 1
        labelDate2.addGestureRecognizer(tapGesture2)
        labelText1.text = labelText1.text?.replacingOccurrences(of: "kDoctorName", with: patient.doctorName)
        labelText2.text = labelText2.text?.replacingOccurrences(of: "kDoctorName", with: patient.doctorName)

        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setDateOnLabel1() {
        labelDate1.text = patient.dateToday
        labelDate1.textColor = UIColor.black
    }
    
    func setDateOnLabel2() {
        labelDate2.text = patient.dateToday
        labelDate2.textColor = UIColor.black
    }
    
    func loadValues() {
        if let otherTreatment = patient.otherTreatment, !otherTreatment.isEmpty {
            textViewTreatment.text = otherTreatment
            textViewTreatment.textColor = UIColor.black
        }
    }
    
    func setValues() {
        patient.otherTreatment = textViewTreatment.text == "TYPE HERE *" || textViewTreatment.isEmpty ? "" : textViewTreatment.text

    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    

    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if textViewTreatment.text == "TYPE HERE *" || textViewTreatment.isEmpty {
            let alert = Extention.alert("PLEASE ENTER TREATMENT NAME")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView1.isSigned() || !signatureView2.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" || labelDate2.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            patient.signature1 = signatureView1.signatureImage()
            patient.signature2 = signatureView2.signatureImage()
            let kRefusalOfDentalTreatmentVC = self.storyboard?.instantiateViewController(withIdentifier: "kRefusalOfDentalFormVC") as! RefusalOfDentalTreatmentFormViewController
            kRefusalOfDentalTreatmentVC.patient = patient
            self.navigationController?.pushViewController(kRefusalOfDentalTreatmentVC, animated: true)
        }
    }
}


extension RefusalOfDentalTreatmentViewController : UITextViewDelegate {
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "TYPE HERE *" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "TYPE HERE *"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
