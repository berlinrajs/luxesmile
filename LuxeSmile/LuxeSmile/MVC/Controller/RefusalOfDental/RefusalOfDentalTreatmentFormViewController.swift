//
//  RefusalOfDentalTreatmentFormViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 3/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class RefusalOfDentalTreatmentFormViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()

    
    @IBOutlet weak var PatietnSignForm: UILabel!
    
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!

    
    @IBOutlet weak var labelPatientDetails: UILabel!
    @IBOutlet weak var labelTreatmentDetails: UILabel!
    
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var labelDate2: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelText.text = labelText.text?.replacingOccurrences(of: "kDoctorName", with: patient.doctorName)
        
        imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        // Do any additional setup after loading the view.
        
        PatietnSignForm.text = patient.fullName
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        
        
        let attributedString = NSMutableAttributedString(string: labelText.text!)
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedString.length))
        labelText.attributedText = attributedString
        
        
        
        var patientInfo = "Patient Name:"
        let patientName = getText("\(patient.fullName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        patientInfo = patientInfo + " Date: \(getText(patient.dateToday))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateToday)))
        let attributedString1 = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString1.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        attributedString1.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedString1.length))
        labelPatientDetails.attributedText = attributedString1
        
        textRanges.removeAll()
        
        var treatmentInfo = patient.doctorName!
        textRanges.append(treatmentInfo.rangeOfText(treatmentInfo))
        if let treatment = patient.otherTreatment {
            treatmentInfo = treatmentInfo + " has advised me that the following treatment \(getText(treatment))"
            textRanges.append(treatmentInfo.rangeOfText(getText(treatment)))
        } else {
            let text = getText("N/A")
            treatmentInfo = treatmentInfo + " has advised me that the following treatment \(text)"
            textRanges.append(treatmentInfo.rangeOfText(text))
        }
        
        treatmentInfo = treatmentInfo + " needs to be performed on (name of patient) \(getText(patientName))"
        textRanges.append(treatmentInfo.rangeOfText(getText(patientName)))
        
        let attributedString2 = NSMutableAttributedString(string: treatmentInfo)
        for range in textRanges {
            attributedString2.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        attributedString2.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedString2.length))
        labelTreatmentDetails.attributedText = attributedString2
        
        constraintHeight.constant = labelTreatmentDetails.attributedText!.heightWithConstrainedWidth(screenSize.width - 120.0)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
}
