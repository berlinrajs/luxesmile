//
//  ReleaseHealthCareInfoStep3VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 17/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ReleaseHealthCareInfoStep3VC: PDViewController {

    
    @IBOutlet var signatureView: SignatureView!
    @IBOutlet var labelDate: DateLabel!
    
    @IBOutlet weak var textFieldRelation: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
        patient.relationShip1 = textFieldRelation.text
    }
    
    func loadValues() {
        textFieldRelation.text = patient.relationShip1
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT A DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView.signatureImage()
                setValues()
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kReleaseHealthCareInfoFormVC") as! ReleaseHealthCareInfoFormVC
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
