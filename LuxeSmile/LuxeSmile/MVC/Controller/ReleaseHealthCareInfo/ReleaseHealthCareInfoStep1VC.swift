//
//  ReleaseHealthCareInfoStep1VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 17/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ReleaseHealthCareInfoStep1VC: PDViewController {
    
//    @IBOutlet weak var textFieldSSN: PDTextField!
    @IBOutlet weak var textFieldPracticeName: PDTextField!
    @IBOutlet weak var textFieldPreviousName: PDTextField!
    
    @IBOutlet weak var textFieldName: PDTextField!
    @IBOutlet weak var textFieldAddress: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buttonBack?.isHidden = isFromPreviousForm
        
        labelDetails.text = labelDetails.text!.replacingOccurrences(of: "kPatientName", with: patient.firstName! + " " + patient.lastName!)
        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
        loadValues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
//        patient.socialSecurityNumber = textFieldSSN.text
        patient.practiceName = textFieldPracticeName.text
        
        patient.emergencyContactName = textFieldName.text
        patient.addressLine = textFieldAddress.text
        patient.city = textFieldCity.text
        patient.state = textFieldState.text
        patient.zipCode = textFieldZipCode.text
    }
    
    func loadValues() {
//        textFieldSSN.text = patient.socialSecurityNumber
        textFieldName.text = patient.emergencyContactName
        textFieldAddress.text = patient.addressLine
        textFieldCity.text = patient.city
        if let state = patient.state {
            textFieldState.text = state
        }
        textFieldZipCode.text = patient.zipCode
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
//        if !textFieldSSN.isEmpty && !textFieldSSN.text!.isSocialSecurityNumber {
//            let alert = Extention.alert("PLEASE ENTER A VALID SOCIAL SECURITY NUMBER")
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else 
        if !textFieldZipCode.isEmpty && !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER A VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            patient.previousClinicName = "N/A" // textFieldPreviousName.isEmpty ? "N/A" : textFieldPreviousName.text!
            let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kReleaseHealthCareInfoStep2VC") as! ReleaseHealthCareInfoStep2VC
            step2VC.patient = patient
            self.navigationController?.pushViewController(step2VC, animated: true)
        }
    }
    
    
}
extension ReleaseHealthCareInfoStep1VC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == textFieldSSN {
//            return textField.forSocialSecurityNumbers(range, string: string)
//        } else 
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
}
