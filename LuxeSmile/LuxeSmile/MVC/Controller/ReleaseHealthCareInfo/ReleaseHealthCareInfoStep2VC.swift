//
//  ReleaseHealthCareInfoStep2VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 17/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ReleaseHealthCareInfoStep2VC: PDViewController {

    
    @IBOutlet weak var textFieldTreatment: PDTextField!
    @IBOutlet weak var textFieldDate: PDTextField!
    @IBOutlet weak var textFieldOthers: PDTextField!
    
    @IBOutlet weak var conditionPopUp: PDView!
    @IBOutlet weak var expiresPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    
    @IBOutlet weak var radioButtonCondition: RadioButton!
    @IBOutlet weak var radioButtonExpires: RadioButton!
//    var dateListView: DateInputView {
//        set {
//            
//        }
//        get {
//            let dateList = DateInputView(frame: CGRectMake(0, 0, screenSize.width, 260))
//            dateList.datePicker.minimumDate = NSDate()
//            return dateList
//        }
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
        patient.treatment = textFieldTreatment.text
    }
    
    func loadValues() {
        textFieldTreatment.text = patient.treatment
        if let expires = patient.expires {
            radioButtonExpires.setSelectedWithTag(expires)
        }
        if let condition = patient.condition {
            radioButtonCondition.setSelectedWithTag(condition)
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func conditionDoneAction() {
        if textFieldOthers.isEmpty {
            patient.conditionOther = nil
            patient.condition = nil
            radioButtonCondition.deselectAllButtons()
        } else {
            patient.conditionOther = textFieldOthers.text!
            patient.condition = radioButtonCondition.selected.tag
        }
        textFieldOthers.resignFirstResponder()
        conditionPopUp.removeFromSuperview()
        viewShadow.isHidden = true
    }
    
    @IBAction func expiresDoneAction() {
        if textFieldDate.isEmpty {
            patient.expires = nil
            patient.expiresString = nil
            radioButtonExpires.deselectAllButtons()
        } else {
            patient.expires = textFieldDate.tag
            patient.expiresString = textFieldDate.text
        }
        
        textFieldDate.resignFirstResponder()
        conditionPopUp.removeFromSuperview()
        viewShadow.isHidden = true
    }
    
    func showConditionPopup() {
        textFieldOthers.text = ""
        self.conditionPopUp.frame = CGRect(x: 0, y: 0, width: 512.0, height: 250.0)
        self.conditionPopUp.center = self.view.center
        self.viewShadow.addSubview(self.conditionPopUp)
        self.conditionPopUp.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.conditionPopUp.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    func showExpiresPopup(_ tag: Int) {
        textFieldDate.text = ""
        textFieldDate.tag = tag
        if tag == 1 {
            
            DateInputView.addDatePickerForTextField(textFieldDate)
//            textFieldDate.inputView = dateListView
//            textFieldDate.inputAccessoryView = dateListView.toolbar
//            dateListView.textField = textFieldDate
        } else if tag == 2 {
            textFieldDate.inputView = nil
            textFieldDate.inputAccessoryView = nil
            textFieldDate.keyboardType = UIKeyboardType.numbersAndPunctuation
        } else if tag == 3 {
            textFieldDate.inputView = nil
            textFieldDate.inputAccessoryView = nil
            textFieldDate.keyboardType = UIKeyboardType.default
        }
        textFieldDate.placeholder = tag == 1 ? "SELECT A DATE" : tag == 2 ? "ENTER NUMBER OF DAYS" : "LIST THE EVENTS"
        self.expiresPopup.frame = CGRect(x: 0, y: 0, width: 512.0, height: 250.0)
        self.expiresPopup.center = self.view.center
        self.viewShadow.addSubview(self.expiresPopup)
        self.expiresPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.expiresPopup.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func conditionAction(_ sender: RadioButton) {
        if sender.tag == 3 {
            showConditionPopup()
        } else {
            patient.conditionOther = ""
            patient.condition = sender.tag
        }
    }
    
    @IBAction func expiresAction(_ sender: RadioButton) {
        showExpiresPopup(sender.tag)
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
/*        if radioButtonCondition.selectedButton == nil {
            let alert = Extention.alert("PLEASE ANSWER ALL THE REQUIRED QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if radioButtonExpires.selectedButton == nil {
            let alert = Extention.alert("PLEASE ANSWER ALL THE REQUIRED QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {*/
        setValues()
            let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kReleaseHealthCareInfoStep3VC") as! ReleaseHealthCareInfoStep3VC
            step3VC.patient = patient
            self.navigationController?.pushViewController(step3VC, animated: true)
//        }
    }
    @IBAction func buttonSkipAction() {
        self.view.endEditing(true)
        setValues()
        patient.expires = nil
        patient.expiresString = nil
        radioButtonExpires.deselectAllButtons()
        
        patient.conditionOther = nil
        patient.condition = nil
        radioButtonCondition.deselectAllButtons()
        
        let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kReleaseHealthCareInfoStep3VC") as! ReleaseHealthCareInfoStep3VC
        step3VC.patient = patient
        self.navigationController?.pushViewController(step3VC, animated: true)
    }
}
extension ReleaseHealthCareInfoStep2VC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldDate && textField.tag == 2 {
            if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
                return false
            }
//            let newRange = textField.text!.startIndex.advancedBy(range.location)..<textField.text!.startIndex.advancedBy(range.location + range.length)
//            let newString = textField.text!.stringByReplacingCharactersInRange(newRange, withString: string)
//            if newString.characters.count > 3 {
//                return false
//            }
        }
        return true
    }
}
