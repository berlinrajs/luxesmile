//
//  ReleaseHealthCareInfoFormVC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 17/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ReleaseHealthCareInfoFormVC: PDViewController {

    @IBOutlet weak var labelPatientName: FormLabel!
    @IBOutlet weak var labelDateOfBirth: FormLabel!
//    @IBOutlet weak var labelSSN: FormLabel!
    @IBOutlet weak var labelDoctorName: FormLabel!
    @IBOutlet weak var labelPracticeName: FormLabel!
    @IBOutlet weak var labelName: FormLabel!
    @IBOutlet weak var labelAddress: FormLabel!
    @IBOutlet weak var labelCityState: FormLabel!
    @IBOutlet weak var labelZipCode: FormLabel!
    @IBOutlet weak var labelTreatment1: FormLabel!
    @IBOutlet weak var labelTreatment2: FormLabel!
    @IBOutlet weak var labelCondition1: FormLabel!
    @IBOutlet weak var labelCondition2: FormLabel!
    @IBOutlet weak var labelExpireDate: FormLabel!
    @IBOutlet weak var labelExpireDay: FormLabel!
    @IBOutlet weak var labelExpireEvent1: FormLabel!
    @IBOutlet weak var labelExpireEvent2: FormLabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelRelation: FormLabel!
    
    @IBOutlet weak var radioButtonCondition: RadioButton!
    
    @IBOutlet weak var signatureView: UIImageView!
    
    @IBOutlet weak var PatientSignName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        
        PatientSignName.text = patient.fullName
        
        labelPatientName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth!
//        labelSSN.text = patient.socialSecurityNumber!.isFormSocialSecurityNumber
        //labelDoctorName.text = "DR. BURGHARDT"
        labelDoctorName.text = patient.doctorName
        labelPracticeName.text = patient.practiceName!
        labelName.text = patient.emergencyContactName!
        labelAddress.text = patient.addressLine!
        labelCityState.text = patient.city
        
        if !patient.state.isEmpty && !patient.city.isEmpty {
            labelCityState.text = "\(patient.city!), \(patient.state!)"
        } else if !patient.city.isEmpty {
            labelCityState.text = patient.city
        } else if !patient.state.isEmpty {
            labelCityState.text = patient.state
        } else {
            labelCityState.text = "N/A"
        }
        labelZipCode.text = patient.zipCode!
        
        if patient.treatment != nil {
            patient.treatment.setTextForArrayOfLabels([labelTreatment1, labelTreatment2])
        }
        if patient.condition != nil {
            radioButtonCondition.setSelectedWithTag(patient.condition)
            
            if patient.condition == 3 {
                patient.conditionOther.setTextForArrayOfLabels([labelCondition1, labelCondition2])
            }
        }
        
        if patient.expires != nil {
            if patient.expires == 1 {
                labelExpireDate.text = patient.expiresString
                labelExpireDay.text = ""
                labelExpireEvent1.text = ""
                labelExpireEvent2.text = ""
            } else if patient.expires == 2 {
                labelExpireDate.text = ""
                labelExpireDay.text = patient.expiresString
                labelExpireEvent1.text = ""
                labelExpireEvent2.text = ""
            } else if patient.expires == 3 {
                labelExpireDate.text = ""
                labelExpireDay.text = ""
                patient.expiresString.setTextForArrayOfLabels([labelExpireEvent1, labelExpireEvent2])
            }
        }
        
        labelDate.text = patient.dateToday
        labelRelation.text = patient.relationShip1
        signatureView.image = patient.signature1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
