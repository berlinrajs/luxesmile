//
//  RetainerInfoViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/14/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RetainerInfoViewController: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            let form = self.storyboard?.instantiateViewController(withIdentifier: "kRetainerInfoFormVC") as! RetainerInfoFormViewController
            patient.signature1 = signatureView.signatureImage()
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        }
    }

}
