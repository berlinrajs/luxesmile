//
//  RetainerInfoFormViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/14/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RetainerInfoFormViewController: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView.image = patient.signature1
        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
