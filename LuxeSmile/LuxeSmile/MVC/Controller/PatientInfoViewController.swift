//
//  PatientInfoViewController.swift
//  North Shore
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonNext: PDButton!
    @IBOutlet weak var textFieldMiddleInitial: PDTextField!
    
    @IBOutlet weak var patientGender: RadioButton!
    @IBOutlet weak var Preferedname: PDTextField!
   // @IBOutlet weak var selectTile: RadioButton!
    @IBOutlet weak var dropdown : BRDropDown!
    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!
    @IBOutlet weak var pickerMonth : UIPickerView!
    
    
    var isNewPatient: Bool!
    var manager: AFHTTPSessionManager?

    var arrayMonths = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        textfieldMonth.inputView = pickerMonth
        textfieldMonth.inputAccessoryView = toolBar

//        datePicker.maximumDate = nil
//        textFieldDateOfBirth.inputView = datePicker
//        textFieldDateOfBirth.inputAccessoryView = toolBar
        labelDate.text = patient.dateToday
        dropdown.items = ["Dr. William Burghardt","Dr. Sarah Rittenger"]
        // Do any additional setup after loading the view.
        
//        let dateString = "1 Jan 1980"
//        let df = NSDateFormatter()
//        df.dateFormat = "dd MM yyyy"
//        let date = df.dateFromString(dateString)
//        if let unwrappedDate = date {
//            datePicker.setDate(unwrappedDate, animated: false)
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if dropdown.selectedIndex == 0 {
            let alert = Extention.alert("PLEASE SELECT A DOCTOR NAME")
            self.present(alert, animated: true, completion: nil)
        }
        else if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT FIRST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT LAST NAME")
            self.present(alert, animated: true, completion: nil)
        }  else if textfieldMonth.isEmpty || textfieldDate.isEmpty  || textfieldYear.isEmpty {
            let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
            self.present(alert, animated: true, completion: nil)
        } else if Int(textfieldDate.text!)! == 0{
            let alert = Extention.alert("PLEASE ENTER THE VALID DATE")
            self.present(alert, animated: true, completion: nil)
        } else if !textfieldYear.text!.isValidYear {
            let alert = Extention.alert("PLEASE ENTER A VALID YEAR")
            self.present(alert, animated: true, completion: nil)
        }
        else {
            
            patient.doctorNameTag = dropdown.selectedIndex
            patient.doctorName = dropdown.items[patient.doctorNameTag! - 1]
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.dateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
            patient.initial = textFieldMiddleInitial.isEmpty ? "" : textFieldMiddleInitial.text!
            #if AUTO
                func showMoreThanOneUserAlert()  {
                    let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "More than one user found. Please handover the device to front desk to enter your patient id", preferredStyle: UIAlertControllerStyle.alert)
                    let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
                        let newPatientStep1VC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationVC") as! VerificationViewController
                        newPatientStep1VC.patient = self.patient
                        self.navigationController?.pushViewController(newPatientStep1VC, animated: true)
                    }
                    alertController.addAction(alertYesAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                }
                
                func showUnableToFindAlert (){
                    let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "Unable to find the patient \(textFieldFirstName.text!) \(textFieldLastName.text!) - \(patient.dateOfBirth) mismatch", preferredStyle: UIAlertControllerStyle.alert)
                    let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
                        showCreateNewPatientAlert()
                    }
                    alertController.addAction(alertYesAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                func showCreateNewPatientAlert (){
                    let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "Do you wish to create a new patient", preferredStyle: UIAlertControllerStyle.alert)
                    let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                        self.patient.patientDetails = nil
                        gotoPatientSignInForm()
                        
                    }
                    alertController.addAction(alertYesAction)
                    let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                        self.navigationController?.popToRootViewController(animated: true)
                        
                    }
                    alertController.addAction(alertNoAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                }
                
                func gotoPatientSignInForm() {
                    let medicalHistoryStep1VC = PatientInTakeStoryBoard.instantiateViewController(withIdentifier: "kPatientInTakeAddressInfoVC") as! AddressInfoVC
                    medicalHistoryStep1VC.patient = patient
                    self.navigationController?.pushViewController(medicalHistoryStep1VC, animated: true)
                }
                
                func APICall() {
                    BRProgressHUD.show()
                    self.buttonNext.isUserInteractionEnabled = false
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd, yyyy"
                    let date = dateFormatter.date(from: patient.dateOfBirth)
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
                    manager?.responseSerializer.acceptableContentTypes = ["text/html"]
                    manager?.post("consent_fetch_patient_info.php", parameters: ["first_name" : self.textFieldFirstName.text!, "last_name": self.textFieldLastName.text!, "dob": dateFormatter.string(from: date!)], progress: { (progress) in
                        }, success: { (task, result) in
                            self.buttonNext.isUserInteractionEnabled = true
                            BRProgressHUD.hide()
                            if self.navigationController?.topViewController == self {
                                let response = result as! [String : AnyObject]
                                if response["status"] as! String == "success"  {
                                    let patientDetails = response["patientData"] as! [String: AnyObject]
                                    self.patient.patientDetails = PatientDetails(details: patientDetails)
                                    self.gotoNextForm()
                                } else {
                                    if response["status"] as! String == "failed" && (response["message"] as! String).contains("Not connected") {
                                        let alert = Extention.alert((response["message"] as! String).uppercased())
                                        self.present(alert, animated: true, completion: nil)
                                    } else if response["status"] as! String == "matching_name"  {
                                        let patientDetails = response["matchingData"] as! [String: String]
                                        let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "Did you mean \(patientDetails["Fname"]!) \(patientDetails["Lname"]!)? If so select YES and choose the correct date of birth", preferredStyle: UIAlertControllerStyle.alert)
                                        let alertOkAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                                            self.textFieldFirstName.text = patientDetails["Fname"]
                                            self.textFieldLastName.text = patientDetails["Lname"]
                                            self.textfieldDate.text = ""
                                            self.textfieldMonth.text = ""
                                            self.textfieldYear.text = ""
                                        }
                                        alertController.addAction(alertOkAction)
                                        let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                                            showUnableToFindAlert()
                                        }
                                        alertController.addAction(alertNoAction)
                                        self.present(alertController, animated: true, completion: nil)
                                    } else if response["status"] as! String == "multiple_patient_found" {
                                        showMoreThanOneUserAlert()
                                    } else {
                                        self.patient.patientDetails = nil
                                        let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "Patient not found. Are you sure the provided details are correct?", preferredStyle: UIAlertControllerStyle.alert)
                                        let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                                            showUnableToFindAlert()
                                        }
                                        alertController.addAction(alertYesAction)
                                        let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                                            
                                        }
                                        alertController.addAction(alertNoAction)
                                        self.present(alertController, animated: true, completion: nil)
                                    }
                                }
                            }
                        }, failure: { (task, error) in
                            self.buttonNext.isUserInteractionEnabled = true
                            BRProgressHUD.hide()
                            self.patient.patientDetails = nil
                            if self.navigationController?.topViewController == self {
                                let alert = Extention.alert(error.localizedDescription)
                                self.present(alert, animated: true, completion: nil)
                            }
                    })
                }
                
                if self.isNewPatient == true {
                    self.gotoNextForm()
                } else {
                    APICall()
                }
            #else
                self.gotoNextForm()
            #endif
        }
        
    }
    
    func setDefaultValues() {
//        func medicalQuestions() {
//            
//            
//        }
//        
//        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//        if formNames.contains(kNewPatientIntakeForm) || formNames.contains(kPatientIntakeForm) {
//            
//            medicalQuestions()
//        } else if formNames.contains(kMedicalHistoryForm) {
//            medicalQuestions()
//        }
//        if formNames.contains(kCreditCard) {
//            
//        }
//        if formNames.contains(kMedicalClearance) {
//            
//        }
//        if formNames.contains(kImplantConsult) {
//            
//        }
//        if formNames.contains(kImproveAspects) {
//            
//
//        }
//        if formNames.contains(kQuickTreatment) {
//            
//        }
//        if formNames.contains(kExamForm) {
//            
//        }
    }
    
    
    @IBAction func toolbarDoneButtonAction(_ sender: AnyObject) {
        
        textfieldMonth.resignFirstResponder()
        let string1 = arrayMonths[pickerMonth.selectedRow(inComponent: 0)]
        textfieldMonth.text = string1.substring(with: string1.startIndex ..< string1.characters.index(string1.startIndex, offsetBy: 3))
        
    }

    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldDateOfBirth.text = dateFormatter.string(from: datePicker.date).uppercased()
        
    }
    
}



extension PatientInfoViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldMiddleInitial {
            return textField.formatInitial(range, string: string)
        }else if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        }else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension PatientInfoViewController : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let string1 = arrayMonths[row]
        
        //let index1 = string1.startIndex.advancedBy(3)
        
        textfieldMonth.text = string1.substring(with: string1.startIndex ..< string1.characters.index(string1.startIndex, offsetBy: 3))
        //  textfieldMonth.text = arrayMonths[row]
    }
}

