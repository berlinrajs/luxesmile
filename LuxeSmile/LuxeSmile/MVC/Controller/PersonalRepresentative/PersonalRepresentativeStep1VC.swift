//
//  PersonalRepresentativeStep1VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 11/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PersonalRepresentativeStep1VC: PDViewController {

    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var textFieldRequestDate: PDTextField!
    @IBOutlet weak var textFieldExpireDate: PDTextField!
    
//    var arrayRelations: [Relation] = [Relation]()
    
    @IBOutlet weak var textFieldName: PDTextField!
    @IBOutlet weak var textFieldRelation: PDTextField!
    @IBOutlet weak var textFieldNumber: PDTextField!
    
    @IBOutlet weak var relationPopupView: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelFotterInfo: UILabel!
    @IBOutlet weak var buttonCancel: PDButton!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet var exceptionButtons: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBack?.isHidden = isFromPreviousForm
        
        DateInputView.addDatePickerForTextField(textFieldRequestDate)
        DateInputView.addDatePickerForTextField(textFieldExpireDate)
        tableView.tableFooterView = footerView
        // Do any additional setup after loading the view.
        loadValues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
        patient.expiryDate = textFieldExpireDate.text
        patient.requestDate = textFieldRequestDate.text
        var arrayExceptions = [String]()
        for button in exceptionButtons {
            if button.isSelected {
                arrayExceptions.append(button.title(for: UIControlState())!)
            }
        }
        patient.exceptions = arrayExceptions.count == 0 ? nil : (arrayExceptions as NSArray).componentsJoined(by: ", ")
    }
    
    func loadValues() {
        textFieldExpireDate.text = patient.expiryDate
        textFieldRequestDate.text = patient.requestDate
        if let exceptions = patient.exceptions {
            let arrayExceptions = exceptions.components(separatedBy: ", ")
            for button in exceptionButtons {
                button.isSelected = arrayExceptions.contains(button.title(for: UIControlState())!)
            }
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func relationAdded() {
        if textFieldRelation.isEmpty || textFieldName.isEmpty || textFieldNumber.isEmpty {
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER A VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else {
            if buttonCancel.title(for: UIControlState()) == "Delete" {
                let relation = Relation()
                relation.name = textFieldName.text!
                relation.phoneNumber = textFieldNumber.text!
                relation.relationShip = textFieldRelation.text!
                
                patient.relatives.remove(at: buttonCancel.tag)
                patient.relatives.insert(relation, at: buttonCancel.tag)
            } else {
                let relation = Relation()
                relation.name = textFieldName.text!
                relation.phoneNumber = textFieldNumber.text!
                relation.relationShip = textFieldRelation.text!
                
                patient.relatives.append(relation)
            }
            self.tableView.reloadData()
            
            relationPopupView.removeFromSuperview()
            viewShadow.isHidden = true
        }
    }
    @IBAction func closePopup(_ sender: PDButton) {
        if sender.title(for: UIControlState()) == "Delete" {
            patient.relatives.remove(at: sender.tag)
            tableView.reloadData()
        }
        relationPopupView.removeFromSuperview()
        viewShadow.isHidden = true
    }
    
    @IBAction func showPopup() {
        let frameSize = CGRect(x: 0, y: 0, width: 639, height: 594)
        self.relationPopupView.frame = frameSize
        self.relationPopupView.center = self.view.center
        self.viewShadow.addSubview(self.relationPopupView)
        self.viewShadow.isHidden = false
        self.relationPopupView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textFieldNumber.text = ""
        textFieldName.text = ""
        textFieldRelation.text = ""
        buttonCancel.setTitle("Cancel", for: UIControlState())
        self.relationPopupView.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.relationPopupView.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        /*if self.arrayRelations.count == 0 {
            let alert = Extention.alert("PLEASE ADD ATLEAST ONE PERSON BEFORE CONTINUE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {*/
        
            setValues()
            
            let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kPersonalRepresentativeStep2VC") as! PersonalRepresentativeStep2VC
            step2VC.patient = patient
            self.navigationController?.pushViewController(step2VC, animated: true)
//        }
    }
    
    @IBAction func addExceptions(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}
extension PersonalRepresentativeStep1VC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        labelFotterInfo.isHidden = patient.relatives.count == 0
        buttonAdd.isHidden = patient.relatives.count == 4
        tableView.tableHeaderView = patient.relatives.count > 0 ? headerView : nil
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.relatives.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "kRelationCell", for: indexPath) as! RelationCell
        cell.configCellWith(patient.relatives[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let frameSize = CGRect(x: 0, y: 0, width: 639, height: 594)
        self.relationPopupView.frame = frameSize
        self.relationPopupView.center = self.view.center
        self.viewShadow.addSubview(self.relationPopupView)
        self.viewShadow.isHidden = false
        self.relationPopupView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textFieldNumber.text = patient.relatives[indexPath.row].phoneNumber
        textFieldName.text = patient.relatives[indexPath.row].name
        textFieldRelation.text = patient.relatives[indexPath.row].relationShip
        buttonCancel.setTitle("Delete", for: UIControlState())
        buttonCancel.tag = indexPath.row
        self.relationPopupView.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.relationPopupView.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
}
extension PersonalRepresentativeStep1VC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldNumber {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
}
class RelationCell: UITableViewCell {
    
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelRelation: UILabel!
    @IBOutlet var labelNumber: UILabel!
    
    func configCellWith(_ relation: Relation) {
        backgroundColor = UIColor.clear
        labelName.text = relation.name
        labelRelation.text = relation.relationShip
        labelNumber.text = relation.phoneNumber
    }
}
class Relation: NSObject {
    var name: String!
    var relationShip: String!
    var phoneNumber: String!
}
