//
//  PersonalRepresentativeStep2VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 11/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PersonalRepresentativeStep2VC: PDViewController {

    @IBOutlet var relationNameLabels: [UILabel]!
    @IBOutlet var relationNumberLabels: [UILabel]!
    @IBOutlet var relationRelationShipLabels: [UILabel]!
    
    @IBOutlet weak var labelExceptions: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBirthDate: UILabel!
    @IBOutlet weak var labelRequestDate: UILabel!
    @IBOutlet weak var labelExpiryDate: UILabel!
    
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    func loadValues() {
        for (idx, relative) in patient.relatives.enumerated() {
            relationNameLabels[idx].text = relative.name
            relationNumberLabels[idx].text = relative.phoneNumber
            relationRelationShipLabels[idx].text = relative.relationShip
        }
        
        labelExceptions.text = patient.exceptions
        labelName.text = patient.firstName + " " + patient.lastName
        labelBirthDate.text = patient.dateOfBirth
        labelRequestDate.text = patient.requestDate
        labelExpiryDate.text = patient.expiryDate
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kPersonalRepresentativeFormVC") as! PersonalRepresentativeFormVC
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
