//
//  PersonalRepresentativeFormVC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 11/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PersonalRepresentativeFormVC: PDViewController {
    
    @IBOutlet var relationNameLabels: [UILabel]!
    @IBOutlet var relationNumberLabels: [UILabel]!
    @IBOutlet var relationRelationShipLabels: [UILabel]!
    @IBOutlet weak var labelExceptions: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBirthDate: UILabel!
    @IBOutlet weak var labelRequestDate: UILabel!
    @IBOutlet weak var labelExpiryDate: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    
    @IBOutlet weak var PatientSignName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        for (idx, relative) in patient.relatives.enumerated() {
            relationNameLabels[idx].text = relative.name
            relationNumberLabels[idx].text = relative.phoneNumber
            relationRelationShipLabels[idx].text = relative.relationShip
        }
        
        labelExceptions.text = patient.exceptions?.value
        labelName.text = patient.firstName + " " + patient.lastName
        labelBirthDate.text = patient.dateOfBirth
        labelRequestDate.text = patient.requestDate?.value
        labelExpiryDate.text = patient.expiryDate?.value
        
        labelDate.text = patient.dateToday
        signatureView.image = patient.signature1
        
        PatientSignName.text = patient.fullName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
