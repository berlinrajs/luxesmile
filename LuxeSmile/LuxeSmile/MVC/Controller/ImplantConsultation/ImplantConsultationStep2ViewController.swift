//
//  ImplantConsultationStep2ViewController.swift
//  SecureDental
//
//  Created by Leojin Bose on 5/14/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ImplantConsultationStep2ViewController: PDViewController {

    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var radioButtonNervous: RadioButton!
    @IBOutlet weak var radioButtonSmoke: RadioButton!
    @IBOutlet weak var radioButtonAspirin: RadioButton!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var textViewIllness: PDTextView!
    
    @IBOutlet weak var textFieldPractioner1: PDTextField!
    @IBOutlet weak var textFieldSpeciality1: PDTextField!
    @IBOutlet weak var textFieldTreatment1: PDTextField!
    
    @IBOutlet weak var textFieldPractioner2: PDTextField!
    @IBOutlet weak var textFieldSpeciality2: PDTextField!
    @IBOutlet weak var textFieldTreatment2: PDTextField!
    
    @IBOutlet weak var textFieldAppointment1: PDTextField!
    
     @IBOutlet weak var textFieldAppointment2: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DateInputView.addDatePickerForTextField(textFieldAppointment1)
        DateInputView.addDatePickerForTextField(textFieldAppointment2)
        
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.firstName + " " + patient.lastName
        
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func radioButtonActionNervous(_ sender: RadioButton) {
        if sender.tag == 1 {
            PopupTextView.sharedInstance.show { (textView, isEdited) in
                if isEdited {
                    self.patient.otherTreatment = textView.text
                } else {
                    sender.isSelected = false
                    self.patient.otherTreatment = "N/A"
                }
            }
        } else {
            self.patient.otherTreatment = "N/A"
        }
        
    }

    
    func setValues() {
        patient.otherIllness = (textViewIllness.text == "TYPE HERE" || textViewIllness.isEmpty) ? "" : textViewIllness.text
        patient.practioner1 = textFieldPractioner1.text
        patient.practioner2 = textFieldPractioner2.text
        patient.speciality1 = textFieldSpeciality1.text
        patient.speciality2 = textFieldSpeciality2.text
        patient.treatment1 = textFieldTreatment1.text
        patient.treatment2 = textFieldTreatment2.text
        patient.appoinment1 = textFieldAppointment1.text
        patient.appoinment2 = textFieldAppointment2.text
        
        if let selected = radioButtonAspirin.selected {
            patient.aspirin = selected.tag
        }
        if let selected = radioButtonSmoke.selected {
            patient.smoke = selected.tag
        }
        if let selected = radioButtonNervous.selected {
            patient.nervous = selected.tag
        }
    }
    
    func loadValues() {
        if let text = patient.otherIllness, !text.isEmpty {
            textViewIllness.text = text
            textViewIllness.textColor = UIColor.white
        }
        textFieldPractioner1.text = patient.practioner1
        textFieldPractioner2.text = patient.practioner2
        textFieldSpeciality1.text = patient.speciality1
        textFieldSpeciality2.text = patient.speciality2
        textFieldTreatment1.text = patient.treatment1
        textFieldTreatment2.text = patient.treatment2
        textFieldAppointment1.text = patient.appoinment1
        textFieldAppointment2.text = patient.appoinment2
        
        if let selected = patient.aspirin {
            radioButtonAspirin.setSelectedWithTag(selected)
        }
        if let selected = patient.smoke {
            radioButtonSmoke.setSelectedWithTag(selected)
        }
        if let selected = patient.nervous {
            radioButtonNervous.setSelectedWithTag(selected)
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findNotSelected() {
            let alert = Extention.alert("MISSING INFORMATION.\nPLEASE SELECT YES OR NO")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate.dateTapped == false {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            patient.signature1 = signatureView.signatureImage()
            let implantConsultationFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kImplantConsultationFormVC") as! ImplantConsultationFormViewController
            implantConsultationFormVC.patient = self.patient
            self.navigationController?.pushViewController(implantConsultationFormVC, animated: true)
        }
        
        
    }
    
    func findNotSelected() -> RadioButton? {
        let buttons = [radioButtonSmoke, radioButtonAspirin, radioButtonNervous]
        for button in buttons {
            if button?.selected == nil {
                return button
            }
        }
        return nil
    }

}


extension ImplantConsultationStep2ViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.white.withAlphaComponent(0.5)
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
