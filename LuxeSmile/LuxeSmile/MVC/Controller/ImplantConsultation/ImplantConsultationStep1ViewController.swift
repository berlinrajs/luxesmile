//
//  ImplantConsultationStep1ViewController.swift
//  SecureDental
//
//  Created by Leojin Bose on 5/14/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class ImplantConsultationStep1ViewController: PDViewController {

    var currentIndex : Int = 0
    var questionIndex: Int = 1
    
    @IBOutlet weak var buttonNext: PDButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonAnswered: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBack?.isHidden = isFromPreviousForm
        activityIndicator.stopAnimating()
        if self.patient.implantConsultation1.count == 0 {
            PDQuestion.fetchImplantConsultationQuestions { (result, success) in
                if success {
                    self.patient.implantConsultation1.append(contentsOf: result[0])
                    self.patient.implantConsultation2.append(contentsOf: result[1])
                    self.patient.implantConsultation3.append(contentsOf: result[2])
                    self.patient.implantConsultation4.append(contentsOf: result[3])
                }
            }
        }
        
        tableViewQuestions.reloadData()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction override func buttonBackAction(_ sender: AnyObject) {
        if currentIndex == 0 {
            if questionIndex == 1 {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.questionIndex -= 1
                buttonAnswered.isSelected = true
                
                self.currentIndex = self.questionIndex == 1 || self.questionIndex == 3 ? 1 : 0
                let labelText = ["DO ANY OF THE FOLLOWING CHIEF COMPLAINTS APPLY TO YOU?", "LIST ANY MEDICATIONS/SUBSTANCES WHICH HAVE CAUSED AN ALLERGIC REACTION:", "LIST ANY MEDICATIONS/SUPPLEMENTS CURRENTLY BEING TAKEN:", "MEDICAL HISTORY (Please indicate dates on questions checked YES)"]
                labelTitle.text = labelText[self.questionIndex - 1]
                self.tableViewQuestions.reloadData()
            }
            if self.currentIndex == 0 && self.questionIndex == 1 {
                self.buttonBack?.isHidden = self.isFromPreviousForm
            }
        } else {
            buttonBack!.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            self.activityIndicator.startAnimating()
            let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.activityIndicator.stopAnimating()
                self.buttonAnswered.isSelected = true
                self.currentIndex = self.currentIndex - 1
                self.tableViewQuestions.reloadData()
                self.buttonBack!.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
                
                if self.currentIndex == 0 && self.questionIndex == 1 {
                    self.buttonBack?.isHidden = self.isFromPreviousForm
                }
            }
        }
        
    }
    
    
    @IBAction func buttonActionAnswered(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    
    @IBAction func buttonNextAction(_ sender: AnyObject) {
        
        if buttonAnswered.isSelected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            buttonAnswered.isSelected = false
            let questions = questionIndex == 1 ? self.patient.implantConsultation1 : questionIndex == 2 ? self.patient.implantConsultation2 : questionIndex == 3 ? self.patient.implantConsultation3 : self.patient.implantConsultation4
            let indexValue = questionIndex == 1 ? 14 : questionIndex == 2 ? 12 : questionIndex == 3 ? 10 : 15
            if ((currentIndex + 1) * indexValue) < questions?.count {
                buttonBack!.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.activityIndicator.startAnimating()
                let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.activityIndicator.stopAnimating()
                    self.currentIndex = self.currentIndex + 1
                    self.tableViewQuestions.reloadData()
                    self.buttonBack!.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
                
            } else {
                self.questionIndex += 1
                if self.questionIndex <= 4 {
                    self.currentIndex = 0
                    let labelText = ["DO ANY OF THE FOLLOWING CHIEF COMPLAINTS APPLY TO YOU?", "LIST ANY MEDICATIONS/SUBSTANCES WHICH HAVE CAUSED AN ALLERGIC REACTION:", "LIST ANY MEDICATIONS/SUPPLEMENTS CURRENTLY BEING TAKEN:", "MEDICAL HISTORY (Please indicate dates on questions checked YES)"]
                    labelTitle.text = labelText[self.questionIndex - 1]
                    self.tableViewQuestions.reloadData()
                } else {
                    self.questionIndex = 4
                    let implantConsultationStep2VC = self.storyboard?.instantiateViewController(withIdentifier: "kImplantConsultationStep2VC") as! ImplantConsultationStep2ViewController
                    implantConsultationStep2VC.patient = self.patient
                    self.navigationController?.pushViewController(implantConsultationStep2VC, animated: true)
                }
            }
            buttonBack?.isHidden = false
        }
    }
    
//    func findEmptyValue() -> PDQuestion? {
//        let objects = implantConsultation1.filter { (obj) -> Bool in
//            return obj.index >= (currentIndex * 19) && obj.index < ((currentIndex + 1) * 19)
//        }
//        for question in objects {
//            if question.isSelected == nil {
//                return question
//            }
//        }
//        return nil
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ImplantConsultationStep1ViewController : MedicalHistoryCellDelegate {
    
    func radioButtonAction(_ sender: RadioButton) {
        
        let questions = questionIndex == 1 ? self.patient.implantConsultation1 : questionIndex == 2 ? self.patient.implantConsultation2 : questionIndex == 3 ? self.patient.implantConsultation3 : self.patient.implantConsultation4
        let obj = questions![sender.tag]
        let checkBoxPopups = ["Jaw locks", "Blood pressure", "Injury to"]
       
       
        func showPopup() {
            PopupTextView.sharedInstance.showWithPlaceHolder("TYPE HERE", completion: { (textView, isEdited) in
                if isEdited == true {
                    obj.answer = textView.text
                    obj.selectedOption = true
                } else {
                    sender.isSelected = false
                    self.tableViewQuestions.reloadData()
                }
            })
        }

        if self.questionIndex != 4 {
            if obj.isAnswerRequired == true {
                showPopup()
            } else {
                obj.selectedOption = true
                if checkBoxPopups.contains(obj.question) {
                    self.tableViewQuestions.reloadData()
                }
            }
        } else {
            if obj.isAnswerRequired == true {
                showPopup()
            } else {
                PopupTextField.sharedInstance.showDatePopupWithTitle("(PLEASE INDICATE DATE)", placeHolder: "SELECT DATE", minDate: nil, maxDate: Date(), completion: { (popUpView, textField, isEdited) in
                    if isEdited == true {
                        obj.answer = textField.text
                        obj.selectedOption = true
                        if checkBoxPopups.contains(obj.question) {
                            self.tableViewQuestions.reloadData()
                        }
                    } else {
                        sender.isSelected = false
                        self.tableViewQuestions.reloadData()
                    }

                })
                
//                PopupTextField.sharedInstance.showDatePopupWithTitle("(PLEASE INDICATE DATE)", placeHolder: "SELECT DATE", minDate : nil, maxDate: NSDate(), completion: { (textField, isEdited) in
//                    if isEdited == true {
//                        obj.answer = textField.text
//                        obj.selectedOption = true
//                        if checkBoxPopups.contains(obj.question) {
//                            self.tableViewQuestions.reloadData()
//                        }
//                    } else {
//                        sender.selected = false
//                        self.tableViewQuestions.reloadData()
//                    }
//                })
            }
        }
    }
}


extension ImplantConsultationStep1ViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let questions = questionIndex == 1 ? self.patient.implantConsultation1 : questionIndex == 2 ? self.patient.implantConsultation2 : questionIndex == 3 ? self.patient.implantConsultation3 : self.patient.implantConsultation4
        let indexValue = questionIndex == 1 ? 14 : questionIndex == 2 ? 12 : questionIndex == 3 ? 10 : 15
        let objects = questions!.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * indexValue) && obj.index < ((currentIndex + 1) * indexValue)
        }
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let checkBoxPopups = ["Jaw locks", "Blood pressure", "Injury to"]
        let questions = questionIndex == 1 ? self.patient.implantConsultation1 : questionIndex == 2 ? self.patient.implantConsultation2 : questionIndex == 3 ? self.patient.implantConsultation3 : self.patient.implantConsultation4
        let indexValue = questionIndex == 1 ? 14 : questionIndex == 2 ? 12 : questionIndex == 3 ? 10 : 15
        let index = (currentIndex * indexValue) + indexPath.row
        let obj = questions![index]
        if checkBoxPopups.contains(obj.question) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellImplantConsultation", for: indexPath) as! ImplantConsultationTableViewCell
            cell.configureCell(obj)
            cell.buttonYes.isSelected = obj.selectedOption!
            cell.buttonYes.tag = index
            cell.buttonNo.tag = index
            cell.delegate = self
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep4", for: indexPath) as! MedicalHistoryStep1TableViewCell
            cell.configureImplantConsultationCell(obj)
            cell.buttonYes.isSelected = obj.selectedOption!
            cell.buttonYes.tag = index
            cell.buttonNo.tag = index
            cell.delegate = self
            return cell

        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let checkBoxPopups = ["Jaw locks", "Blood pressure", "Injury to"]
        let questions = questionIndex == 1 ? self.patient.implantConsultation1 : questionIndex == 2 ? self.patient.implantConsultation2 : questionIndex == 3 ? self.patient.implantConsultation3 : self.patient.implantConsultation4
        let indexValue = questionIndex == 1 ? 14 : questionIndex == 2 ? 12 : questionIndex == 3 ? 10 : 15
        let index = (currentIndex * indexValue) + indexPath.row
        let obj = questions![index]
        if checkBoxPopups.contains(obj.question) {
            return 68
        } else {
            return self.questionIndex == 4 ? 35 : 40
        }
        
    }
}
