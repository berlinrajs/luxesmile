//
//  ImplantConsultationFormViewController.swift
//  SecureDental
//
//  Created by Leojin Bose on 5/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ImplantConsultationFormViewController: PDViewController {

    
    @IBOutlet weak var radioButtonNervous: RadioButton!
    @IBOutlet weak var radioButtonSmoke: RadioButton!
    @IBOutlet weak var radioButtonAspirin: RadioButton!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelIllness: UILabel!
    @IBOutlet weak var labelNervous: UILabel!
    
    @IBOutlet weak var labelPractioner1: UILabel!
    @IBOutlet weak var labelSpeciality1: UILabel!
    @IBOutlet weak var labelTreatment1: UILabel!
    
    @IBOutlet weak var labelPractioner2: UILabel!
    @IBOutlet weak var labelSpeciality2: UILabel!
    @IBOutlet weak var labelTreatment2: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        radioButtonNervous.setSelectedWithTag(patient.nervous)
        radioButtonSmoke.setSelectedWithTag(patient.smoke)
        radioButtonAspirin.setSelectedWithTag(patient.aspirin)
        labelPractioner1.text = patient.practioner1?.value
        labelPractioner2.text = patient.practioner2?.value
        labelSpeciality1.text = patient.speciality1?.value
        labelSpeciality2.text = patient.speciality2?.value
        labelTreatment1.text = patient.treatment1 + "  &  " + patient.appoinment1
        labelTreatment2.text = patient.treatment2 + "  &  " + patient.appoinment2
        labelIllness.text = patient.otherIllness?.value
        labelDate.text = patient.dateToday
        imageViewSignature.image = patient.signature1
        if patient.nervous == 1 {
            labelNervous.text = patient.otherTreatment?.value
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension ImplantConsultationFormViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = [15, 12, 6, 6, 11, 9, 25, 25, 16, 15]
        return count[tableView.tag]
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let checkBoxPopups = ["Jaw locks", "Blood pressure", "Injury to"]
        let start = [0, 15, 0, 6, 0, 11, 0, 25, 50, 66]
        let tag = tableView.tag
        let questions = tag == 0 || tag == 1 ? patient.implantConsultation1 : tag == 2 || tag == 3 ? patient.implantConsultation2 : tag == 4 || tag == 5 ? patient.implantConsultation3 : patient.implantConsultation4
        let index = start[tag] + indexPath.row
        let obj = questions![index]
        if checkBoxPopups.contains(obj.question) && (tag == 1 || tag == 6 || tag == 8) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellConsultationFormCell2", for: indexPath) as! ImplantConsultationFormCell2
            cell.configureCell(obj, tag : tag)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellConsultationFormCell1", for: indexPath) as! ImplantConsultationFormCell1
            cell.configureCell(obj, tag : tag)
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let checkBoxPopups = ["Jaw locks", "Blood pressure", "Injury to"]
        let start = [0, 15, 0, 6, 0, 11, 0, 25, 50, 66]
        let tag = tableView.tag
        let questions = tag == 0 || tag == 1 ? patient.implantConsultation1 : tag == 2 || tag == 3 ? patient.implantConsultation2 : tag == 4 || tag == 5 ? patient.implantConsultation3 : patient.implantConsultation4
        let index = start[tag] + indexPath.row
        let obj = questions![index]
        if checkBoxPopups.contains(obj.question) && (tag == 1 || tag == 6 || tag == 8) {
            return 52
        } else {
            return 26
        }
        
    }
}
