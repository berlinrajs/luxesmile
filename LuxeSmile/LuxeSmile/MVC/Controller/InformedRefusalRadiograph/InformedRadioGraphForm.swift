//
//  InformedRefusalForm.swift
//  LuxeSmile
//
//  Created by SRS on 19/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InformedRadioGraphForm: PDViewController {
    
    
    @IBOutlet var dentistRecomend: [FormLabel]!
    
    @IBOutlet weak var patientName: FormLabel!
    
    @IBOutlet weak var labelDate1: FormLabel!
    
    @IBOutlet weak var labelDate2: FormLabel!

    @IBOutlet weak var parentSign: UIImageView!
    
    @IBOutlet weak var guardianRelationship: FormLabel!
    
    @IBOutlet weak var witnessSign: UIImageView!
    
    @IBOutlet weak var PatientSignName: UILabel!
    
    @IBOutlet weak var btn1: UIButton!
    
    @IBOutlet weak var btn2: UIButton!
    
    @IBOutlet weak var btn3: UIButton!
    
    
    @IBOutlet weak var RadioDocName: RadioButton!
    
    ///////
    override func viewDidLoad() {
        super.viewDidLoad()

//        // Do any additional setup after loading the view.
      
        
        PatientSignName.text = patient.fullName
        RadioDocName.setSelectedWithTag(patient.doctorNameTag!)
        
        
        patient.RadioDentistRecomend == "" ?  patient.getTextNA.setTextForArrayOfLabels(dentistRecomend) :  patient.RadioDentistRecomend.setTextForArrayOfLabels(dentistRecomend)
       
        
        patientName.text = patient.fullName
        
        btn1.isSelected = patient.RadioBtnSelected1
         btn2.isSelected = patient.RadioBtnSelected2
         btn3.isSelected = patient.RadioBtnSelected3
        
        
        labelDate1.text = patient.todayDateTime
        labelDate2.text = patient.todayDateTime
        
        guardianRelationship.text = patient.RadioguardianRelationship
        
        
        parentSign.image = patient.RadioInformedSign1
        witnessSign.image = patient.RadioInformedSign2
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
