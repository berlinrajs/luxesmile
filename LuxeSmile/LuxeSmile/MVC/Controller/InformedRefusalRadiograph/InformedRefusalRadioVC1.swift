//
//  InformedRefusalTreatmentVC1.swift
//  LuxeSmile
//
//  Created by SRS on 18/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InformedRefusalRadioVC1: PDViewController {
    
 
    @IBOutlet weak var textViewDentistRecomend: PDTextView!
    
    @IBOutlet weak var guadianRelationship: PDTextField!
    
    @IBOutlet weak var selectBtn1: UIButton!
    
    @IBOutlet weak var selectBtn2: UIButton!
    
    @IBOutlet weak var selectBtn3: UIButton!
    
    
    @IBOutlet weak var sign1: SignatureView!
    
    @IBOutlet weak var sign2: SignatureView!
   
    @IBOutlet weak var labelDate1: DateLabel!
    
    @IBOutlet weak var labelDate2: DateLabel!
 
    override func viewDidLoad() {
        
        super.viewDidLoad()

        
        self .dateChangedNotification()
        
        // Do any additional setup after loading the view.
        labelDate1.todayDate = patient.todayDateTime
        labelDate2.todayDate = patient.todayDateTime
        
        loadValues()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValues() {
        if let text = patient.RadioguardianRelationship, !text.isEmpty {
            guadianRelationship.text = text
            guadianRelationship.textColor = UIColor.black
        }
        if let text = patient.RadioDentistRecomend, !text.isEmpty {
            textViewDentistRecomend.text = text
            textViewDentistRecomend.textColor = UIColor.black
        }
        if let selected = patient.RadioBtnSelected1 {
            selectBtn1.isSelected = selected
        }
        
        if let selected = patient.RadioBtnSelected2 {
            selectBtn2.isSelected = selected
        }
        if let selected = patient.RadioBtnSelected3 {
            selectBtn3.isSelected = selected
        }
    }
    
    func setValues() {
        patient.RadioBtnSelected1 = selectBtn1.isSelected
        
        patient.RadioBtnSelected2 = selectBtn2.isSelected
        
        patient.RadioBtnSelected3 = selectBtn3.isSelected
        
        patient.RadioguardianRelationship = guadianRelationship.text == "TYPE HERE" ? "" : guadianRelationship.text
        
        patient.RadioDentistRecomend = textViewDentistRecomend.text == "TYPE HERE" ? "" : textViewDentistRecomend.text
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func selectBtnAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
    }
    
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
        patient.todayDateTime = dateFormatter.string(from: Date()).uppercased()
    }

    @IBAction func nextBtnAction(_ sender: AnyObject) {
      
        if !sign1.isSigned() || !sign2.isSigned() {
            
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
            
        }else if labelDate1.text == "Tap to date" || labelDate2.text == "Tap to date"{
            
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
     
            setValues()
            
            patient.RadioInformedSign1 = sign1.signatureImage()
            patient.RadioInformedSign2 = sign2.signatureImage()
            
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "kInformedRadioForm") as! InformedRadioGraphForm
            nextVC.patient = patient
            self.navigationController?.pushViewController(nextVC, animated: true)
            
           
        }
    }
}


extension InformedRefusalRadioVC1 : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
        
        
        
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

