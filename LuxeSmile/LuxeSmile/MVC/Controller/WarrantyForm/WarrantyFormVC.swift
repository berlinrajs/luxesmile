//
//  WarrantyFormVC.swift
//  LuxeSmile
//
//  Created by Manjusha Chembra on 11/10/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class WarrantyFormVC: PDViewController {
    var signature : UIImage!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signatureView.image = patient.patientSignature
        
        labelDate.text = patient.dateToday
        
        labelName.text = patient.fullName

        
    }
    
    
}
