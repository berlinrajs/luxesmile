//
//  WarrantyVC.swift
//  LuxeSmile
//
//  Created by Manjusha Chembra on 11/10/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import Foundation

class WarrantyVC: PDViewController {
    
    
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageSignatureView: SignatureView!
    @IBOutlet weak var buttonNext: PDButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageSignatureView.layer.cornerRadius = 3.0
        labelName.text = patient.fullName
        labelDate.todayDate = patient.dateToday
       
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if !imageSignatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
            
        }else if !labelDate.dateTapped {
            
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        }else {
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kWarrantyFormVC") as! WarrantyFormVC
            formVC.patient = patient
            formVC.signature = imageSignatureView.signatureImage()
            self.navigationController?.pushViewController(formVC, animated: true)
           
        }
    }
}
