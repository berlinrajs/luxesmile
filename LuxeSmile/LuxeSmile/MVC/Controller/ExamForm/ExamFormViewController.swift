//
//  ExamFormViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ExamFormViewController: PDViewController {
    
    @IBOutlet var labelSection1Questions: [UILabel]!
    @IBOutlet var labelSection2Questions: [UILabel]!
    @IBOutlet var labelSection3Questions: [UILabel]!
    @IBOutlet var labelSection6Questions: [UILabel]!
    @IBOutlet var labelSection7Questions: [UILabel]!
    @IBOutlet var labelSection8Questions: [UILabel]!

    
    @IBOutlet weak var labelPatientName: UILabel!
    
    //SECTION1
    @IBOutlet weak var labelMaxillary: UILabel!
    @IBOutlet weak var labelProbingDepth: UILabel!
    @IBOutlet weak var labelPassiveEruption: UILabel!
    @IBOutlet weak var labelActiveEruption: UILabel!
    @IBOutlet weak var labelGummySmileWidth: UILabel!
    @IBOutlet weak var labelGummySmileLength: UILabel!
    @IBOutlet weak var labelZoneLength: UILabel!
    @IBOutlet weak var labelZoneWidth: UILabel!
    @IBOutlet weak var labelTeethShape: UILabel!
    @IBOutlet weak var labelDesiredShade: UILabel!
    @IBOutlet weak var labelOverbite: UILabel!
    @IBOutlet weak var labelOverjet: UILabel!
    @IBOutlet weak var labelMidlineTowards: UILabel!
        
    //SECTION2
    @IBOutlet weak var labelRefer: UILabel!
    @IBOutlet weak var labelTeethMissing: UILabel!
    @IBOutlet weak var labelReplaceTeeth: UILabel!
    
    
    //SECTION3,6
    @IBOutlet weak var radioButtonOcclusion: RadioButton!
    @IBOutlet var buttonPhotos: [UIButton]!
    
    //SECTION7
    @IBOutlet weak var labelOralHygiene: UILabel!

    //SECTION8
    @IBOutlet weak var labelTreatmentLength: UILabel!
    @IBOutlet weak var labelReshaping: UILabel!
    @IBOutlet weak var labelTreatmentTime: UILabel!
    @IBOutlet weak var labelNotes: UILabel!
    
    
    //POPUP
    @IBOutlet weak var labelOcclusalLevelAnswer: UILabel!
    @IBOutlet weak var labelGingivalSymmetryAnswer: UILabel!
    @IBOutlet weak var labelGingivalGraftingAnswer: UILabel!
    @IBOutlet weak var labelGingivectomiesAnswer: UILabel!
    @IBOutlet weak var labelBleachTeethAnswer: UILabel!
    @IBOutlet weak var labelZoneAcceptableAnswer: UILabel!

    @IBOutlet weak var labelUpperMidlineAnswer1: UILabel!
    @IBOutlet weak var labelUpperMidlineAnswer2: UILabel!
    @IBOutlet weak var labelLowerMidlineAnswer1: UILabel!
    @IBOutlet weak var labelLowerMidlineAnswer2: UILabel!
    
    @IBOutlet weak var labelReplacementAnswer: UILabel!
    @IBOutlet weak var labelPerioChartingAnswer: UILabel!
    @IBOutlet weak var labelBlackTrianglesAnswer: UILabel!
    @IBOutlet weak var labelXraysAnswer: UILabel!
    @IBOutlet var labelSpecialAttentionAnswer: [UILabel]!
    @IBOutlet weak var labelLowerIncisorAnswer: UILabel!
    @IBOutlet weak var labelCrossbiteAnswer: UILabel!
    @IBOutlet weak var labelReferralAnswer: UILabel!

    
    
    var examForm: ExamForm {
        return patient.examForm
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelPatientName.text = patient.fullName
        
        //
        labelOcclusalLevelAnswer.text = examForm.section1Questions[1].answer.value
        
        labelGingivalSymmetryAnswer.text = examForm.section1Questions[2].answer.value
        labelGingivalGraftingAnswer.text = examForm.section1Questions[3].answer.value
        labelGingivectomiesAnswer.text = examForm.section1Questions[4].answer.value
        labelBleachTeethAnswer.text = examForm.section1Questions[10].answer.value
        labelZoneAcceptableAnswer.text = examForm.section1Questions[9].answer.value
        let upperMiddle = examForm.section1Questions[11].answer
        let lowerMiddle = examForm.section1Questions[12].answer
        if upperMiddle!.isEmpty {
            labelUpperMidlineAnswer1.text = "N/A"
            labelUpperMidlineAnswer2.text = "N/A"
        } else {
            let text = upperMiddle!.components(separatedBy: "@#@")
            labelUpperMidlineAnswer1.text = text[0]
            labelUpperMidlineAnswer2.text = text[1]
        }
        if lowerMiddle!.isEmpty == true {
            labelLowerMidlineAnswer1.text = "N/A"
            labelLowerMidlineAnswer2.text = "N/A"
        } else {
            let text = lowerMiddle!.components(separatedBy: "@#@")
            labelLowerMidlineAnswer1.text = text[0]
            labelLowerMidlineAnswer2.text = text[1]
        }
        //
        labelReplacementAnswer.text = examForm.section2Questions[0].answer.value
        labelPerioChartingAnswer.text = examForm.section2Questions[3].answer.value
        labelBlackTrianglesAnswer.text = examForm.section2Questions[4].answer.value
        labelXraysAnswer.text = examForm.section2Questions[5].answer.value
        examForm.section2Questions.last!.answer.value.setTextForArrayOfLabels(labelSpecialAttentionAnswer)
        
        //
        labelLowerIncisorAnswer.text = examForm.section3Questions.last!.answer.value
        labelCrossbiteAnswer.text = examForm.section3Questions[4].answer.value

        labelReferralAnswer.text = examForm.section7Questions[3].answer.value

        
        for (idx, question) in patient.examForm.section1Questions.enumerated() {
            let label = labelSection1Questions.filter({ (label) -> Bool in
                return label.tag == idx
            }).first!
            label.text = question.selectedOption == nil ? "N/A" : question.selectedOption == true ? "YES" : "NO"
        }

        for (idx, question) in patient.examForm.section2Questions.enumerated() {
            labelSection2Questions[idx].text = question.selectedOption == nil ? "N/A" : question.selectedOption == true ? "YES" : "NO"
        }
        for (idx, question) in patient.examForm.section3Questions.enumerated() {
            labelSection3Questions[idx].text = question.selectedOption == nil ? "N/A" : question.selectedOption == true ? "YES" : "NO"
        }
        for (idx, question) in patient.examForm.section6Questions.enumerated() {
            labelSection6Questions[idx].text = question.selectedOption == nil ? "N/A" : question.selectedOption == true ? "YES" : "NO"
        }
        for (idx, question) in patient.examForm.section7Questions.enumerated() {
            labelSection7Questions[idx].text = question.selectedOption == nil ? "N/A" : question.selectedOption == true ? "YES" : "NO"
        }
        for (idx, question) in patient.examForm.section8Questions.enumerated() {
            labelSection8Questions[idx].text = question.selectedOption == nil ? "N/A" : question.selectedOption == true ? "YES" : "NO"
        }
        
        //SECTIONS1
        labelMaxillary.text = examForm.maxillary.value
        labelProbingDepth.text = examForm.probingDepth.value
        labelPassiveEruption.text = examForm.passiveEruption.value
        labelActiveEruption.text = examForm.activeEruption.value
        labelGummySmileWidth.text = examForm.gummySmileWidth.value
        labelGummySmileLength.text = examForm.gummySmileLength.value
        labelZoneLength.text = examForm.zoneLength.value
        labelZoneWidth.text = examForm.zoneWidth.value
        labelTeethShape.text = examForm.teethShape.value
        labelDesiredShade.text = examForm.desiredShade.value
        labelOverbite.text = examForm.overbite.value
        labelOverjet.text = examForm.overjet.value
        labelMidlineTowards.text = examForm.midlineTowards.value

        //SECTION2
        labelRefer.text = examForm.refer.value
        labelTeethMissing.text = examForm.teethMissing.value
        labelReplaceTeeth.text = examForm.replaceTeeth.value
        
        
        //SECTION3,6
        if let tag = examForm.occlusionTag {
            radioButtonOcclusion.setSelectedWithTag(tag)
        }
        for button in buttonPhotos {
            button.isSelected = examForm.photoTags.contains(button.tag)
        }
        
        //SECTION7
        labelOralHygiene.text = examForm.oralHygiene.value
        
        //SECTION8
        labelTreatmentLength.text = examForm.treatmentLength.value
        labelReshaping.text = examForm.reshaping.value
        labelTreatmentTime.text = examForm.treatmentTime.value
        labelNotes.text = examForm.notes.value
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
