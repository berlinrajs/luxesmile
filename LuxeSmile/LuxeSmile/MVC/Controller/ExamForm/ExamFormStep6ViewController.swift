//
//  ExamFormStep6ViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ExamFormStep6ViewController: PDViewController {
    
    @IBOutlet var radioButton: [RadioButton]!
//    var section8Questions : [PDQuestion] =  [PDQuestion]()
    
    @IBOutlet weak var buttonVerified: UIButton!
    
    @IBOutlet weak var textFieldTreatmentLength: PDTextField!
    @IBOutlet weak var textFieldReshaping: MCTextField!
    @IBOutlet weak var textFieldTreatmentTime: MCTextField!
    @IBOutlet weak var textViewNotes: MCTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldTreatmentTime.textFormat = .number
//        textFieldReshaping.textFormat = .ToothNumber
        if patient.examForm.section8Questions.count == 0 {
            for index in 0...radioButton.count - 1 {
                let question = PDQuestion()
                question.selectedOption = nil
                question.index = index
                question.answer = ""
                question.resetAnswer = false
                patient.examForm.section8Questions.append(question)
            }
        }
        loadValues()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    func setValues() {
        for (idx, question) in self.patient.examForm.section8Questions.enumerated() {
            if let selectedButton = radioButton[idx].selected {
                question.selectedOption = selectedButton.tag == 1
            } else {
                question.selectedOption = nil
            }
        }
        
        patient.examForm.treatmentLength = textFieldTreatmentLength.text
        patient.examForm.reshaping = textFieldReshaping.text
        patient.examForm.treatmentTime = textFieldTreatmentTime.text
        patient.examForm.notes = textViewNotes.isEmpty ? "" : textViewNotes.text
    }
    
    func loadValues() {
        for (idx, question) in self.patient.examForm.section8Questions.enumerated() {
            if let selected = question.selectedOption {
                radioButton[idx].isSelected = selected
            }
        }
        
        textFieldTreatmentLength.text = patient.examForm.treatmentLength
        textFieldReshaping.text = patient.examForm.reshaping
        textFieldTreatmentTime.text = patient.examForm.treatmentTime
        if let notes = patient.examForm.notes, !notes.isEmpty {
            textViewNotes.text = notes
            textViewNotes.textColor = UIColor.black
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(false)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kExamFormVC") as! ExamFormViewController
            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
}
