//
//  ExamFormStep1ViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ExamFormStep1ViewController: PDViewController {
    
    @IBOutlet weak var textFieldMaxillary: MCTextField!
    @IBOutlet weak var textFieldProbingDepth: PDTextField!
    @IBOutlet weak var textFieldPassiveEruption: PDTextField!
    @IBOutlet weak var textFieldActiveEruption: PDTextField!

    @IBOutlet weak var textFieldGummySmileWidth: MCTextField!
    @IBOutlet weak var textFieldGummySmileLength: MCTextField!

    @IBOutlet var radioButton: [RadioButton]!
    @IBOutlet weak var viewNeedGingive: UIView!
    @IBOutlet weak var viewGummyStyle: UIView!
    
    @IBOutlet weak var buttonVerified: UIButton!

//    var section1Questions : [PDQuestion] =  [PDQuestion]()

    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldMaxillary.textFormat = .number
        textFieldGummySmileWidth.textFormat = .number
        textFieldGummySmileLength.textFormat = .number
        if patient.examForm.section1Questions.count == 0 {
            for index in 0...14 {
                let question = PDQuestion()
                question.selectedOption = nil
                question.index = index
                question.answer = ""
                question.resetAnswer = false
                patient.examForm.section1Questions.append(question)
            }
        }
        
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonActionGummySmile(_ sender: RadioButton) {
        viewGummyStyle.isUserInteractionEnabled = sender.tag == 1
        viewGummyStyle.alpha = sender.tag == 1 ? 1.0 : 0.5
        if sender.tag == 2 {
            radioButton[8].deselectAllButtons()
            textFieldGummySmileWidth.text = ""
            textFieldGummySmileLength.text = ""
        }
    }
    
    
    @IBAction func radioButtonOcclusalLevel(_ sender: RadioButton) {
        let question = patient.examForm.section1Questions[1]
        if sender.tag == 1 {
            question.answer = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Are the centrals coronal or apical to posterior teeth?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            }
        }
    }
    
    @IBAction func radioButtonGingivalSymmetry(_ sender: RadioButton) {
        let question = patient.examForm.section1Questions[2]
        if sender.tag == 1 {
            question.answer = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Which areas are off?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            }
        }
    }
    
    @IBAction func radioButtonGingivalGrafting(_ sender: RadioButton) {
        let question = patient.examForm.section1Questions[3]
        if sender.tag == 2 {
            question.answer = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Which areas?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            }
        }
    }
    

    @IBAction func buttonActionNeedGingive(_ sender: RadioButton) {
        viewNeedGingive.isUserInteractionEnabled = sender.tag == 1
        viewNeedGingive.alpha = sender.tag == 1 ? 1.0 : 0.5
        
        let question = patient.examForm.section1Questions[4]
        if sender.tag == 2 {
            question.answer = ""
            radioButton[5].deselectAllButtons()
            radioButton[6].deselectAllButtons()
            textFieldProbingDepth.text = ""
            textFieldPassiveEruption.text = ""
            textFieldActiveEruption.text = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Which areas?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                    self.viewNeedGingive.isUserInteractionEnabled = false
                    self.viewNeedGingive.alpha = 0.5
                    self.radioButton[5].deselectAllButtons()
                    self.radioButton[6].deselectAllButtons()
                    self.textFieldProbingDepth.text = ""
                    self.textFieldPassiveEruption.text = ""
                    self.textFieldActiveEruption.text = ""
                }
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    func loadValues() {
        textFieldMaxillary.text = patient.examForm.maxillary
        textFieldProbingDepth.text = patient.examForm.probingDepth
        textFieldPassiveEruption.text = patient.examForm.passiveEruption
        textFieldActiveEruption.text = patient.examForm.activeEruption
        textFieldGummySmileWidth.text = patient.examForm.gummySmileWidth
        textFieldGummySmileLength.text = patient.examForm.gummySmileLength
        let questions = patient.examForm.section1Questions[0...8]
        for (idx, question) in questions.enumerated() {
            if let selected = question.selectedOption {
                radioButton[idx].isSelected = selected
                if idx == 7 && selected {
                    viewGummyStyle.isUserInteractionEnabled = true
                    viewGummyStyle.alpha = 1.0
                }
                if idx == 4 && selected {
                    viewNeedGingive.isUserInteractionEnabled = true
                    viewNeedGingive.alpha = 1.0
                }
            }
        }
    }
    
    func setValues() {
        let questions = patient.examForm.section1Questions[0...8]
        for (idx, question) in questions.enumerated() {
            if let selectedButton = radioButton[idx].selected {
                question.selectedOption = selectedButton.tag == 1
            } else {
                question.selectedOption = nil
            }
        }
        patient.examForm.maxillary = textFieldMaxillary.text
        patient.examForm.probingDepth = textFieldProbingDepth.text
        patient.examForm.passiveEruption = textFieldPassiveEruption.text
        patient.examForm.activeEruption = textFieldActiveEruption.text
        patient.examForm.gummySmileWidth = textFieldGummySmileWidth.text
        patient.examForm.gummySmileLength = textFieldGummySmileLength.text
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(false)
        if radioButton[4].isSelected == true && textFieldProbingDepth.isEmpty {
            let alert = Extention.alert("PLEASE ENTER THE PROBING DEPTH")
            self.present(alert, animated: true, completion: nil)
        } else if radioButton[7].isSelected == true && radioButton[8].selected == nil {
            let alert = Extention.alert("PLEASE SELECT THAT GUMMY SMILE AREAS")
            self.present(alert, animated: true, completion: nil)
        } else if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            
            let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kExamFormStep2VC") as! ExamFormStep2ViewController
            step2VC.patient = self.patient
            self.navigationController?.pushViewController(step2VC, animated: true)
        }
    }

}
