//
//  ExamFormStep5ViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ExamFormStep5ViewController: PDViewController {
    
    @IBOutlet var radioButton: [RadioButton]!
    
    
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet weak var textFieldOralHygiene: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if patient.examForm.section7Questions.count == 0 {
            for index in 0...radioButton.count - 1 {
                let question = PDQuestion()
                question.selectedOption = nil
                question.index = index
                question.answer = ""
                question.resetAnswer = false
                patient.examForm.section7Questions.append(question)
            }
        }
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func setValues() {
        for (idx, question) in self.patient.examForm.section7Questions.enumerated() {
            if let selectedButton = radioButton[idx].selected {
                question.selectedOption = selectedButton.tag == 1
            } else {
                question.selectedOption = nil
            }
        }
        patient.examForm.oralHygiene = textFieldOralHygiene.text
    }
    
    func loadValues() {
        for (idx, question) in self.patient.examForm.section7Questions.enumerated() {
            if let selected = question.selectedOption {
                radioButton[idx].isSelected = selected
            }
        }
        textFieldOralHygiene.text = patient.examForm.oralHygiene
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func radioButtonReferral(_ sender: RadioButton) {
        let question = patient.examForm.section7Questions[3]
        if sender.tag == 2 {
            question.answer = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Who and for what?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            }
        }
        //textfield
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(false)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            let step6VC = self.storyboard?.instantiateViewController(withIdentifier: "kExamFormStep6VC") as! ExamFormStep6ViewController
            step6VC.patient = self.patient
            self.navigationController?.pushViewController(step6VC, animated: true)
        }
    }
    
}
