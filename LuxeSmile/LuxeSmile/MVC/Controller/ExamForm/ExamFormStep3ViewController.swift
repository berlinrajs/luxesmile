//
//  ExamFormStep3ViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ExamFormStep3ViewController: PDViewController {
    
    @IBOutlet var radioButton: [RadioButton]!
    
//    var section2Questions : [PDQuestion] =  [PDQuestion]()
    
    @IBOutlet weak var buttonVerified: UIButton!
    
    @IBOutlet weak var textFieldRefer: PDTextField!
    @IBOutlet weak var textFieldTeethMissing: MCTextField!
    @IBOutlet weak var textFieldReplaceTeeth: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        textFieldReplaceTeeth.textFormat = .ToothNumber
//        textFieldTeethMissing.textFormat = .ToothNumber
        if patient.examForm.section2Questions.count == 0 {
            for index in 0...radioButton.count - 1 {
                let question = PDQuestion()
                question.selectedOption = nil
                question.index = index
                question.answer = ""
                question.resetAnswer = false
                patient.examForm.section2Questions.append(question)
            }
        }
        loadValues()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func radioButtonReplacement(_ sender: RadioButton) {
        let question = patient.examForm.section2Questions[0]
        if sender.tag == 2 {
            question.answer = ""
        } else {
            PopupTextView.popUpView().showWithPlaceHolder("Which areas?", completion: { (textView, isEdited) in
                if isEdited {
                    question.answer = textView.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            })
        }
    }
    
    @IBAction func radioButtonPerioCharting(_ sender: RadioButton) {
        let question = patient.examForm.section2Questions[3]
        if sender.tag == 2 {
            question.answer = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Which areas seem to have perio disease?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            }
        }
        //textfield
    }
    
    @IBAction func radioButtonBlackTriangles(_ sender: RadioButton) {
        let question = patient.examForm.section2Questions[4]
        if sender.tag == 2 {
            question.answer = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Which areas?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            }
        }
        //textfield
    }
    
    @IBAction func radioButtonXrays(_ sender: RadioButton) {
        let question = patient.examForm.section2Questions[5]
        if sender.tag == 2 {
            question.answer = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Which areas?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            }
        }
        //textfield
    }
    
    @IBAction func radioButtonSpecialAttention(_ sender: RadioButton) {
        let question = patient.examForm.section2Questions.last!
        if sender.tag == 2 {
            question.answer = ""
        } else {
            PopupTextView.popUpView().showWithPlaceHolder("(Baby tooth, very dark tooth, misshapen tooth etc.)", completion: { (textView, isEdited) in
                if isEdited {
                    question.answer = textView.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            })
        }
        //textview
    }
    
    func setValues() {
        patient.examForm.refer = textFieldRefer.text
        patient.examForm.teethMissing = textFieldTeethMissing.text
        patient.examForm.replaceTeeth = textFieldReplaceTeeth.text
        for (idx, question) in self.patient.examForm.section2Questions.enumerated() {
            if let selectedButton = radioButton[idx].selected {
                question.selectedOption = selectedButton.tag == 1
            } else {
                question.selectedOption = nil
            }
        }
    }
    
    func loadValues() {
        textFieldRefer.text = patient.examForm.refer
        textFieldTeethMissing.text = patient.examForm.teethMissing
        textFieldReplaceTeeth.text = patient.examForm.replaceTeeth
        for (idx, question) in patient.examForm.section2Questions.enumerated() {
            if let selected = question.selectedOption {
                radioButton[idx].isSelected = selected
            }
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(false)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            let step4VC = self.storyboard?.instantiateViewController(withIdentifier: "kExamFormStep4VC") as! ExamFormStep4ViewController
            step4VC.patient = self.patient
            self.navigationController?.pushViewController(step4VC, animated: true)
        }
    }
    
}
