//
//  ExamFormStep4ViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ExamFormStep4ViewController: PDViewController {
    
    @IBOutlet weak var viewPhotosTaken: UIView!
    
    @IBOutlet var radioButton1: [RadioButton]!
    @IBOutlet var radioButton2: [RadioButton]!
    
    
    @IBOutlet weak var buttonVerified: UIButton!
    
    @IBOutlet weak var radioButtonOcclusion: RadioButton!
    @IBOutlet var buttonPhotos: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if patient.examForm.section3Questions.count == 0 {
            for index in 0...radioButton1.count - 1 {
                let question = PDQuestion()
                question.selectedOption = nil
                question.index = index
                question.answer = ""
                question.resetAnswer = false
                patient.examForm.section3Questions.append(question)
            }
        }
        if patient.examForm.section6Questions.count == 0 {
            for index in 0...radioButton2.count - 1 {
                let question = PDQuestion()
                question.selectedOption = nil
                question.index = index
                question.answer = ""
                question.resetAnswer = false
                patient.examForm.section6Questions.append(question)
            }
        }
        for button in buttonPhotos {
            button.addTarget(self, action: #selector(buttonPhotoOptions(_:)), for: .touchUpInside)
        }
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    
    func buttonPhotoOptions(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func radioButtonCrossbite(_ sender: RadioButton) {
        let question = patient.examForm.section3Questions[4]
        if sender.tag == 2 {
            question.answer = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Where?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            }
        }
        //textfield
    }
    
    @IBAction func radioButtonLowerIncisor(_ sender: RadioButton) {
        let question = patient.examForm.section3Questions.last!
        if sender.tag == 2 {
            question.answer = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Which one? (view upper arch to help determine)", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                }
            }
        }
        //textfield
    }
    
    
    @IBAction func buttonActionPhotosTaken(_ sender: RadioButton) {
        viewPhotosTaken.isUserInteractionEnabled = sender.tag == 1
        viewPhotosTaken.alpha = sender.tag == 1 ? 1.0 : 0.5
        if sender.tag == 2 {
            for button in buttonPhotos {
                button.isSelected = false
            }
            patient.examForm.photoTags.removeAll()
        }
    }
    
    func setValues() {
        for (idx, question) in self.patient.examForm.section3Questions.enumerated() {
            if let selectedButton = radioButton1[idx].selected {
                question.selectedOption = selectedButton.tag == 1
            } else {
                question.selectedOption = nil
            }
        }
        for (idx, question) in self.patient.examForm.section6Questions.enumerated() {
            if let selectedButton = radioButton2[idx].selected {
                question.selectedOption = selectedButton.tag == 1
            } else {
                question.selectedOption = nil
            }
        }
        for button in buttonPhotos {
            if button.isSelected == true {
                patient.examForm.photoTags.append(button.tag)
            }
        }
        if let selectedButton = radioButtonOcclusion.selected {
            patient.examForm.occlusionTag = selectedButton.tag
        } else {
            patient.examForm.occlusionTag = nil
        }
    }
    
    func loadValues() {
        for (idx, question) in patient.examForm.section3Questions.enumerated() {
            if let selected = question.selectedOption {
                radioButton1[idx].isSelected = selected
            }
        }
        for (idx, question) in patient.examForm.section6Questions.enumerated() {
            if let selected = question.selectedOption {
                radioButton2[idx].isSelected = selected
                if idx == 0 && selected {
                    viewPhotosTaken.isUserInteractionEnabled = true
                    viewPhotosTaken.alpha = 1.0
                }
            }
        }
        if let occlusionTag = patient.examForm.occlusionTag {
            radioButtonOcclusion.setSelectedWithTag(occlusionTag)
        }
        for button in buttonPhotos {
            button.isSelected = patient.examForm.photoTags.contains(button.tag)
        }
        
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(false)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            setValues()
            let step5VC = self.storyboard?.instantiateViewController(withIdentifier: "kExamFormStep5VC") as! ExamFormStep5ViewController
            step5VC.patient = self.patient
            self.navigationController?.pushViewController(step5VC, animated: true)
        }
    }
    
}
