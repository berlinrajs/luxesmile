//
//  ExamFormStep2ViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ExamFormStep2ViewController: PDViewController {
    
    @IBOutlet var radioButton: [RadioButton]!
    @IBOutlet weak var viewEstheticZone: UIView!
    @IBOutlet weak var viewUpperCanted: UIView!
    
    @IBOutlet weak var textFieldZoneLength: MCTextField!
    @IBOutlet weak var textFieldZoneWidth: MCTextField!
    @IBOutlet weak var textFieldTeethShape: PDTextField!
    @IBOutlet weak var textFieldDesiredShade: PDTextField!
    @IBOutlet weak var textFieldOverbite: MCTextField!
    @IBOutlet weak var textFieldOverjet: MCTextField!
    
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet weak var radioButtonMidlineTowards: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldZoneLength.textFormat = .number
        textFieldZoneWidth.textFormat = .number
        textFieldOverbite.textFormat = .number
        textFieldOverjet.textFormat = .number
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    
    @IBAction func radioButtonBleachTeeth(_ sender: RadioButton) {
        let question = patient.examForm.section1Questions[10]
        PopupTextView.popUpView().showWithPlaceHolder("NOTES", completion: { (textView, isEdited) in
            if isEdited {
                question.answer = textView.text
            } else {
                question.answer = ""
            }
        })
    }
    
    @IBAction func radioButtonUpperMidline(_ sender: RadioButton) {
        let question = patient.examForm.section1Questions[11]
        if sender.tag == 1 {
            question.answer = ""
        } else {
            PopupTextFieldNew.popUpView().show("THE MIDLINE IS(mm)", title2: "OFF TO THE", completion: { (popUpView, textField1, textField2) in
                if !textField1.isEmpty && !textField2.isEmpty {
                    question.answer = "\(textField1.text!)@#@\(textField2.text!)"
                } else {
                    sender.deselectAllButtons()
                    question.answer = ""
                }
            })
        }
        //double text field
    }
    
    @IBAction func radioButtonLowerMidline(_ sender: RadioButton) {
        let question = patient.examForm.section1Questions[12]
        if sender.tag == 1 {
            question.answer = ""
        } else {
            PopupTextFieldNew.popUpView().show("THE MIDLINE IS(mm)", title2: "OFF TO THE", completion: { (popUpView, textField1, textField2) in
                if !textField1.isEmpty && !textField2.isEmpty {
                    question.answer = "\(textField1.text!)@#@\(textField2.text!)"
                } else {
                    sender.deselectAllButtons()
                    question.answer = ""
                }
            })
        }
        //double text field
    }
    
    
    @IBAction func buttonActionEstheticZone(_ sender: RadioButton) {
        viewEstheticZone.isUserInteractionEnabled = sender.tag == 1
        viewEstheticZone.alpha = sender.tag == 1 ? 1.0 : 0.5
        let question = patient.examForm.section1Questions[9]
        if sender.tag == 1 {
            question.answer = ""
            textFieldZoneWidth.text = ""
            textFieldZoneLength.text = ""
        } else {
            PopupTextField.popUpView().showWithTitle("Which areas?", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self) { (popUpView, textField, isEdited) in
                if isEdited {
                    question.answer = textField.text
                } else {
                    question.answer = ""
                    sender.deselectAllButtons()
                    self.viewEstheticZone.isUserInteractionEnabled = false
                    self.viewEstheticZone.alpha = 0.5
                    self.textFieldZoneWidth.text = ""
                    self.textFieldZoneLength.text = ""
                }
            }
        }
        
    }
    
    @IBAction func buttonActionUpperCanted(_ sender: RadioButton) {
        viewUpperCanted.isUserInteractionEnabled = sender.tag == 1
        viewUpperCanted.alpha = sender.tag == 1 ? 1.0 : 0.5
        if sender.tag == 2 {
            radioButtonMidlineTowards.deselectAllButtons()
        }
    }
    
    func setValues() {
        patient.examForm.zoneLength = textFieldZoneLength.text
        patient.examForm.zoneWidth = textFieldZoneWidth.text
        patient.examForm.teethShape = textFieldTeethShape.text
        patient.examForm.desiredShade = textFieldDesiredShade.text
        patient.examForm.overbite = textFieldOverbite.text
        patient.examForm.overjet = textFieldOverjet.text
        patient.examForm.midlineTowards = radioButtonMidlineTowards.selected == nil ? "" : radioButtonMidlineTowards.isSelected ? "LEFT" : "RIGHT"
        let questions = patient.examForm.section1Questions[9...14]
        for (idx, question) in questions.enumerated() {
            if let selectedButton = radioButton[idx].selected {
                question.selectedOption = selectedButton.tag == 1
            } else {
                question.selectedOption = nil
            }
        }
    }
    
    func loadValues() {
        textFieldZoneLength.text = patient.examForm.zoneLength
        textFieldZoneWidth.text = patient.examForm.zoneWidth
        textFieldTeethShape.text = patient.examForm.teethShape
        textFieldDesiredShade.text = patient.examForm.desiredShade
        textFieldOverbite.text = patient.examForm.overbite
        textFieldOverjet.text = patient.examForm.overjet
        if let midlineTowards = patient.examForm.midlineTowards, !midlineTowards.isEmpty {
            radioButtonMidlineTowards.isSelected = midlineTowards == "LEFT"
        }
        let questions = patient.examForm.section1Questions[9...14]
        for (idx, question) in questions.enumerated() {
            if let selected = question.selectedOption {
                radioButton[idx].isSelected = selected
                if idx == 4 && selected {
                    viewUpperCanted.isUserInteractionEnabled = true
                    viewUpperCanted.alpha = 1.0
                }
                if idx == 1 && selected {
                    viewEstheticZone.isUserInteractionEnabled = true
                    viewEstheticZone.alpha = 1.0
                }
            }
        }
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if radioButton[4].isSelected == true && radioButtonMidlineTowards.selected == nil {
            let alert = Extention.alert("PLEASE SELECT, at the incisal edge the midline tips towards the patient's".uppercased())
            self.present(alert, animated: true, completion: nil)
        } else if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            self.view.endEditing(false)
            setValues()
            let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kExamFormStep3VC") as! ExamFormStep3ViewController
            step3VC.patient = self.patient
            self.navigationController?.pushViewController(step3VC, animated: true)
        }
    }
    
}
