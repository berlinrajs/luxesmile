//
//  InHouseBleachingVc.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 24/06/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InHouseBleachingVc: PDViewController {
    
    @IBOutlet weak var imageViewSignature1: SignatureView!
    @IBOutlet weak var imageViewSignature3: SignatureView!
    @IBOutlet weak var imageViewSignature2: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        buttonBack?.isHidden = isFromPreviousForm

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func nextAction(_ sender: AnyObject) {
        if !imageViewSignature1.isSigned() || !imageViewSignature2.isSigned() || !imageViewSignature3.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }
        else{
       let nextViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "InHouseBleachingFormVc") as!
        InHouseBleachingFormVc
        patient.signature1 = imageViewSignature1.signatureImage()
        patient.signature2 = imageViewSignature2.signatureImage()
        patient.signature3 = imageViewSignature3.signatureImage()
        nextViewControllerObj.patient = patient
        self.navigationController?.pushViewController(nextViewControllerObj, animated: true)
        
        }
    }
 

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
