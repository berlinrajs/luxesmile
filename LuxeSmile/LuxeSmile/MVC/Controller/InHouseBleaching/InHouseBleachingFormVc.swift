//
//  InHouseBleachingFormVc.swift
//  LuxeSmile
//
//  Created by SRS Web Solutions on 24/06/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InHouseBleachingFormVc: PDViewController {
    
     @IBOutlet weak var imageViewSignature: UIImageView!
    
    @IBOutlet weak var imageViewSignature2: UIImageView!
    
    @IBOutlet weak var imageViewSignature3: UIImageView!
    
    @IBOutlet weak var labelPatientName: UILabel!
    
    
    @IBOutlet weak var labelDate: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = ("\(patient.firstName) \(patient.lastName)")
        imageViewSignature.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        imageViewSignature3.image = patient.signature3
        
        labelDate.text = patient.dateToday

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
