//
//  DentalReleaseFormViewController.swift
//  LuxeSmile
//
//  Created by Bala Murugan on 6/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalReleaseFormViewController: PDViewController {
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelCity : UILabel!
    @IBOutlet weak var labelState : UILabel!
    @IBOutlet weak var labelZipcode : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var signatureImage : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth
        labelName.text = patient.dentalReleaseName
        labelAddress.text = patient.dentalReleaseAddress
        labelCity.text = patient.dentalReleaseCity
        labelState.text = patient.dentalReleaseState
        labelZipcode.text = patient.dentalReleaseZipcode
        labelDate.text = patient.dateToday
        signatureImage.image = patient.dentalReleaseSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
