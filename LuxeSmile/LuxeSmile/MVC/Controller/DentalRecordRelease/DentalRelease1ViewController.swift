//
//  DentalRelease1ViewController.swift
//  LuxeSmile
//
//  Created by Bala Murugan on 6/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalRelease1ViewController: PDViewController {
    
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var signature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        buttonBack?.isHidden = isFromPreviousForm
        StateListView.addStateListForTextField(textfieldState)
        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValues() {
        textfieldName.text = patient.dentalReleaseName
        textfieldAddress.text = patient.dentalReleaseAddress
        textfieldCity.text = patient.dentalReleaseCity
        if let state = patient.dentalReleaseState {
            textfieldState.text = state
        }
        textfieldZipcode.text = patient.dentalReleaseZipcode
    }
    
    func setValues() {
        patient.dentalReleaseName = textfieldName.text
        patient.dentalReleaseAddress = textfieldAddress.text
        patient.dentalReleaseCity = textfieldCity.text
        patient.dentalReleaseState = textfieldState.text
        patient.dentalReleaseZipcode = textfieldZipcode.text
    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        
        if textfieldName.isEmpty || textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty{
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        }else if !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.present(alert, animated: true, completion: nil)
        }else if !signature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            setValues()
            patient.dentalReleaseSignature = signature.signatureImage()
            let dentalRelease = self.storyboard?.instantiateViewController(withIdentifier: "DentalReleaseFormVC") as! DentalReleaseFormViewController
            dentalRelease.patient = patient
            self.navigationController?.pushViewController(dentalRelease, animated: true)

        }
    }


}

extension DentalRelease1ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldZipcode {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
