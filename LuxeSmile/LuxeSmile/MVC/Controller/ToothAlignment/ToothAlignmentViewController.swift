//
//  ToothAlignmentViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/14/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ToothAlignmentViewController: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelText: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelText.text = labelText.text?.replacingOccurrences(of: "kHardFoodFee", with: "\(patient.hardFoodFee)$").replacingOccurrences(of: "kAppointmentsFee", with: "\(patient.appointmentCancelFee)$")
        labelDate.todayDate = patient.dateToday
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            let form = self.storyboard?.instantiateViewController(withIdentifier: "kToothAlignmentFormVC") as! ToothAlignmentFormViewController
            patient.signature1 = signatureView.signatureImage()
            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        }
    }

}
