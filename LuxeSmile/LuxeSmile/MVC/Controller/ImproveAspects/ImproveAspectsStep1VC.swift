//
//  ImproveAspectsViewController.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImproveAspectsStep1VC: PDViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
//    var improveAspects1 : [PDOption] = [PDOption]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if patient.improveAspects1.count == 0 {
            patient.improveAspects1.append(contentsOf: PDOption.improveAspects1)
            patient.improveAspects2.append(contentsOf: PDOption.improveAspects2)
        }
        
        tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kimproveAspects2VC") as! ImproveAspectsStep2VC
        step2VC.patient = self.patient
        self.navigationController?.pushViewController(step2VC, animated: true)
    }
}

extension ImproveAspectsStep1VC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = patient.improveAspects1[indexPath.row]
        obj.isSelected = !obj.isSelected!
        if obj.question == "OTHER" && obj.isSelected == true {
            PopupTextField.popUpView().showWithTitle("OTHER ASPECTS", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited {
                    obj.answer = textField.text
                } else {
                    obj.isSelected = false
                    tableView.reloadRows(at: [indexPath], with: .automatic)
                }
            })
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 38.0
    }
}

extension ImproveAspectsStep1VC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.improveAspects1.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep2", for: indexPath) as! MedicalHistoryStep2TableViewCell
        let obj = patient.improveAspects1[indexPath.row]
        cell.buttonCheckbox.isSelected = obj.isSelected != nil && obj.isSelected == true
        cell.configureCell(obj)
        return cell
    }
}
