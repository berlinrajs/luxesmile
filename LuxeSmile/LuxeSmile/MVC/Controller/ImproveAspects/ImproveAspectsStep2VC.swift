//
//  ImproveAspectsStep2VC.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImproveAspectsStep2VC: PDViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textViewToDentist: MCTextView!
    
//    var improveAspects2 : [PDOption] = [PDOption]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValues() {
        if let comments = self.patient.comments, !comments.isEmpty {
            textViewToDentist.text = comments
            textViewToDentist.textColor = UIColor.black
        }
    }
    
    func setValues() {
        self.patient.comments = textViewToDentist.isEmpty ? "" : textViewToDentist.text

    }
    
    override func buttonBackAction(_ sender: AnyObject) {
        setValues()
        super.buttonBackAction(sender)
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        setValues()
        let form = self.storyboard?.instantiateViewController(withIdentifier: "kimproveAspectsFormVC") as! ImproveAspectsFormVC
        form.patient = self.patient
        self.navigationController?.pushViewController(form, animated: true)
    }
}

extension ImproveAspectsStep2VC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = patient.improveAspects2[indexPath.row]
        obj.isSelected = !obj.isSelected!
        if obj.question == "OTHER" && obj.isSelected == true {
            PopupTextField.popUpView().showWithTitle("OTHER INTERESTS", placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited {
                    obj.answer = textField.text
                } else {
                    obj.isSelected = false
                    tableView.reloadRows(at: [indexPath], with: .automatic)
                }
            })
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 38.0
    }
}

extension ImproveAspectsStep2VC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.improveAspects2.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep2", for: indexPath) as! MedicalHistoryStep2TableViewCell
        let obj = patient.improveAspects2[indexPath.row]
        cell.buttonCheckbox.isSelected = obj.isSelected != nil && obj.isSelected == true
        cell.configureCell(obj)
        return cell
    }
}
