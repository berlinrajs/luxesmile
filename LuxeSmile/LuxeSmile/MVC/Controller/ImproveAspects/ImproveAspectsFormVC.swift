//
//  ImproveAspectsFormVC.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 11/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImproveAspectsFormVC: PDViewController {
    
    @IBOutlet var checkBoxes1: [UIButton]!
    @IBOutlet var checkBoxes2: [UIButton]!
    
    @IBOutlet weak var labelOthers1: UILabel!
    @IBOutlet weak var labelOthers2: UILabel!
    
    @IBOutlet var labelsArray: [UILabel]!

    @IBOutlet weak var labelName: FormLabel!
    @IBOutlet weak var labelDate: FormLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func loadView() {
        super.loadView()
        for checkBox in checkBoxes1 {
            let obj = patient.improveAspects1[checkBox.tag]
            checkBox.isSelected = obj.isSelected!
            if obj.question == "OTHER" && obj.isSelected == true {
                labelOthers1.text = obj.answer
            }
        }
        for checkBox in checkBoxes2 {
            let obj = patient.improveAspects2[checkBox.tag]
            checkBox.isSelected = obj.isSelected!
            if obj.question == "OTHER" && obj.isSelected == true {
                labelOthers2.text = obj.answer
            }
        }
        self.patient.comments?.setTextForArrayOfLabels(labelsArray)
        labelName.text = patient.fullName
        labelDate.text = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
