//
//  DigitalPhotosViewController.swift
//  LuxeSmile
//
//  Created by Bala Murugan on 7/15/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DigitalPhotosViewController: PDViewController {

    
    @IBOutlet weak var signatureViewPatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelConsent : UILabel!
    var storyBoardId : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        (patient.selectedForms.first!.value(forKey: "formTitle") as! String) == "CONSENT FOR USE OF DIGITAL PHOTOS (FULL FACE/ HEAD SHOT & CLOSE UP SMILE PHOTO)" ? loadFullFace() : loadCloseUpSmile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadFullFace (){
        storyBoardId = "DigitalForm1VC"
        let string : NSString = "I, \(patient.fullName) consent for the use of my photos (Full Face/ Head Shot & Close up Smile photo) for the use of patient education and visual aid. I consent and understand that these pictures may be used on the Center of Cosmetic & General Dentistry, P.A. website, www.luxe-smile.com and in the office Before & After Picture Album as well as any social networking sites such as Facebook & Twitter. Dr. Burghardt and his Team have explained that I may decline this opportunity if I desire. By signing below, I give consent for the use of my digital photos on the website, Before & After Album,Facebook, and/or Twitter." as NSString
        let range = string.range(of: "www.luxe-smile.com")
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 0/255.0, green: 0/255.0, blue: 255.0/255.0, alpha: 1.0), range: range)
        labelConsent.attributedText = attributedString
        
        
        if patient.doctorName == "Dr. Sarah Rittenger" {
            
         labelConsent.text = labelConsent.text?.replacingOccurrences(of: "Dr. Burghardt and his Team", with: patient.doctorName + " " + "her Team")
        
        
        }

    }
    
    func loadCloseUpSmile() {
        storyBoardId = "DigitalForm2VC"

        let string : NSString = "I, \(patient.fullName) consent for the use of my photos (Close up smile photo-­only shows teeth and lips, not the full face) for the use of patient education and visual aid. I consent and understand that these pictures may be used on the Center of Cosmetic & General Dentistry, P.A. website, www.luxe-smile.com and in the office Before & After Picture Album. Dr. Burghardt and his Team have explained that I may decline this opportunity if I desire. By signing below, I give consent for the use of my digital photos on the website and Before & After Album." as NSString
        let range = string.range(of: "www.luxe-smile.com")
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 0/255.0, green: 0/255.0, blue: 255.0/255.0, alpha: 1.0), range: range)
        labelConsent.attributedText = attributedString

        if patient.doctorName == "Dr. Sarah Rittenger" {
            
            labelConsent.text = labelConsent.text?.replacingOccurrences(of: "Dr. Burghardt and his Team", with: patient.doctorName + " " + "her Team")
            
            
        }
    }
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if !signatureViewPatient.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            let step1VC = consentStoryBoard.instantiateViewController(withIdentifier: storyBoardId) as! DigitalPhotosFormViewController
            step1VC.signatureImage = signatureViewPatient.signatureImage()
            step1VC.patient = self.patient
            self.navigationController?.pushViewController(step1VC, animated: true)
            
        }
    }
}
