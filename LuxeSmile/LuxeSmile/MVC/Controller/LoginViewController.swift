//
//  LoginViewController.swift
//  OptimaDentistry
//
//  Created by SRS Web Solutions on 12/07/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class LoginViewController: PDViewController {

    @IBOutlet weak var textFieldUserName: PDTextField!
    @IBOutlet weak var textFieldPassword: PDTextField!
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        self.dateChangedNotification()
        labelVersion.text = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String
        // Do any additional setup after loading the view.
    }
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.string(from: Date()).uppercased()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonSubmitAction() {
        if textFieldUserName.isEmpty || !textFieldUserName.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER THE VALID EMAIL")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldPassword.isEmpty {
            let alert = Extention.alert("PLEASE ENTER THE PASSWORD")
            self.present(alert, animated: true, completion: nil)
        } else {
            self.submitAction()
        }
    }
    
    func submitAction() {
        self.view.endEditing(true)
        if Reachability.isConnectedToNetwork() {
            ServiceManager.loginWithUsername(textFieldUserName.text!, password: textFieldPassword.text!) { (success, error) -> (Void) in
                if success {
                    UserDefaults.standard.set(true, forKey: kAppLoggedInKey)
                    UserDefaults.standard.setValue(self.textFieldUserName.text!, forKey: kAppLoginUsernameKey)
                    UserDefaults.standard.setValue(self.textFieldPassword.text!, forKey: kAppLoginPasswordKey)
                    UserDefaults.standard.synchronize()
                    (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                } else {
                    if error == nil {
                        let alert = Extention.alert("PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN")
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let alert = Extention.alert(error!.localizedDescription.uppercased())
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        } else {
            let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldUserName {
            textFieldPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.submitAction()
        }
        return true
    }
}
