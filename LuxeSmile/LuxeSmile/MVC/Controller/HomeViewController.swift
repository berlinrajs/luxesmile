//
//  ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewController: PDViewController {
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableViewForms: UITableView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var labelVersion : UILabel!
    var consentIndex: Int = 4
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    
    var appointmentCancelFee : String = ""
    var hardFoodFee : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showAlertPopUp), name: NSNotification.Name(rawValue: kFormsCompletedNotification), object: nil)
        
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        self.dateChangedNotification()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.string(from: Date()).uppercased()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            let defaults = UserDefaults.standard
            ServiceManager.loginWithUsername(defaults.value(forKey: kAppLoginUsernameKey) as! String, password: defaults.value(forKey: kAppLoginPasswordKey) as! String) { (success, error) -> (Void) in
                if success {
                    
                } else {
                    if error == nil {
                        
                    } else {
                        DispatchQueue.main.async(execute: {
                            UserDefaults.standard.set(false, forKey: "kApploggedIn")
                            UserDefaults.standard.synchronize()
                            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                        })
                    }
                }
            }
        }
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            if forms != nil {
                self.formList = forms
                self.tableViewForms.reloadData()
            }
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: "LUXE SMILE DENTISTRY", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
//            self.formList = forms
//            self.tableViewForms.reloadData()
        }
    }
    
    @IBAction func buttonActionLogout(_ sender: AnyObject) {
        GTMOAuth2ViewControllerTouch.removeAuthFromKeychain(forName: kKeychainItemName)
        buttonLogout.isHidden = true
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        selectedForms.removeAll()
        for (idx, form) in formList.enumerated() {
            if form.isSelected == true {
                if idx == 4  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
                } else {
                    selectedForms.append(form)
                }
            }
        }
        self.view.endEditing(true)
        if selectedForms.count > 0 {
            selectedForms.sort(by: { (formObj1, formObj2) -> Bool in
                return formObj1.index < formObj2.index
            })
            
            
            if selectedForms.count == 1 && selectedForms.first!.formTitle == kFeedBack {
                let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            } else {
                let patient = PDPatient(forms: selectedForms)
                patient.dateToday = labelDate.text
                patient.appointmentCancelFee = self.appointmentCancelFee
                patient.hardFoodFee = self.hardFoodFee
                let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
                patientInfoVC.patient = patient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            }
            
        } else {
            let alert = Extention.alert("PLEASE SELECT ANY FORM")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func showAlertPopUp() {
        viewAlert.isHidden = false
        viewShadow.isHidden = false
    }
    
    @IBAction func buttonActionOk(_ sender: AnyObject) {
        
        viewAlert.isHidden = true
        viewShadow.isHidden = true
    }
}


extension HomeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == consentIndex {
            let subForms = formList[consentIndex].subForms!
            for subFrom in subForms {
                subFrom.isSelected = false
            }
            
        }

//        let form = formList[indexPath.row]
//        form.isSelected = !form.isSelected
//        tableView.reloadData()
        var form : Forms!
        if (indexPath.row <= consentIndex) {
            form = formList[indexPath.row]
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
        } else {
            form = formList.last
        }
        form.isSelected = !form.isSelected
        if indexPath.row == consentIndex {
            var indexPaths : [IndexPath] = [IndexPath]()
            for (idx, _) in form.subForms.enumerated() {
                let indexPath = IndexPath(row: consentIndex + 1 + idx, section: 0)
                indexPaths.append(indexPath)
            }
            if form.isSelected == true {
                tableView.insertRows(at: indexPaths, with: .bottom)
                let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    if form.isSelected == true {
                        tableView.scrollToRow(at: indexPaths.last!, at: .bottom, animated: true)
                    }
                }
            } else {
                tableView.deleteRows(at: indexPaths, with: .bottom)
            }
            tableView.reloadRows(at: [indexPath], with: .automatic)
        } else  {
            tableView.reloadData()
        }
        
        if form.formTitle == kCosmeticTreatment && form.isSelected == true {
            PopupTextField.popUpView().showWithTitle("TOOTH NUMBERS", placeHolder: "05, 18, 29", keyboardType: UIKeyboardType.numbersAndPunctuation, textFormat: TextFormat.toothNumber, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if textField.isEmpty {
                    form.toothNumber = nil
                    form.isSelected = false
                    self.tableViewForms.reloadData()
                } else {
                    form.toothNumber = textField.text!
                }
            })
        }else if form.formTitle == kCrownOrBridgeVC && form.isSelected == true {
            PopupTextField.popUpView().showWithTitle("TOOTH NUMBERS", placeHolder: "05, 18, 29", keyboardType: UIKeyboardType.numbersAndPunctuation, textFormat: TextFormat.toothNumber, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if textField.isEmpty {
                    form.toothNumber = nil
                    form.isSelected = false
                    self.tableViewForms.reloadData()
                } else {
                    form.toothNumber = textField.text!
                }
            })
        }else if form.formTitle == kImplantConsent && form.isSelected == true {
            PopupTextField.popUpView().showWithTitle("TOOTH NUMBERS", placeHolder: "05, 18, 29", keyboardType: UIKeyboardType.numbersAndPunctuation, textFormat: TextFormat.toothNumber, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if textField.isEmpty {
                    form.toothNumber = nil
                    form.isSelected = false
                    self.tableViewForms.reloadData()
                } else {
                    form.toothNumber = textField.text!
                }
            })
        } else if form.formTitle == kQuickTreatment && form.isSelected == true {
            PopupTextField.popUpView().showWithTitle("ESTIMATED TREATMENT TIME(MONTHS)", placeHolder: "00", keyboardType: UIKeyboardType.numberPad, textFormat: TextFormat.number, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if textField.isEmpty {
                    form.toothNumber = nil
                    form.isSelected = false
                    self.tableViewForms.reloadData()
                } else {
                    form.toothNumber = textField.text!
                }
            })
        } else if form.formTitle == kToothAlignment && form.isSelected == true {
            PopupTextFieldNew.popUpView().show({ (popUpView, textField1, textField2) in
                self.hardFoodFee = textField1.text!
                self.appointmentCancelFee = textField2.text!
            })
        } else if form.formTitle == kCosmeticTreatment || form.formTitle == kCrownOrBridgeVC || form.formTitle == kImplantConsent || form.formTitle == kQuickTreatment {
            form.toothNumber = nil
            form.isSelected = false
            self.tableViewForms.reloadData()
        }
    }
}



extension HomeViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if formList.count > 0{
            let subForms = formList[consentIndex].subForms
            return formList[consentIndex].isSelected == true ? formList.count + subForms!.count  : formList.count
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = [25, 26, 31, 34]
        return (formList[consentIndex].isSelected == true && indexPath.row == 5) || index.contains(indexPath.row) ? 65 : 42
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row <= consentIndex) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm", for: indexPath) as! FormsTableViewCell
            let form = formList[indexPath.row]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as!FormsTableViewCell
            let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm", for: indexPath) as! FormsTableViewCell
            let form = formList[consentIndex + 1]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        }
    }
    
}
