//
//  ToothNumberView.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothNumberView: UIView {

    static let sharedInstance = Bundle.main.loadNibNamed("ToothNumberView", owner: nil, options: nil)!.first as! ToothNumberView
    
    class func popUpView() -> ToothNumberView {
        return Bundle.main.loadNibNamed("ToothNumberView", owner: nil, options: nil)!.first as! ToothNumberView
    }
    
    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    
    var completion:((ToothNumberView, String?)->Void)?
    
    func showWith(_ toothNumber: String, inViewController: UIViewController?, completion : @escaping (_ popUpView: ToothNumberView, _ toothNumbers : String?) -> Void) {
        
        textFieldToothNumbers.text = toothNumber
        
        self.completion = completion
        if inViewController == nil {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        } else {
            inViewController!.view.addSubview(self)
        }
        
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    func close() {
        self.removeFromSuperview()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        self.completion?(self, self.textFieldToothNumbers.isEmpty ? nil : self.textFieldToothNumbers.text!)
        self.removeFromSuperview()
    }
}


extension ToothNumberView : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldToothNumbers {
            return textField.formatToothNumbers(range, string: string)
        }
        return true
    }
    
    
}
