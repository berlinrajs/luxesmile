//
//  MedicalHistoryStep1TableViewCell.swift
//  Luxe Smile
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol MedicalHistoryCellDelegate {
    func radioButtonAction(_ sender : RadioButton)
}

class MedicalHistoryStep1TableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonYes: RadioButton!
    @IBOutlet weak var buttonNo: RadioButton!
    
    var delegate : MedicalHistoryCellDelegate?
    var question : PDQuestion!
    var option : PDOption!
    var impantObj : PDQuestion!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell (_ obj : PDQuestion) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        question = obj
        labelTitle.text = obj.question
        buttonYes.isSelected = obj.selectedOption!
    }
    
    
    func configureCellOption (_ obj : PDOption) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        option = obj
        labelTitle.text = obj.question
    }
    
    func configureImplantConsultationCell (_ obj : PDQuestion) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        question = obj
        labelTitle.text = obj.question
    }

    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        
                if question != nil {
                    if question.isAnswerRequired == true && sender == buttonYes {
                        self.delegate?.radioButtonAction(sender)
                    } else {
                        question.selectedOption = sender == buttonYes
                    }
                } else if option != nil {
                    option.isSelected = sender == buttonYes
                    self.delegate?.radioButtonAction(sender)
                }
        
        
        
        
    }
}
