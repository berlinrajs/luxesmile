//
//  ImplantConsultationTableViewCell.swift
//  SecureDental
//
//  Created by Leojin Bose on 5/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ImplantConsultationTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonYes: RadioButton!
    @IBOutlet weak var buttonNo: RadioButton!
    
    var delegate : MedicalHistoryCellDelegate?
    var question : PDQuestion!
    @IBOutlet weak var buttonCheckBox1: UIButton!
    @IBOutlet weak var buttonCheckBox2: UIButton!
    @IBOutlet weak var buttonCheckBox3: UIButton!
    @IBOutlet weak var buttonCheckBox4: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   
    func configureCell (_ obj : PDQuestion) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        question = obj
        labelTitle.text = obj.question
        let buttons = [buttonCheckBox1, buttonCheckBox2, buttonCheckBox3, buttonCheckBox4]
        for (idx, value) in obj.options!.enumerated() {
            buttons[idx]!.setTitle("\( value.question)", for: .normal)
            buttons[idx]!.isSelected = value.isSelected == true && obj.selectedOption!
            buttons[idx]!.isEnabled = obj.selectedOption!
            buttons[idx]!.alpha = obj.selectedOption == true ? 1.0 : 0.5
        }
        buttons[2]!.isHidden = obj.options!.count < 3
        buttons[3]!.isHidden = obj.options!.count < 4
    }
    
    @IBAction func buttonActionCheckBox(_ sender: UIButton) {
        let option = question.options![sender.tag]
        option.isSelected = !option.isSelected!
        sender.isSelected = option.isSelected == true
        
    }
    
    
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        if sender == buttonYes {
            self.delegate?.radioButtonAction(sender)
        } else {
            question.selectedOption = false
            let buttons = [buttonCheckBox1, buttonCheckBox2, buttonCheckBox3, buttonCheckBox4]
            for (idx, obj) in question.options!.enumerated() {
                obj.isSelected = false
                buttons[idx]!.isSelected = false
                buttons[idx]!.isEnabled = false
                buttons[idx]!.alpha = 0.5
            }
        }
    }
}
