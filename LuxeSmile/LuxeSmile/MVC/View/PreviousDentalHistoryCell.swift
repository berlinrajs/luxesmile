//
//  MedicalHistoryStep1TableViewCell.swift
//  SecureDental
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol PreviousDentalHistoryCellDelegate {
    func radioButtonAction(_ sender : RadioButton)
}

class PreviousDentalHistoryCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonYes: RadioButton!
    @IBOutlet weak var buttonNo: RadioButton!
    
    var delegate : PreviousDentalHistoryCellDelegate?
    var question : PDQuestion!
    var option : PDOption!
    var impantObj : PDQuestion!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell (_ obj : PDQuestion) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        question = obj
        labelTitle.text = obj.question
        buttonYes.isSelected = obj.selectedOption == true
    }
    
    func configureImplantConsultationCell (_ obj : PDQuestion) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        impantObj = obj
        labelTitle.text = obj.question
    }
    
    func configureCellOption (_ obj : PDOption) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        option = obj
        labelTitle.text = obj.question
    }
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        if impantObj != nil {
            if sender == buttonYes {
                self.delegate?.radioButtonAction(sender)
            } else {
                impantObj.selectedOption = false
            }
        } else if question != nil {
            if question.isAnswerRequired == true && sender == buttonYes {
                self.delegate?.radioButtonAction(sender)
            } else {
                question.selectedOption = sender == buttonYes
            }
        } else if option != nil {
            option.isSelected = sender == buttonYes
            self.delegate?.radioButtonAction(sender)
        }
        
    }
}
