//
//  ImplantConsultationFormCell1.swift
//  SecureDental
//
//  Created by Leojin Bose on 5/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ImplantConsultationFormCell1: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonYes: RadioButton!
    @IBOutlet weak var buttonNo: RadioButton!
    @IBOutlet weak var labelText: FormLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell (_ obj : PDQuestion, tag : Int) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        labelTitle.text = obj.question
        buttonYes.isSelected = obj.selectedOption!
//        if obj.selectedOption == true && obj.isAnswerRequired == true && (tag == 6 || tag == 7 || tag == 8 || tag == 9) {
//            labelText.text = obj.answer == nil ? "N/A" : obj.answer
//            labelText.hidden = false
//        } else {
//            labelText.text = ""
//            labelText.hidden = true
//        }

        if obj.selectedOption == true && obj.isAnswerRequired == true  {
            labelText.text = obj.answer == nil ? "N/A" : obj.answer
            labelText.isHidden = false
        } else {
            labelText.text = ""
            labelText.isHidden = true
        }


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
