//
//  MedicalHistoryStep2TableViewCell.swift
//  Luxe Smile
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep2TableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonCheckbox: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell (_ obj : PDOption) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        labelTitle.text = obj.question
    }

}
