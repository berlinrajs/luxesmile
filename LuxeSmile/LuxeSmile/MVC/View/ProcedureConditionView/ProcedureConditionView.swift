//
//  ProcedureConditionView.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ProcedureConditionView: UIView {

    static let sharedInstance = Bundle.main.loadNibNamed("ProcedureConditionView", owner: nil, options: nil)!.first as! ProcedureConditionView
    
    class func popUpView() -> ProcedureConditionView {
        return Bundle.main.loadNibNamed("ProcedureConditionView", owner: nil, options: nil)!.first as! ProcedureConditionView
    }
    
    @IBOutlet weak var textFieldProcedure: PDTextField!
    @IBOutlet weak var textFieldCondition: PDTextField!
    
    var completion:((ProcedureConditionView, String?, String?)->Void)?
    
    func showWith(_ condition: String, procedure: String, inViewController: UIViewController?, completion : @escaping (_ popUpView: ProcedureConditionView, _ procedure : String?, _ condition : String?) -> Void) {
        
        textFieldCondition.text = condition
        textFieldProcedure.text = procedure
        
        self.completion = completion
        if inViewController == nil {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        } else {
            inViewController!.view.addSubview(self)
        }
        
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    func close() {
        self.removeFromSuperview()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        self.completion?(self, self.textFieldProcedure.isEmpty ? nil : self.textFieldProcedure.text!, self.textFieldCondition.isEmpty ? nil : self.textFieldCondition.text!)
        self.removeFromSuperview()
    }
}
