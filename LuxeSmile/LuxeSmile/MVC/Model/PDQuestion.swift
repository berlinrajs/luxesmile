//
//  PDQuestion.swift
//  Luxe Smile
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDQuestion: NSObject {
    var question : String!
    var isAnswerRequired : Bool!
    var yesOrNo : Bool!
    var answer : String!
    var index : Int!
    var options: [PDOption]?
    var resetAnswer: Bool = true
    

    var selectedOption : Bool? = false {
        didSet {
            if selectedOption == false && resetAnswer == true {
                answer = nil
            }
        }
    }
    override init() {
        super.init()
    }
    
    init(question : (String, Bool)) {
        super.init()
        self.question = question.0
        self.isAnswerRequired = question.1
    }


    
    init(dict : NSDictionary) {
        super.init()
        self.question = dict["question"] as! String
        self.isAnswerRequired = (dict["verification"] as! String) == "Yes"
    }
    
    class func getObjects (_ arrayResult : NSArray) -> [PDQuestion] {
        var questions  = [PDQuestion]()
        for dict in arrayResult {
            let obj = PDQuestion(dict: dict as! NSDictionary)
            obj.selectedOption = false
            questions.append(obj)
        }
        return questions
    }
    
    
    class func getJSONObject(_ responseString : String) -> AnyObject? {
        do {
            let object = try JSONSerialization.jsonObject(with: responseString.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.allowFragments)
            return object as AnyObject
        } catch {
            return nil
        }
    }

    
    class func fetchQuestionsForm1(_ completion:(_ result : [PDQuestion]?, _ success : Bool) -> Void) {
        
          let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Are you under a physician's care now?\",\"verification\":\"Yes\"},{\"question\":\"Have you ever had a serious head or neck injury?\",\"verification\":\"Yes\"},{\"question\":\"Are you taking any medications, pills or drugs?\",\"verification\":\"Yes\"},{\"question\":\"Have you ever taken Fosamax, Boniva, Actonel or any other medications containing bisphosphonates?\",\"verification\":\"Yes\"},{\"question\":\"Do you take, or have you taken, Phen-fen or Redux? \",\"verification\":\"Yes\"},{\"question\":\"Have you ever been hospitalized or had a major operation?\",\"verification\":\"Yes\"},{\"question\":\"Do you need pre-med?\",\"verification\":\"Yes\"},{\"question\":\"Are you on a special diet?\",\"verification\":\"No\"},{\"question\":\"Do you use tobacco?\",\"verification\":\"No\"}]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
//        ServiceManager.fetchDataFromService("general_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
    
    
    class func fetchQuestionsPreviousDentalForm2(_ completion:(_ result : [PDQuestion]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Do you have any dental implants, partials or dentures?\",\"verification\":\"Yes\"},{\"question\":\"Do you grind your teeth, either consciously or been told you do so in your sleep? \",\"verification\":\"Yes\"},{\"question\":\"Do any of your teeth currently cause pain/discomfort?\",\"verification\":\"Yes\"},{\"question\":\"Do your gums bleed when you brush or floss?\",\"verification\":\"Yes\"}]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
    }

    
    class func fetchQuestionsPremedQuesForm(_ completion:(_ result : [PDQuestion]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\" Have you been told by your medical doctor that you need to take a premedication antibiotic before a dental visit?\",\"verification\":\"Yes\"},{\"question\":\" Do you have any cardiac conditions, i.e. artificial heart valves or transplant? Serious congenital heart conditions? Joint replacement? (If yes, which one?)  \",\"verification\":\"Yes\"},{\"question\":\"Do you have any allergies? i.e. Penicillin, Latex, etc.\",\"verification\":\"Yes\"}]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
    }
    
    
    class func fetchImplantConsultationQuestions(_ completion:(_ result : [[PDQuestion]], _ success : Bool) -> Void) {
        let questions1 = [["Diet limited to semisolid food or soft foods" : false],
                          ["Mouth sores" : false],
                          ["Diet limited to liquid foods" : false],
                          ["Numbness in lower lip" : false],
                          ["Difficulty chewing" : false],
                          ["Numbness in jawbone" : false],
                          ["Difficulty speaking" : false],
                          ["Tingling in jaw bone" : false],
                          ["Difficulty swallowing" : false],
                          ["Nutritional disorders" : false],
                          ["Digestive problems" : false],
                          ["Pain in jaw bone" : false],
                          ["Facial pain" : false],
                          ["Are you currently in pain?" : true],
                          ["Do you feel your oral condition is affecting your general health in any way?" : true],
                          ["Jaw locks" : false],
                          ["Limited opening of jaw" : false],
                          ["Teeth do not meet properly" : false],
                          ["Loss of teeth" : false],
                          ["Poorly fitting dental appliance" : false],
                          ["Pain in jaw joint" : false],
                          ["Gagging easily" : false],
                          ["Pain when swallowing" : false],
                          ["Head Pain" : false],
                          ["Pain when chewing" : false],
                          ["Jaw clicks" : false],
                          ["Other" : false]]
        
        let questions2 = [["Antibiotics" : false],
                          ["Aspirin" : false],
                          ["Barbiturates" : false],
                          ["Codeine" : false],
                          ["Lidocaine" : false],
                          ["Latex" : false],
                          ["Metals" : false],
                          ["Plastic" : false],
                          ["Sedative" : false],
                          ["Sleeping Pill" : false],
                          ["Local anesthetics" : false],
                          ["Sulfa drugs" : false]]
        
        let questions3 = [["Antibiotics" : false],
                          ["Insulin" : false],
                          ["Anticoagulants" : false],
                          ["Muscle relaxants" : false],
                          ["Barbiturates" : false],
                          ["Nerve Pills" : false],
                          ["Blood Thinners" : false],
                          ["Pain Medication" : false],
                          ["Codeine" : false],
                          ["Sleeping Pills" : false],
                          ["Other" : true],
                          ["Cortisone" : false],
                          ["Sulfa Drugs" : false],
                          ["Ginko Biloba" : false],
                          ["Diet Pills" : false],
                          ["Heart Medication" : false],
                          ["Tranquilizers" : false],
                          ["Medications for Osteoporosis" : false],
                          ["Bisphosphonates" : false],
                          ["Herbal Supplements" : false]]
        
        let questions4 = [["Abnormal bleeding after surgery or injury" : false],
                          ["Anemia" : false],
                          ["Allergic Rhinitis" : false],
                          ["Arteriosclerosis" : false],
                          ["Asthma" : false],
                          ["Autoimmune disorders" : false],
                          ["Bleeding easily" : false],
                          ["Bloating" : false],
                          ["Blood pressure" : false],
                          ["Bruising easily" : false],
                          ["Cancer" : false],
                          ["Chemotherapy" : false],
                          ["Chronic Bronchitis" : false],
                          ["Chronic fatigue" : false],
                          ["Chronic mouth dryness" : false],
                          ["Cold hands & feet" : false],
                          ["Colitis" : false],
                          ["Current pregnancy" : false],
                          ["Depression" : false],
                          ["Diabetes" : false],
                          ["Dizziness" : false],
                          ["Emphysema" : false],
                          ["Epilepsy" : false],
                          ["Excessive thirst" : false],
                          ["Fainting spells" : false],
                          
                          ["Heart Disorder" : false],
                          ["Heart Pacemaker" : false],
                          ["Heart valve replacement" : false],
                          ["Hemophilia" : false],
                          ["Hepatitis" : false],
                          ["Hypoglycemia" : false],
                          ["Immune system disorder" : false],
                          ["Insomnia" : false],
                          ["Intestinal Disorders" : false],
                          ["Jaw joint surgery" : false],
                          ["Kidney problems" : false],
                          ["Liver disease" : false],
                          ["Menstrual Cramps" : false],
                          ["Multiple sclerosis" : false],
                          ["Muscle aches" : false],
                          ["Muscle shaking (tremors)" : false],
                          ["Muscle spasms or cramps" : false],
                          ["Muscular dystrophy" : false],
                          ["Nasal stuffiness in the morning" : false],
                          ["Nervousness" : false],
                          ["Neuralgia" : false],
                          ["Osteoporosis" : false],
                          ["Ovarian cysts" : false],
                          ["Parkinson’s disease" : false],
                          ["Poor circulation" : false],
                          
                          ["Fluid retention" : false],
                          ["Frequent cough" : false],
                          ["Frequent illness" : false],
                          ["Frequent stressful situations" : false],
                          ["Glaucoma" : false],
                          ["Gout" : false],
                          ["Hay fever" : false],
                          ["Headaches" : false],
                          ["Hearing impairment" : false],
                          ["Heart murmur" : false],
                          ["Injury to" : false],
                          ["Needing extra pillows to breath at night" : false],
                          ["Tumors" : false],
                          ["Urinary disorders" : false],
                          ["Tuberculosis" : false],
                          ["Other Medical/ Dental History:" : true],
                          
                          ["Prior orthodontic treatment" : false],
                          ["Psychiatric treatment" : false],
                          ["Rheumatoid arthritis b 0" : false],
                          ["Rheumatic fever" : false],
                          ["Scarlet fever" : false],
                          ["Seizures" : false],
                          ["Shortness of breath" : false],
                          ["slow healing sores" : false],
                          ["Sickle Cell Anemia" : false],
                          ["Sinus problems" : false],
                          ["Speech difficulties" : false],
                          ["Stomach ulcers" : false],
                          ["Stroke" : false],
                          ["Swelling of ankles" : false],
                          ["Tendency for Frequent Colds" : false]]
        
        
        
        
        let result1 = self.getQuestionObjects(questions1)
        let result2 = self.getQuestionObjects(questions2)
        let result3 = self.getQuestionObjects(questions3)
        let result4 = self.getQuestionObjects(questions4)
        completion([result1, result2, result3, result4], true)
    }

    
    
//    class func fetchImplantConsultationQuestions(completion:(result : [[PDQuestion]], success : Bool) -> Void) {
//        let questions1 = [["Diet limited to semisolid food or soft foods" : false],
//                          ["Mouth sores" : false],
//                          ["Diet limited to liquid foods" : false],
//                          ["Numbness in lower lip" : false],
//                          ["Difficulty chewing" : false],
//                          ["Numbness in jawbone" : false],
//                          ["Difficulty speaking" : false],
//                          ["Tingling in jaw bone" : false],
//                          ["Difficulty swallowing" : false],
//                          ["Nutritional disorders" : false],
//                          ["Digestive problems" : false],
//                          ["Pain in jaw bone" : false],
//                          ["Facial pain" : false],
//                          ["Are you currently in pain?" : true],
//                          ["Do you feel your oral condition is affecting your general health in any way?" : true],
//                          ["Jaw locks" : false],
//                          ["Limited opening of jaw" : false],
//                          ["Teeth do not meet properly" : false],
//                          ["Loss of teeth" : false],
//                          ["Poorly fitting dental appliance" : false],
//                          ["Pain in jaw joint" : false],
//                          ["Gagging easily" : false],
//                          ["Pain when swallowing" : false],
//                          ["Head Pain" : false],
//                          ["Pain when chewing" : false],
//                          ["Jaw clicks" : false],
//                          ["Other" : false]]
//        
//        let questions2 = [["Antibiotics" : false],
//                          ["Aspirin" : false],
//                          ["Barbiturates" : false],
//                          ["Codeine" : false],
//                          ["Lidocaine" : false],
//                          ["Latex" : false],
//                          ["Metals" : false],
//                          ["Plastic" : false],
//                          ["Sedative" : false],
//                          ["Sleeping Pill" : false],
//                          ["Local anesthetics" : false],
//                          ["Sulfa drugs" : false]]
//        
//        let questions3 = [["Antibiotics" : false],
//                          ["Insulin" : false],
//                          ["Anticoagulants" : false],
//                          ["Muscle relaxants" : false],
//                          ["Barbiturates" : false],
//                          ["Nerve Pills" : false],
//                          ["Blood Thinners" : false],
//                          ["Pain Medication" : false],
//                          ["Codeine" : false],
//                          ["Sleeping Pills" : false],
//                          ["Other" : true],
//                          ["Cortisone" : false],
//                          ["Sulfa Drugs" : false],
//                          ["Ginko Biloba" : false],
//                          ["Diet Pills" : false],
//                          ["Heart Medication" : false],
//                          ["Tranquilizers" : false],
//                          ["Medications for Osteoporosis" : false],
//                          ["Bisphosphonates" : false],
//                          ["Herbal Supplements" : false]]
//        
//        let questions4 = [["Abnormal bleeding after surgery or injury" : false],
//                          ["Anemia" : false],
//                          ["Allergic Rhinitis" : false],
//                          ["Arteriosclerosis" : false],
//                          ["Asthma" : false],
//                          ["Autoimmune disorders" : false],
//                          ["Bleeding easily" : false],
//                          ["Bloating" : false],
//                          ["Blood pressure" : false],
//                          ["Bruising easily" : false],
//                          ["Cancer" : false],
//                          ["Chemotherapy" : false],
//                          ["Chronic Bronchitis" : false],
//                          ["Chronic fatigue" : false],
//                          ["Chronic mouth dryness" : false],
//                          ["Cold hands & feet" : false],
//                          ["Colitis" : false],
//                          ["Current pregnancy" : false],
//                          ["Depression" : false],
//                          ["Diabetes" : false],
//                          ["Dizziness" : false],
//                          ["Emphysema" : false],
//                          ["Epilepsy" : false],
//                          ["Excessive thirst" : false],
//                          ["Fainting spells" : false],
//                          
//                          ["Heart Disorder" : false],
//                          ["Heart Pacemaker" : false],
//                          ["Heart valve replacement" : false],
//                          ["Hemophilia" : false],
//                          ["Hepatitis" : false],
//                          ["Hypoglycemia" : false],
//                          ["Immune system disorder" : false],
//                          ["Insomnia" : false],
//                          ["Intestinal Disorders" : false],
//                          ["Jaw joint surgery" : false],
//                          ["Kidney problems" : false],
//                          ["Liver disease" : false],
//                          ["Menstrual Cramps" : false],
//                          ["Multiple sclerosis" : false],
//                          ["Muscle aches" : false],
//                          ["Muscle shaking (tremors)" : false],
//                          ["Muscle spasms or cramps" : false],
//                          ["Muscular dystrophy" : false],
//                          ["Nasal stuffiness in the morning" : false],
//                          ["Nervousness" : false],
//                          ["Neuralgia" : false],
//                          ["Osteoporosis" : false],
//                          ["Ovarian cysts" : false],
//                          ["Parkinson’s disease" : false],
//                          ["Poor circulation" : false],
//                          
//                          ["Fluid retention" : false],
//                          ["Frequent cough" : false],
//                          ["Frequent illness" : false],
//                          ["Frequent stressful situations" : false],
//                          ["Glaucoma" : false],
//                          ["Gout" : false],
//                          ["Hay fever" : false],
//                          ["Headaches" : false],
//                          ["Hearing impairment" : false],
//                          ["Heart murmur" : false],
//                          ["Injury to" : false],
//                          ["Needing extra pillows to breath at night" : false],
//                          ["Tumors" : false],
//                          ["Urinary disorders" : false],
//                          ["Tuberculosis" : false],
//                          ["Other Medical/ Dental History:" : true],
//                          
//                          ["Prior orthodontic treatment" : false],
//                          ["Psychiatric treatment" : false],
//                          ["Rheumatoid arthritis b 0" : false],
//                          ["Rheumatic fever" : false],
//                          ["Scarlet fever" : false],
//                          ["Seizures" : false],
//                          ["Shortness of breath" : false],
//                          ["slow healing sores" : false],
//                          ["Sickle Cell Anemia" : false],
//                          ["Sinus problems" : false],
//                          ["Speech difficulties" : false],
//                          ["Stomach ulcers" : false],
//                          ["Stroke" : false],
//                          ["Swelling of ankles" : false],
//                          ["Tendency for Frequent Colds" : false]]
//        
//        let result1 = self.getQuestionObjects(questions1)
//        let result2 = self.getQuestionObjects(questions2)
//        let result3 = self.getQuestionObjects(questions3)
//        let result4 = self.getQuestionObjects(questions4)
//        completion(result: [result1, result2, result3, result4], success: true)
//    }
    
    class func getQuestionObjects (_ questions: [[String : Bool]]) -> [PDQuestion] {
        let checkBoxPopups = ["Jaw locks" : ["Upper", "Lower"], "Blood pressure" : ["High", "Low"], "Injury to" : ["Face", "Neck", "Mouth", "Teeth"]]
        
        var questionObjects  = [PDQuestion]()
        for (idx, question) in questions.enumerated() {
            let obj = PDQuestion(question: question.first!)
            obj.index = idx
            obj.selectedOption = false
            if checkBoxPopups.keys.contains(obj.question) {
                let options = checkBoxPopups.filter({ (val) -> Bool in
                    return val.0 == obj.question
                })[0]
                var optionObjects  = [PDOption]()
                for opt in options.1 {
                    let optObj = PDOption(value: opt)
                    optObj.isSelected = false
                    optionObjects.append(optObj)
                }
                obj.options = optionObjects
            }
            questionObjects.append(obj)
        }
        return questionObjects
    }

}


class PDOption : NSObject {
    var question : String!
    var isSelected : Bool?
    var index : Int!
    var answer : String!

    init(value : String) {
        super.init()
        self.question = value
    }
    
    class func getObjects (_ arrayResult : NSArray) -> [PDOption] {
        var questions  = [PDOption]()
        for (idx, value) in arrayResult.enumerated() {
            let obj = PDOption(value: value as! String)
            obj.isSelected = false
            obj.index = idx
            questions.append(obj)
        }
        return questions
    }
    
    
    class var improveAspects1 : [PDOption] {
        let options = ["CROWDING/CROOKED TEETH", "JAW JOINT PAIN", "SPACES", "MISSING TEETH", "TOOTH SHAPE", "DARK TEETH", "TOOTH SIZE", "SPEECH PROBLEMS", "GUMMY SMILE", "OVERBITE", "UNDERBITE", "FACIAL PROFILE", "TEETH ARE DIFFERENT COLORS", "UGLY OLD CROWNS", "OTHER"]
        return self.getObjects(options as NSArray)
    }
    
    class var improveAspects2 : [PDOption] {
        let options = ["SIX MONTHSMILES (Short-term orthodontic treatment)", "TEETH WHITENING", "VENEERS", "OTHER"]
        return self.getObjects(options as NSArray)
    }
    
    class func fetchQuestionsForm2(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\", \"posts\":[\"Pregnant \\/ trying to get pregnant\", \"Nursing\", \"Taking oral contraceptives\", \"None\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
//        ServiceManager.fetchDataFromService("women_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
    
    class func fetchQuestionsForm3(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Aspirin\",\"Penicillin\",\"Codeine\",\"Acrylic\",\"Metal\",\"Latex\",\"Sulfa drugs\",\"Local anesthetics\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
        
//        ServiceManager.fetchDataFromService("alergic_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
    
    class func fetchQuestionsForm4(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[\"AIDS\\/HIV Positive\",\"Alzheimers disease\",\"Anaphylaxis\",\"Anemia\",\"Angina\",\"Arthritis/Gout\",\"Artificial heart valve\",\"Artificial Joints\",\"Asthma\",\"Blood Disease\",\"Blood Transfusion\",\"Breathing Problems\",\"Bruise Easily\",\"Cancer\",\"Chemotherapy\",\"Chest pains\",\"Cold sores \\/ Fever blisters\",\"Congenital heart disorder\",\"Convulsion\",\"Cortisone medicine\",\"Diabetes\",\"Drug Addiction\",\"Easily Winded\",\"Emphysema\",\"Epilepsy or Seizures\",\"Excessive Bleeding\",\"Excessive Thirst\",\"Fainting spells \\/ Dizziness\",\"Frequent Cough\",\"Frequent Diarrhea\",\"Frequent Headaches\",\"Genital Herpes\",\"Glaucoma\",\"Hay Fever\",\"Heart Attack \\/ Failure\",\"Heart Murmur\",\"Heart Pacemaker\",\"Heart Trouble \\/ Disease\",\"Hemophilia\",\"Hepatitis A\",\"Hepatitis B or C\",\"Herpes\",\"High Blood Pressure\",\"High Cholesterol\",\"Hives or Rash\",\"Hypoglycemia\",\"Irregular Heartbeat\",\"Kidney Problem\",\"Leukemia\",\"Liver Diseases\",\"Low Blood Pressure\",\"Lung diseases\",\"Mitral Valve Prolapse\",\"Osteoporosis\",\"Pain in Jaw Joints\",\"Parathyroid Disease\",\"Psychiatric Care\",\"Radiation Treatments\",\"Recent Weight Loss\",\"Renal Dialysis\",\"Rheumatic Fever\",\"Rheumatism\",\"Scarlet Fever\",\"Shingles\",\"Sickle Cell Disease\",\"Sinus Trouble\",\"Spina Bifida\",\"Stomach\\/Intestinal Disease\",\"Stroke\",\"Swelling of Limbs\",\"Thyroid Disease\",\"Tonsillitis\",\"Tuberculosis\",\"Tumors or Growths\",\"Ulcers\",\"Venereal Disease\",\"Yellow Jaundice\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
//        ServiceManager.fetchDataFromService("disease_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
    
    class func fetchQuestionsToothExtractionForm1(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Pain\",\"Infection\",\"Decay\",\"Gum Disease\",\"Broken tooth\",\"Non Restorable\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
    }
    
    class func fetchQuestionsToothExtractionForm2(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\",\"posts\":[\"No Treatment\",\"Root Canal Therapy\",\"Filling\",\"Crowns\",\"Gum Treatment\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
    }
    
    
    

}
