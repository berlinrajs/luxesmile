//
//  PDPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
let kCommonDateFormat = "MMM dd, yyyy"

class PDPatient: NSObject {
    var selectedForms : [Forms]!
    
    var doctorNameTag : Int?
     var doctorName : String!
    var firstName : String!
    var lastName : String!
    var initial : String!
    var dateOfBirth : String!
    var dateToday : String!
    var phoneNumber : String!
    
    #if AUTO
    var patientDetails : PatientDetails?
    #endif

    var is18YearsOld : Bool {
        return patientAge >= 18
    }
    
    var fullName: String {
        return initial != nil || initial.characters.count > 0 ? firstName + " " + initial + " " + lastName : firstName + " " + lastName
    }
    
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = kCommonDateFormat
            
            let yearVariation = (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: Date())  - (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: dateFormatter.date(from: self.dateOfBirth)!)
            let monthVariation = (Calendar.current as NSCalendar).component(NSCalendar.Unit.month, from: Date())  - (Calendar.current as NSCalendar).component(NSCalendar.Unit.month, from: dateFormatter.date(from: self.dateOfBirth)!)
            let dayVariation = (Calendar.current as NSCalendar).component(NSCalendar.Unit.day, from: Date())  - (Calendar.current as NSCalendar).component(NSCalendar.Unit.day, from: dateFormatter.date(from: self.dateOfBirth)!)
            
            return monthVariation > 0 ? yearVariation - 1 : (monthVariation == 0 ? (dayVariation >= 0 ? yearVariation - 1 : yearVariation) : yearVariation)
        }
    }
    
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    func getPatient() -> PDPatient {
        let patient = PDPatient(forms: self.selectedForms)
        patient.dateToday = self.dateToday
        patient.appointmentCancelFee = self.appointmentCancelFee
        patient.hardFoodFee = self.hardFoodFee
        patient.doctorNameTag = self.doctorNameTag
        patient.doctorName = self.doctorName
        patient.firstName = self.firstName
        patient.lastName = self.lastName
        patient.dateOfBirth = self.dateOfBirth
        patient.initial = self.initial
        return patient
    }
    
    var frontImage: UIImage?
    var backImage: UIImage?
    
    //NEW PATIENT
    var parentFirstName : String?
    var parentLastName : String?
    var parentDateOfBirth : String?
    var addressLine : String!
    var city : String!
    var state : String!
    var zipCode : String!
//    var socialSecurityNumber : String?
    var email : String!
    var employerName : String?
    var employerPhoneNumber : String?
    var emergencyContactName : String!
    var emergencyContactPhoneNumber : String!
    var whoReferred: String!
    var lastDentalVisit : String?
    var reasonForTodayVisit : String?
    var isHavingPain : Bool!
    var painLocation : String?
    var anyConcerns : String?
    var hearAboutUs : String?
    var signature1 : UIImage!
    var signature2 : UIImage!
    var signature3 : UIImage!
    var signature4 : UIImage!
    var signature5 : UIImage!
    var relationship: String?
    
    var initial1 : UIImage!
    var initial2 : UIImage!
    var initial3 : UIImage!
    
    var isCallAvail: Bool!
    var isTextAvail: Bool!
    
    var repName: String!
    var repAuthority: String!
    var repSignature: UIImage?
    
    
    //PATIENT INTAKE ADDRESS VC
    
    var PatientIntakeaddressLine : String!
    var PatientIntakecity : String!
    var PatientIntakestate : String!
    var PatientIntakezipCode : String!
//    var PatientIntakesocialSecurityNumber : String?
    var PatientIntakeemail : String!
     var PatientIntakephoneNumber : String!

    // PATIENT INTAKE ADDRESS VC2
    var referredByDetails : String!
    var othersDetails : String!
    
    var PatientIntakeCellPhone : String!
    var PatientIntakeWorkPhone : String!
    var PatientIntakeAppoinment : String!
    var PatientIntakeFamilyMember : String!
    var PatientGenderTag : Int?
    var ReferedBywhomeTag: Int?
     var PatientInsuranceTag : Int?
    
    //PATIENT INTAKE INSURANCE VC
    
    var InsuranceCoName : String!
    var InsuranceSubscriberName : String!
    var InsuranceDOB : String!
    var InsuranceRelation : String!
    
    
    var InsuranceSubsciberID : String!
    var InsuranceCoPhone : String!
    var InsuranceGroupName : String!
    var InsuranceAccountNum : String!
    
  
    
    //PREVIOUS DENTAL HISTORY VC
    
    var DentalHistoryHowLongSawADentistCount : String!
    var DentalHistoryHowLongSawADentistPeriod : String!
    var DentalHistoryHaveYouRegularWithDental : String!
    var DentalHistoryOnAScale : String!
    var DentalHistoryChangeAnyAboutSmile : String!
      var DentalHistoryCleaningOrProb : Int?
    
    var DentalCleaningOrProbString : String!
    
      var dentalHistory2 : [PDQuestion]! = [PDQuestion]()
    
    
    //PREVIOUS DENTAL HISTORY VC3
    
    var Dental3HaveyouBeendoctor : String!
    var Dental3AnyCardiac : String!
    var Dental3AnyAllergic : String!
    
    
    //APPOINMENT VC
    
     var premedQues : [PDQuestion]! = [PDQuestion]()
    
    var AppoinmetDate : String!
    var AppoinmetTime : String!
    var AppointmentSign :UIImage!
    
    
    
    // INFORMED REFUSAL TREATMENT VC1
    var informedRefusaldentistRecomend : String!
    var informedRefusalneedForService : String!
    var informedRefusaltextViewRisk : String!
    
    
    
    // INFORMED REFUSAL TREATMENT VC2
    
    var BtnSelected1 : Bool!
     var BtnSelected2 : Bool!
     var BtnSelected3 : Bool!
    var guardianRelationship : String!
    
    var InformedSign1 : UIImage!
    var InformedSign2 : UIImage!
    
    
    var todayDateTime : String!
    
    // INFORMED REFUSAL RADIOGRAPHY
    
    var getTextNA = "N/A"
    
    var RadioBtnSelected1 : Bool!
    var RadioBtnSelected2 : Bool!
    var RadioBtnSelected3 : Bool!
    var RadioguardianRelationship : String!
    var RadioDentistRecomend : String!
    
    var RadioInformedSign1 : UIImage!
    var RadioInformedSign2 : UIImage!
    
    
    //MEDICAL HISTORY
    var medicalHistoryQuestions1 : [PDQuestion]! = [PDQuestion]()
    var medicalHistoryQuestions2 : [PDOption]! = [PDOption]()
    var medicalHistoryQuestions3 : [PDOption]! = [PDOption]()
    var medicalHistoryQuestions4 : [PDOption]! = [PDOption]()
    var isWomen : Bool! = false
    var controlledSubstances : String?
    var controlledSubstancesClicked : Bool!
    var othersTextForm3 : String?
    var comments : String?
    var otherIllness : String?

    
    //DENTAL RECORDS RELEASE
    var dentalReleaseName : String!
    var dentalReleaseAddress : String!
    var dentalReleaseCity : String!
    var dentalReleaseState : String!
    var dentalReleaseZipcode : String!
    var dentalReleaseSignature : UIImage!

    var cardDetails : CreditCard!
    var responsibleParty : String!
    var clearanceQuestions : [PDQuestion]! = [PDQuestion]()
    var creditCardQuestions : [PDQuestion]! = [PDQuestion]()

    //PATIENT AUTHORIZATION
    var previousClinicName : String!
    var newClinicName : String!
    var patientNumber : String!
    var faxNumber : String?
    var reasonForTransfer : String?
    var relation : String!

    var implantConsultation1: [PDQuestion]! = [PDQuestion]()
    var implantConsultation2: [PDQuestion]! = [PDQuestion]()
    var implantConsultation3: [PDQuestion]! = [PDQuestion]()
    var implantConsultation4: [PDQuestion]! = [PDQuestion]()
    
    var speciality1: String!
    var treatment1: String!
    var practioner1: String!
    var speciality2: String!
    var treatment2: String!
    var practioner2: String!
    var appoinment1 : String!
    var appoinment2 : String!
    
    var aspirin : Int!
    var smoke: Int!
    var nervous: Int!
    var otherTreatment : String?

    var selectedOptions : [String]! = [String]()
    var treatmentDate : String?
    var physicianName : String!


    var relationShip1: String?
    var relationShip2: String?

    //AuthorizationRelease
    var practiceName: String?
    var treatment: String!
    var condition: Int!
    var conditionOther: String!
    var expires: Int!
    var expiresString: String!
    
    //Personal Representative
    var relatives: [Relation]! = [Relation]()
    var requestDate: String!
    var expiryDate: String!
    var exceptions: String!
    
    // CROWN OR BRIDGE VC
    
    var patientSignature : UIImage!
    var witness : String!
   
    // FINANCIAL POLICY

    //TOOTH ALIGNMENT
    var appointmentCancelFee : String!
    var hardFoodFee : String!

    var improveAspects1: [PDOption]! = [PDOption]()
    var improveAspects2: [PDOption]! = [PDOption]()
    
    var quickTreatment: QuickTreatment! = QuickTreatment()
    var examForm: ExamForm! = ExamForm()

}


class ExamForm: NSObject {
    var section1Questions: [PDQuestion]! = [PDQuestion]()
    var section2Questions: [PDQuestion]! = [PDQuestion]()
    var section3Questions: [PDQuestion]! = [PDQuestion]()
    var section6Questions: [PDQuestion]! = [PDQuestion]()
    var section7Questions: [PDQuestion]! = [PDQuestion]()
    var section8Questions: [PDQuestion]! = [PDQuestion]()
    
    //SECTION1
    var maxillary: String!
    var probingDepth: String!
    var passiveEruption: String!
    var activeEruption: String!
    var gummySmileWidth: String!
    var gummySmileLength: String!
    var zoneLength: String!
    var zoneWidth: String!
    var teethShape: String!
    var desiredShade: String!
    var overbite: String!
    var overjet: String!
    var midlineTowards: String!
    
    //SECTION2
    var refer: String!
    var teethMissing: String!
    var replaceTeeth: String!
    
    
    //SECTION3,4
    var occlusionTag: Int?
    var photoTags: [Int] = [Int]()
    
    //SECTION7
    var oralHygiene: String!
    
    //SECTION8
    var treatmentLength: String!
    var reshaping: String!
    var treatmentTime: String!
    var notes: String!
}


class QuickTreatment: NSObject {
    //QUICK TREATMENT
    var quickTreatmentQuestions: [PDQuestion]! = [PDQuestion]()
    var crowdingUpper: String!
    var crowdingUpperSpacing: String!
    var crowdingLower: String!
    var crowdingLowerSpacing: String!
    var upperWire: String!
    var lowerWire: String!
    var upperReproximation: Int?
    var lowerReproximation: Int?
    var specialAttention: String!
    var complaint: String!
    var itemToCorrect: String!
}

class CreditCard : NSObject {
    var depositAmount : String!
    var planAmount : String!
    var lengthMethod : String!
    var lengthOfTime : String!
    var cardType : Int!
    var cardHolderName : String!
    var cardNumber : String!
    var expiryDate : String!
    var ccvCode : String!
}


enum MaritalStatus: String {
    case SINGLE = "S"
    case MARRIED = "M"
    case WIDOWED = "W"
    case DIVORCED = "D"
    case SEPARATE = "X"
    case UNKNOWN = "U"
    case DEFAULT = "DE"
    
    init(value: Int) {
        switch value {
        case 1: self = .SINGLE
        case 2: self = .MARRIED
        case 3: self = .WIDOWED
        case 4: self = .DIVORCED
        case 5: self = .SEPARATE
        case 6: self = .UNKNOWN
        default: self = .DEFAULT
        }
    }
    var index: Int {
        switch self {
        case .SINGLE: return 1
        case .MARRIED: return 2
        case .WIDOWED: return 3
        case .DIVORCED: return 4
        case .SEPARATE: return 5
        case .UNKNOWN: return 6
        default: return 7
        }
    }
    var description: String {
        switch self {
        case .SINGLE: return "SINGLE"
        case .MARRIED: return "MARRIED"
        case .WIDOWED: return "WIDOWED"
        case .DIVORCED: return "DIVORCED"
        case .SEPARATE: return "SEPARATED"
        case .UNKNOWN: return "UNKNOWN"
        default: return "DEFAULT"
        }
    }
}

enum Gender: String {
    case MALE = "M"
    case FEMALE = "F"
    case DEFAULT = "D"
    
    init(value: Int) {
        switch value {
        case 1: self = .MALE
        case 2: self = .FEMALE
        default: self = .DEFAULT
        }
    }
    var index: Int {
        switch self {
        case .MALE: return 1
        case .FEMALE: return 2
        default: return 3
        }
    }
    
    var description: String {
        switch self {
        case .MALE: return "MALE"
        case .FEMALE: return "FEMALE"
        default: return "DEFAULT"
        }
    }
}

#if AUTO
    class PatientDetails : NSObject {
        var dateOfBirth : String!
        var firstName : String!
        var lastName : String!
        var preferredName : String!
        var address : String!
        var city : String!
        var state : String!
        var zipCode : String!
        var country : String!
        var gender : Gender?
//        var socialSecurityNumber : String!
        var email : String!
        var homePhone : String!
        var workPhone : String!
        var cellPhone : String!
        
        
        var patientNumber : String!
        var maritalStatus: MaritalStatus?
        var middleInitial : String!
        
        override init() {
            super.init()
        }
        
        init(details : [String: AnyObject]) {
            super.init()
            self.city = getValue(details["City"])
            self.dateOfBirth = getValue(details["Birthdate"])
            self.email = getValue(details["Email"])
//            self.socialSecurityNumber = getValue(details["SSN"])
            self.address = getValue(details["Address"])
            self.zipCode = getValue(details["Zip"])
            self.gender = getValue(details["sex"]) == "" ? nil : Gender(rawValue: getValue(details["sex"]))
            self.firstName = getValue(details["FName"])
            self.patientNumber = getValue(details["PatNum"])
            self.state = getValue(details["State"])
            self.homePhone = getValue(details["HmPhone"]).toPhoneNumber()
            self.workPhone = getValue(details["work_phone"]).toPhoneNumber()
            self.cellPhone = getValue(details["cellular_phone"]).toPhoneNumber()
            self.country = getValue(details["Country"])
            self.lastName = getValue(details["LName"])
            self.preferredName = getValue(details["Preferred"])
            self.maritalStatus = getValue(details["marital_status"]) == "" ? nil : MaritalStatus(rawValue: getValue(details["marital_status"]))
            self.middleInitial = getValue(details["MiddleI"])
        }
        
        func getValue(_ value: AnyObject?) -> String {
            if value is String {
                return (value as! String).uppercased()
            }
            return ""
        }
    }
#endif
