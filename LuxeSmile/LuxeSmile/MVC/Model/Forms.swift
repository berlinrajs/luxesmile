//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kNewPatientForm = "NEW PATIENT SIGN IN FORM"
let kMedicalHistoryForm = "MEDICAL HISTORY FORM"
let kNewPatientIntakeForm = "NEW PATIENT INTAKE FORM"
let kPatientIntakeForm = "PATIENT INTAKE FORM"

let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"

let kdentalRelease = "AUTHORIZATION TO RELEASE DENTAL RECORDS"
let kPrivacyPractice = "ACKNOWLEDGEMENT OF RECEIPT OF NOTICE OF PRIVACY PRACTICES"
let kCreditCard = "CREDIT CARD AUTHORIZATION"
let kDentalRecords = "DENTAL RECORDS TRANSFER REQUEST"
let kImplantConsult = "IMPLANT CONSULTATION"
let kMedicalClearance = "MEDICAL CLEARANCE REQUEST FORM"
let kReleaseHealthCare = "AUTHORIZATION TO RELEASE HEALTH CARE INFORMATION"
let kPersonalRepresentative = "PERSONAL REPRESENTATIVE"
let kRefusalDentalTreatment = "REFUSAL OF DENTAL TREATMENT"
let kCrownOrBridgeVC = "CROWN OR BRIDGE"
let kFinancialPolicy = "FINANCIAL POLICY"
let KInHouseBleaching = "CONSENT FORM FOR IN HOUSE BLEACHING"

let kCosmeticTreatment = "COSMETIC TREATMENT"
let kImplantConsent = "IMPLANT CONSENT FORM"
let kPeriodontalCleaning = "CONSENT TO PERFORM PERIODONTAL CLEANING"
let kOperativeInstructions = "ORAL SEDATION (ANXIOLYSIS) OPERATIVE INSTRUCTIONS"
let kConsentTreatment = "INFORMED CONSENT FOR TREATMENT"
let kDigitalPhotos = "CONSENT FOR USE OF DIGITAL PHOTOS"
let kImplantSurgery = "POST OP INSTRUCTIONS FOR DENTAL IMPLANT SURGERY"
let kPostInstructions = "POST OP INSTRUCTIONS"
let kProcelain = "PROCELAIN RESTORATIONS CONSENT"
let kDigitalPhotos1 = "CONSENT FOR USE OF DIGITAL PHOTOS (FULL FACE/ HEAD SHOT & CLOSE UP SMILE PHOTO)"
let kDigitalPhotos2 = "CONSENT FOR USE OF DIGITAL PHOTOS (CLOSE UP SMILE PHOTO-ONLY SHOWS TEETH AND LIPS, NOT THE FULL FACE)"
let kInformedRefusal = "INFORMED TREATMENT REFUSAL FORM"
let kInformedRadioGraph = "INFORMED RADIOGRAPH REFUSAL FORM"
let kNitrousOxide = "NITROUS OXIDE INFORMED CONSENT"
let kQuickTreatment = "QUICK TREATMENT PLANNING FORM"
let kImproveAspects = "WHAT ASPECTS OF YOUR SMILE WOULD YOU LIKE TO IMPROVE?"
let kRetainerInfo = "RETAINER INFO AND AGREEMENT"
let kResultAcceptance = "TREATMENT RESULT ACCEPTANCE"
let kToothAlignment = "PATIENT AGREEMENT AND INFORMED CONSENT FOR COSMETICALLY FOCUSED TOOTH ALIGNMENT"
let kExamForm = "SHORT TERM ORTHO AND COSMETIC EXAM FORM"
let kWarrantyForm = "WARRANTY ON OUR DENTAL WORK"
let kFeedBack = "CUSTOMER REVIEW FORM"

let kConsentForm = "CONSENT FORM"

#if AUTO
let kNewPatient = ["NEW PATIENT" : [kNewPatientIntakeForm,kMedicalHistoryForm,kInsuranceCard,kDrivingLicense]]
let kExistingPatient = ["EXISTING PATIENT": [kPatientIntakeForm,kMedicalHistoryForm,kInsuranceCard,kDrivingLicense]]
let kConsentFormsAuto = ["CONSENT FORMS": [kPrivacyPractice,kCreditCard,kDentalRecords,kdentalRelease,kImplantConsult,kMedicalClearance,kReleaseHealthCare,kPersonalRepresentative,kRefusalDentalTreatment,kCrownOrBridgeVC,kFinancialPolicy,KInHouseBleaching, kCosmeticTreatment, kImplantConsent,kImplantSurgery,kPostInstructions,kProcelain,kPeriodontalCleaning,kOperativeInstructions,kConsentTreatment,kDigitalPhotos1,kDigitalPhotos2,kInformedRefusal,kInformedRadioGraph,kNitrousOxide,kRetainerInfo,kToothAlignment,kResultAcceptance,kQuickTreatment,kImproveAspects,kExamForm,kWarrantyForm,kFeedBack]]
#endif

class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var toothNumber: String!

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (_ completion :(_ isConnectionfailed: Bool, _ forms : [Forms]?) -> Void) {
        
        let isConnected = Reachability.isConnectedToNetwork()
        #if AUTO
            var arrayResult : [Forms] = [Forms]()
            let forms = [kNewPatient, kExistingPatient, kConsentFormsAuto]
            for form in forms {
                let formObj = getFormObjects(forms: form)
                arrayResult.append(formObj)
            }
            completion(isConnected ? false : true, arrayResult)
        #else
            let forms = [kNewPatientIntakeForm,kMedicalHistoryForm,kInsuranceCard,kDrivingLicense,kConsentForm, kFeedBack]
            let formObj = getFormObjects(forms, isSubForm: false)
            completion(isConnected ? false : true, formObj)
        #endif
    }
    
    
    #if AUTO
    private class func getFormObjects (forms : [String : [String]]) -> Forms {
        let formObject = Forms()
        formObject.formTitle = forms.keys.first!
        formObject.isSelected = false
        
        var formList : [Forms] = [Forms]()
        for (_, form) in forms.values.first!.enumerated() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.formTitle = form
            formList.append(formObj)
        }
        formObject.subForms = formList
        return formObject
    }
    #else
    fileprivate class func getFormObjects (_ forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerated() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 5 : idx
            formObj.formTitle = form
            if formObj.formTitle == kConsentForm {
                formObj.subForms = getFormObjects([kPrivacyPractice,kCreditCard,kDentalRecords,kdentalRelease,kImplantConsult,kMedicalClearance,kReleaseHealthCare,kPersonalRepresentative,kRefusalDentalTreatment,kCrownOrBridgeVC,kFinancialPolicy,KInHouseBleaching, kCosmeticTreatment, kImplantConsent,kImplantSurgery,kPostInstructions,kProcelain,kPeriodontalCleaning,kOperativeInstructions,kConsentTreatment,kDigitalPhotos1,kDigitalPhotos2,kInformedRefusal,kInformedRadioGraph,kNitrousOxide,kRetainerInfo,kToothAlignment,kResultAcceptance,kQuickTreatment,kImproveAspects,kExamForm,kWarrantyForm], isSubForm:  true)
            }
            if formObj.formTitle == kFeedBack {
                formObj.index = 38
            }
            formList.append(formObj)
        }
        return formList
    }
    #endif
}
