//
//  PDTextField.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDTextField: UITextField {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = isEnabled ? borderColor.cgColor : borderColor.withAlphaComponent(0.2).cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)])
        tintColor = isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)
        clipsToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: frame.height))
        self.leftView = leftView
        leftViewMode = .always
    }
    
    var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var placeHolderColor: UIColor = UIColor.white.withAlphaComponent(0.5) {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    override var placeholder: String? {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
}



class MCTextField: UITextField {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = isEnabled ? borderColor.cgColor : borderColor.withAlphaComponent(0.2).cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)])
        tintColor = isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)
        clipsToBounds = true
        
        self.autocorrectionType = UITextAutocorrectionType.no
        self.spellCheckingType = UITextSpellCheckingType.no
        self.autocapitalizationType = UITextAutocapitalizationType.allCharacters
        
        self.prepareTextField()
    }
    
    fileprivate var count : Int?
    fileprivate var limit : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if delegate == nil {
            delegate = self
        }
    }
    
    var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var placeHolderColor: UIColor = UIColor.white.withAlphaComponent(0.5) {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    override var placeholder: String? {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    func setNumberFormatWithCount(_ count: Int, limit: Int) {
        self.textFormat = .number
        self.count = count
        self.limit = limit
    }
    
    @IBInspectable var textFormat: TextFormat = TextFormat.default
    func prepareTextField() {
        switch self.textFormat {
        case .default:
            self.keyboardType = UIKeyboardType.default
        case .socialSecurity:
            self.isSecureTextEntry = true
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
            self.count = 9
        case .toothNumber:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .phone:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .zipcode:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .number:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
            self.count = 100
        case .date:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
            self.count = 2
            self.limit = 31
        case .middleInitial:
            self.keyboardType = UIKeyboardType.default
        case .month: break
//            MonthListView.addMonthListForTextField(self)
        case .year:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
            self.count = 4
        case .time: break
//            DateInputView.addTimePickerForTextField(self)
        case .dateInCurrentYear:
            DateInputView.addDatePickerForTextField(self)
        case .dateIn1980: break
//            DateInputView.addDatePickerForDateOfBirthTextField(self)
        case .state:
            StateListView.addStateListForTextField(self)
        case .amount:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .extensionCode:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .email:
            self.keyboardType = UIKeyboardType.emailAddress
        case .secureText:
            self.isSecureTextEntry = true
        case .numbersWithoutValidation:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation

        }
    }
}
extension MCTextField: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.textFormat == .phone {
            return textField.formatPhoneNumber(range, string: string)
        } else if self.textFormat == .zipcode {
            return textField.formatZipCode(range, string: string)
        } else if self.textFormat == .number || self.textFormat == .socialSecurity || self.textFormat == .year || self.textFormat == .date {
            return textField.formatNumbers(range, string: string, count: count!, limit: limit)
        } else if textFormat == .amount {
            return textField.formatAmount(range, string: string)
        } else if textFormat == .amount {
            return textField.formatExt(range, string: string)
        } else if self.textFormat == .middleInitial {
//            return textField.formatMiddleName(range, string: string)
        } else if self.textFormat == .toothNumber {
            return textField.formatToothNumbers(range, string: string)
        }
        return true
    }
}
