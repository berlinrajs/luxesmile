//
//  DateInputView.swift
//  AceDental
//
//  Created by SRS Web Solutions on 27/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CountInputView: UIView {
    var toolbar: UIToolbar!
    var pickerMonth : UIPickerView!
    var textField: UITextField!
  
    var arrayMonths = ["1"]
    
 
    override init(frame: CGRect) {
        
        for i in 2...31 {
            
            let myString = String(i)
            arrayMonths.append(myString)
        }
        
        
        super.init(frame: frame)
        pickerMonth = UIPickerView(frame: frame)
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        let barButtonClear = UIBarButtonItem(title: "Clear", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPressed))
        toolbar.items = [barButtonClear, UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        self.addSubview(pickerMonth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func cancelPressed() {
        textField.resignFirstResponder()
        textField.text = ""
    }
    
    func donePressed() {
        let string1 = arrayMonths[pickerMonth.selectedRow(inComponent: 0)]
        textField.text = string1
        textField.resignFirstResponder()
    }
    
    class func addCountPickerForTextField(_ textField: UITextField) {
        let monthListView = CountInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = monthListView
        textField.inputAccessoryView = monthListView.toolbar
        monthListView.textField = textField
    }
}

extension CountInputView : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      //  let string1 = arrayMonths[row]
        
        //let index1 = string1.startIndex.advancedBy(3)
        
       // textField.text = string1.substringWithRange(string1.startIndex ..< string1.startIndex.advancedBy(3))
        
          textField.text = arrayMonths[row]
    }
}
