//
//  AlertView.swift
//  ProDental
//
//  Created by Leojin Bose on 3/3/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class PopupTextView: UIView {
    static let sharedInstance = Bundle.main.loadNibNamed("PopupTextView", owner: nil, options: nil)!.first as! PopupTextView

    
    class func popUpView() -> PopupTextView {
        return Bundle.main.loadNibNamed("PopupTextView", owner: nil, options: nil)!.first as! PopupTextView
    }
    @IBOutlet weak var textView: PDTextView!
    var placeHolder : String!
    var completion:((UITextView, Bool)->Void)?
    
    func show(_ completion : @escaping (_ textView : UITextView, _ isEdited : Bool) -> Void) {
        self.showWithPlaceHolder("IF YES, TYPE HERE", completion: completion)
    }
    
    func showWithPlaceHolder(_ placeHolder : String, completion : @escaping (_ textView : UITextView, _ isEdited : Bool) -> Void) {
        self.placeHolder = placeHolder
        self.completion = completion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textView.text = placeHolder
        textView.textColor = UIColor.lightGray
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        self.removeFromSuperview()
        completion?(self.textView, isEdited)
    }
    
    var isEdited : Bool {
        get {
            return (textView.text != placeHolder && !textView.isEmpty)
        }
    }
}


extension PopupTextView : UITextViewDelegate {
   
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeHolder {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
