//
//  PDTableView.swift
//  LuxeSmile
//
//  Created by Leojin Bose on 03/02/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol PDTableViewDelegate {
    func selectedValue(_ value : String, id : Int , table : UITableView)
}


class PDTableView: UITableView, UITableViewDataSource, UITableViewDelegate {
    var delegatePDTableView : PDTableViewDelegate?
    
    var arrayValues : [String]?
    var useUppercaseString : Bool! = true
    
    override func awakeFromNib() {
        
        self.delegate = self
        self.dataSource = self
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayValues != nil ? arrayValues!.count : 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellList", for: indexPath)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.text = useUppercaseString == true ? arrayValues![indexPath.row].uppercased() : arrayValues![indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegatePDTableView?.selectedValue(arrayValues![indexPath.row], id: indexPath.row, table: self)
    }
}
