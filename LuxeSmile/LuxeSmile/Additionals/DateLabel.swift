//
//  DateLabel.swift
//  ProDental
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateLabel: PDLabel {

    var todayDate: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.white
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        text = "Tap to date"
        textColor = UIColor.lightGray
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(labelDateTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        addGestureRecognizer(tapGesture)
    }
    
    func labelDateTapped(_ sender: AnyObject) {
        text = todayDate
        textColor = UIColor.black
    }
    
    var dateTapped: Bool {
        get {
            return text != "Tap to date"
        }
    }
}
