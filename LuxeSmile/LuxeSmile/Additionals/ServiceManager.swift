//
//  ServiceManager.swift
//  ProDental
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016 ProDental. All rights reserved.
//

import UIKit

let hostUrl = "https://integrations.srswebsolutions.com/demoes/eaglesoft/"
let kAppKey = "mcLuxeSmile"

class ServiceManager: NSObject {
    
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
            }, success: { (task, result) in
                success(result as AnyObject)
        }) { (task, error) in
            failure(error)
        }
    }
    
    class func postReview(_ name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        //https://alpha.mncell.com/review/app_review.php
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php", parameters: ["patient_appkey": kAppKey, "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]) as Error)
            }
        }) { (error) in
            completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]) as Error)
        }
    }
    
    class func loginWithUsername(_ userName: String, password: String, completion: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        ServiceManager.fetchDataFromService("http://mncell.com/mclogin/", serviceName: "apploginapi.php?", parameters: ["appkey": "mclex", "username": userName, "password": password], success: { (result) in
            if (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]) as Error)
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    
    #if AUTO
    class func sendPatientDetails(patient: PDPatient, completion:@escaping (_ success : Bool, _ error: Error?) -> Void) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let date = dateFormatter.date(from: patient.dateOfBirth)!
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var params: [String: String] = [String: String]()
        params["Birthdate"] = dateFormatter.string(from: date)
        params["FName"] = patient.firstName
        params["LName"] = patient.lastName
        if let initial = patient.initial {
            params["MiddleI"] = initial
        }
        if let _ = patient.PatientIntakecity {
            params["City"] = patient.PatientIntakecity
            params["Email"] = patient.PatientIntakeemail
            params["Address"] = patient.PatientIntakeaddressLine
            params["Zip"] = patient.PatientIntakezipCode
            params["HmPhone"] = patient.PatientIntakephoneNumber.numbers
            params["work_phone"] = patient.PatientIntakeWorkPhone?.numbers
            params["cellular_phone"] = patient.PatientIntakeCellPhone?.numbers
            params["sex"] = patient.PatientGenderTag == 1 ? "M" : "F"
            params["State"] = patient.PatientIntakestate
        }
        
//        if let ssn = patient.PatientIntakesocialSecurityNumber {
//            params["SSN"] = ssn.numbers
//        }
        if let patientDetails = patient.patientDetails {
            params["PatNum"] = patientDetails.patientNumber.isEmpty ? "0" : patientDetails.patientNumber
        } else {
            params["PatNum"] = "0"
        }
        let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post("consent_form_add_newpatient_op.php", parameters: params, progress: { (progress) in
            
            }, success: { (task, result) in
                if let patientId = (result as AnyObject)["patient_id"] as? String {
                    if let patientDetails = patient.patientDetails {
                        patientDetails.patientNumber = patientId
                    } else {
                        patient.patientDetails = PatientDetails()
                        patient.patientDetails?.patientNumber = patientId
                    }
                }
                completion(true, nil)
        }) { (task, error) in
            completion(false, error)
        }
    }
    #endif
    
    class func uploadFile(_ fileName: String, clientName: String, patientName: String, formName: String, pdfData: Data, completion: @escaping (_ success: Bool, _ errorMessage: String?)-> Void) {
        #if AUTO
            let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            manager.post("consent_form_upload_to_server.php", parameters: nil, constructingBodyWith: { (formData) in
                formData.appendPart(withFileData: pdfData, name: "consent_file", fileName: fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                completion(true, nil)
            }) { (task, error) in
                completion(false, error.localizedDescription)
            }
        #else
            
            // http://mconsent.net/admin/api/activity_api.php?client_name=pp&formname=MedicalHistoryform&task_data=image&email=srs@gmail.com
            
            let manager = AFHTTPSessionManager(baseURL: URL(string: "http://mconsent.net/admin/api/")!)
            
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            manager.post("activity_api.php?", parameters: ["client_name": clientName, "patient_name": patientName, "formname": formName, "appkey": kAppKey], constructingBodyWith: { (data) in
                data.appendPart(withFileData: pdfData, name: "task_data", fileName: fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                completion(true, nil)
                print("RESULT: \(String(describing: result))")
            }, failure: { (task, error) in
                print("ERROR: \(error.localizedDescription)")
                completion(false, error.localizedDescription)
            })
        #endif
    }
}
