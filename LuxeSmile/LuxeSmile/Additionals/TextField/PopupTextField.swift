//
//  PopupTextField.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

enum TextFormat : Int {
    case `default` = 0
    case socialSecurity
    case toothNumber
    case phone
    case zipcode
    case number
    case date
    case email
    case extensionCode
    case month
    case year
    case dateInCurrentYear
    case dateIn1980
    case time
    case middleInitial
    case state
    case amount
    case secureText
    case numbersWithoutValidation
}

import UIKit

class PopupTextField: UIView {

    static let sharedInstance = Bundle.main.loadNibNamed("PopupTextField", owner: nil, options: nil)!.first as! PopupTextField
    
    class func popUpView() -> PopupTextField {
        return Bundle.main.loadNibNamed("PopupTextField", owner: nil, options: nil)!.first as! PopupTextField
    }
    
    @IBOutlet weak var textField: PDTextField!
    @IBOutlet weak var labelTitle: UILabel!
    
    var completion:((PopupTextField,UITextField, Bool)->Void)?
    var textFormat : TextFormat = .default
    var count : Int!

    
    func show(_ completion : @escaping (_ popUpView: PopupTextField, _ textField : UITextField, _ isEdited : Bool) -> Void) {
        self.showWithTitle(labelTitle.text, placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, inViewController: nil, completion: completion)
    }
    
    
    
    func showWithTitle(_ title: String?, placeHolder : String?, keyboardType : UIKeyboardType, textFormat: TextFormat, inViewController: UIViewController?, completion : @escaping (_ popUpView: PopupTextField, _ textField : UITextField, _ isEdited : Bool) -> Void) {
        textField.text = ""
        labelTitle.text = title
        self.textFormat = textFormat
        if let _ = placeHolder {
            textField.placeholder = placeHolder
        }
        
        textField.keyboardType = keyboardType
        self.completion = completion
        if inViewController == nil {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        } else {
            inViewController!.view.addSubview(self)
        }
        self.frame = screenSize
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    func showDatePopupWithTitle(_ title: String?, placeHolder : String?, minDate : Date?, maxDate : Date?, completion : @escaping (_ popUpView: PopupTextField, _ textField : UITextField, _ isEdited : Bool) -> Void) {
        textField.text = ""
        labelTitle.text = title
        self.textFormat = .date
        if let _ = placeHolder {
            textField.placeholder = placeHolder
        }
        DateInputView.addDatePickerForTextField(textField)
        self.completion = completion
        self.frame = screenSize
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }

    func close() {
        self.removeFromSuperview()
    }
    
    
     
    @IBAction func buttonActionOK(_ sender: AnyObject) {
            completion?(self, self.textField, isEdited)
            self.removeFromSuperview()
    }
    
    var isEdited : Bool {
        get {
            return !textField.isEmpty
        }
    }
    var isValidText: Bool {
        get {
            switch self.textFormat {
            case .socialSecurity:
                return !(isEdited && !textField.text!.isSocialSecurityNumber)
            case .phone:
                return !(isEdited && !textField.text!.isPhoneNumber)
            case .zipcode:
                return !(isEdited && !textField.text!.isZipCode)
            case .email:
                return !(isEdited && !textField.text!.isValidEmail)
            default:
                return true
            }
        }
    }
    
    func showWithPlaceHolder(_ placeHolder : String?, keyboardType : UIKeyboardType, textFormat: TextFormat, completion : @escaping (_ popUpView: PopupTextField, _ textField : UITextField, _ isEdited : Bool) -> Void) {
        textField.text = ""
        self.textFormat = textFormat
        if let _ = placeHolder {
            textField.placeholder = placeHolder
        }
        if textFormat == .date {
            DateInputView.addDatePickerForTextField(textField, minimumDate: Date(), maximumDate: nil, dateFormat: nil)
        } else {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            textField.keyboardType = keyboardType
        }
        self.completion = completion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.frame = screenSize
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }

}


extension PopupTextField : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.textFormat == .phone {
            return textField.formatPhoneNumber(range, string: string)
        } else if self.textFormat == .zipcode {
            return textField.formatZipCode(range, string: string)
        } else if self.textFormat == .number {
            return textField.formatNumbers(range, string: string, count: 3)
        } else if self.textFormat == .toothNumber {
            return textField.formatToothNumbers(range, string: string)
        } else if self.textFormat == .socialSecurity {
            return textField.formatSocialSecurityNumber(range, string: string)
        }
        return true
    }
    
    
}


class PopupTextFieldNew: UIView {
    
    static let sharedInstance = Bundle.main.loadNibNamed("PopupTextField", owner: nil, options: nil)![1] as! PopupTextFieldNew
    
    class func popUpView() -> PopupTextFieldNew {
        return Bundle.main.loadNibNamed("PopupTextField", owner: nil, options: nil)![1] as! PopupTextFieldNew
    }
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var textField1: PDTextField!
    @IBOutlet weak var textField2: PDTextField!
    
    var completion:((PopupTextFieldNew,UITextField, UITextField)->Void)?
    var textFormat : TextFormat = .default
    var count : Int!
    
    
    func show(_ completion : @escaping (_ popUpView: PopupTextFieldNew, _ textField1 : UITextField, _ textField2 : UITextField) -> Void) {
        self.completion = completion
        self.textFormat = . number
        textField1.keyboardType = .numberPad
        textField2.keyboardType = .numberPad
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.frame = screenSize
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    func show(_ title1: String, title2: String, completion : @escaping (_ popUpView: PopupTextFieldNew, _ textField1 : UITextField, _ textField2 : UITextField) -> Void) {
        label1.text = title1
        label2.text = title2
        self.show(completion)
    }
    
    
    
    func close() {
        self.removeFromSuperview()
    }
    
    
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        completion?(self, self.textField1, self.textField2)
        self.removeFromSuperview()
    }
}


extension PopupTextFieldNew : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.textFormat == .phone {
            return textField.formatPhoneNumber(range, string: string)
        } else if self.textFormat == .zipcode {
            return textField.formatZipCode(range, string: string)
        } else if self.textFormat == .number {
            return textField.formatNumbers(range, string: string, count: 10)
        } else if self.textFormat == .toothNumber {
            return textField.formatToothNumbers(range, string: string)
        } else if self.textFormat == .socialSecurity {
            return textField.formatSocialSecurityNumber(range, string: string)
        }
        return true
    }
    
    
}
